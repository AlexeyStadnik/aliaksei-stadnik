package by.stadnik.epam.service;

import java.util.List;

import by.stadnik.epam.entity.Comment;



public interface CommentsService {
	
	public Long addComment(Comment comment) throws ServiceException;
	public void deleteCommentByCommentId(Long commentId) throws ServiceException;
	public void deleteCommentsByNewsId(Long newsId) throws ServiceException;
	public void updateComment(Comment comment) throws ServiceException;
	public List<Comment> getComments(Long newsId) throws ServiceException;
	public Comment getCommentByCommentId(Long commentId) throws ServiceException;

}
