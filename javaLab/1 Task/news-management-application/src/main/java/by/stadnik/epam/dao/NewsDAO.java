package by.stadnik.epam.dao;


import java.util.List;

import by.stadnik.epam.entity.News;
import by.stadnik.epam.util.SearchCriteria;

public interface NewsDAO {
	
	public Long addNews(News news) throws DAOException;
	public void deleteNews(Long newsId) throws DAOException;
	public News getNews(Long newsId) throws DAOException;
	public void updateNews(News news) throws DAOException;
	public List<News> getListOfNews(SearchCriteria searchCriteria) throws DAOException;
	
}
