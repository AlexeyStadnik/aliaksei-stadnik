package by.stadnik.epam.service;

import java.util.List;

import by.stadnik.epam.entity.Author;
import by.stadnik.epam.entity.News;
import by.stadnik.epam.entity.NewsVO;


public interface ServiceManager {

	Long addNews(News news,Author author,List<Long> list ) throws ServiceException;
	void deleteNews(Long newsId) throws ServiceException;
	public NewsVO getNewsVO(Long newsId) throws ServiceException;
}
