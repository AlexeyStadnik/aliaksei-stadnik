package by.stadnik.epam.entity;

import java.sql.Timestamp;

public class Author {
	
	private Long authorId;
	private String authorName;
	private Timestamp expired;
	
	public Author() {
		
	}

	public Author(Long id, String author_name, Timestamp expired) {
		
		this.authorId = id;
		this.authorName = author_name;
		this.expired = expired;
	}

	public Long getId() {
		return authorId;
	}

	public void setId(Long id) {
		this.authorId = id;
	}

	public String getAuthor_name() {
		return authorName;
	}

	public void setAuthor_name(String author_name) {
		this.authorName = author_name;
	}

	public Timestamp getExpired() {
		return expired;
	}

	public void setExpired(Timestamp expired) {
		this.expired = expired;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorName == null) ? 0 : authorName.hashCode());
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		result = prime * result + ((authorId == null) ? 0 : authorId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (authorName == null) {
			if (other.authorName != null)
				return false;
		} else if (!authorName.equals(other.authorName))
			return false;
		if (expired == null) {
			if (other.expired != null)
				return false;
		} else if (!expired.equals(other.expired))
			return false;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Author [id=" + authorId + ", author_name=" + authorName
				+ ", expired=" + expired + "]";
	}
	
	
	
	
	
	

}
