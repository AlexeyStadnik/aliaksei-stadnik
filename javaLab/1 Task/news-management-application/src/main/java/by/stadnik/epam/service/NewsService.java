package by.stadnik.epam.service;

import java.util.List;

import by.stadnik.epam.entity.News;
import by.stadnik.epam.util.SearchCriteria;


public interface NewsService {
	
	public Long addNews(News news) throws ServiceException;
	public void deleteNews(Long newsId) throws ServiceException;
	public News getNews(Long newsId) throws ServiceException;
	public void updateNews(News news) throws ServiceException; 
	List<News>getListOfNews(SearchCriteria searchCriteria) throws ServiceException;
}
