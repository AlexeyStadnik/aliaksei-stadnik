package by.stadnik.epam.service.impl;

import java.util.List;


import org.springframework.transaction.annotation.Transactional;

import by.stadnik.epam.entity.Author;
import by.stadnik.epam.entity.News;
import by.stadnik.epam.entity.NewsVO;
import by.stadnik.epam.service.AuthorService;
import by.stadnik.epam.service.CommentsService;
import by.stadnik.epam.service.NewsService;
import by.stadnik.epam.service.ServiceException;
import by.stadnik.epam.service.ServiceManager;
import by.stadnik.epam.service.TagService;


@Transactional (rollbackFor = Exception.class)
public class ServiceManagerImpl implements ServiceManager  {

	private AuthorService authorService;
	private TagService tagService;
	private CommentsService commentsService;
	private NewsService newsService;



	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	public void setCommentsService(CommentsService commentsService) {
		this.commentsService = commentsService;
	}

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public Long addNews(News news, Author author, List<Long> list) throws ServiceException {

		Long newsId = newsService.addNews(news);

		tagService.addNewsTag(list, newsId);

		authorService.addNewsAuthor(author.getId(), newsId);
		return newsId;
	}




	public void deleteNews(Long newsId) throws ServiceException {
		authorService.deleteNewsAuthor(newsId);
		tagService.deleteNewsTagByNewsId(newsId);
		commentsService.deleteCommentsByNewsId(newsId);
		newsService.deleteNews(newsId);

	}

	public NewsVO getNewsVO(Long newsId) throws ServiceException {
		NewsVO newsVO = new NewsVO(
				newsService.getNews(newsId),
				authorService.getAuthorByNewsId(newsId),
				commentsService.getComments(newsId),
				tagService.getTag(newsId)	
				);
		return newsVO;
	}





	



}
