package by.stadnik.epam.service;

import java.util.List;

import by.stadnik.epam.entity.Tag;

public interface TagService {
	
	public void deleteTag(Long tagId) throws ServiceException;
	public Long addTag(Tag tag) throws ServiceException;
	public void addNewsTag(List<Long> tagList,Long newsId) throws ServiceException;
	public void deleteNewsTagByNewsId(Long newsId) throws ServiceException;
	public void deleteNewsTagByTagId(Long tagId) throws ServiceException;
	public List<Tag> getTag(Long newsId) throws ServiceException;
	public void updateTag(Tag tag) throws ServiceException;
	public Tag getTagByTagId(Long tagId) throws ServiceException;
}
