package by.stadnik.epam.service;

import by.stadnik.epam.entity.Author;


public interface AuthorService {
	
	public Long addAuthor(Author author) throws ServiceException;
	public void deleteAuthor(Long authorId) throws ServiceException;
	public void addNewsAuthor(Long authorId,Long newsId) throws ServiceException;
	public void deleteNewsAuthor(Long newsId) throws ServiceException;
	public Author getAuthorByNewsId(Long newsId) throws ServiceException;
	public Author getAuthorByAuthorId(Long authorId) throws ServiceException;
	public void updateAuthor(Author author) throws ServiceException;

}
