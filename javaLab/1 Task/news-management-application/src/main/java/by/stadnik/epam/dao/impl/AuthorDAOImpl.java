package by.stadnik.epam.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;






import javax.sql.DataSource;


import org.springframework.jdbc.datasource.DataSourceUtils;

import by.stadnik.epam.dao.AuthorDAO;
import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.entity.Author;
import by.stadnik.epam.util.ConnectionClose;

/**
 * DAO class for working with AUTHOR and NEWS_AUTHOR tables
 * @author Aliaksei_Stadnik
 *
 */
public class AuthorDAOImpl  implements AuthorDAO {
	
	private DataSource dataSource;
	
	public static final String INSERT_AUTHOR_SQL = "INSERT INTO author (author_id,author_name,expired) VALUES (author_seq.NEXTVAL,?, ?)";
	public static final String DELETE_AUTHOR_SQL = "DELETE FROM author WHERE author_id = ?";
	public static final String DELETE_NEWS_AUTHOR_SQL = "DELETE FROM news_author WHERE news_id = ?";
	public static final String INSERT_NEWS_AUTHOR_SQL = "INSERT INTO news_author (news_id,author_id) VALUES (?, ?)";
	public static final String SELECT_AUTHOR_BY_AUTHOR_ID_SQL = "SELECT author_id,author_name,expired FROM author WHERE author_id = ? ";
	public static final String SELECT_AUTHOR_BY_NEWS_ID_SQL = "SELECT author.author_id,author_name,expired FROM author JOIN news_author ON author.author_id = news_author.author_id WHERE news_author.news_id = ?";
	public static final String UPDATE_AUTHOR_SQL = "UPDATE author SET author_name=?,expired=? WHERE author_id=?";
	
	public static final String PARAM_AUTHOR_ID = "AUTHOR_ID";
	public static final String PARAM_AUTHOR_NAME = "AUTHOR_NAME";
	public static final String PARAM_EXPIRED = "EXPIRED";
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	
	
	
	public Long addAuthor(Author author) throws DAOException {
		Long authorId = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource); 
			preparedStatement = connection.prepareStatement(INSERT_AUTHOR_SQL,new String [] {"AUTHOR_ID"});
			preparedStatement.setNString(1, author.getAuthor_name());
			preparedStatement.setTimestamp(2, author.getExpired());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if(resultSet.next()) {
				authorId = Long.parseLong(resultSet.getString(1));
			}
			
		} catch (SQLException e) {
			throw new DAOException("Exception while author adding",e);
		} finally {
			ConnectionClose.close(connection, preparedStatement,dataSource,resultSet);
		}
		return authorId;
	}
	
	public void deleteAuthor(Long authorId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(DELETE_AUTHOR_SQL);
			preparedStatement.setLong(1, authorId);
			preparedStatement.executeUpdate();
		
		}catch(SQLException e) {
			
			throw new DAOException("Exception while author delete",e);
		} finally {
			ConnectionClose.close(connection,preparedStatement,dataSource);
		}
	}

	public void addNewsAuthor(Long authorId, Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource); 
			preparedStatement = connection.prepareStatement(INSERT_NEWS_AUTHOR_SQL);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();
			
		}catch(SQLException e) {
			throw new DAOException("Exception while news_author adding process",e);
			
		} finally {
			ConnectionClose.close(connection,preparedStatement,dataSource);
		}
		
	}

	public void deleteNewsAuthor( Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource); 
			preparedStatement = connection.prepareStatement(DELETE_NEWS_AUTHOR_SQL);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
			
		}catch(SQLException e) {
			throw new DAOException("Exception while news_author deleting process",e);
			
		} finally {
			ConnectionClose.close(connection,preparedStatement,dataSource);
		}
		
	}

	public Author getAuthorByNewsId(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Author author = new Author();
		try {
		connection = DataSourceUtils.doGetConnection(dataSource); 
		preparedStatement = connection.prepareStatement(SELECT_AUTHOR_BY_NEWS_ID_SQL);
		preparedStatement.setLong(1, newsId);
		resultSet = preparedStatement.executeQuery();
		while(resultSet.next()) {
			
						   author.setId(resultSet.getLong(PARAM_AUTHOR_ID));
						   author.setAuthor_name(resultSet.getNString(PARAM_AUTHOR_NAME));
						   author.setExpired(resultSet.getTimestamp(PARAM_EXPIRED));
						
					
		}
		
		return author;
		} catch(SQLException e) {
			throw new DAOException("Exception while comments deleting",e);
		}finally {
			ConnectionClose.close(connection, preparedStatement,dataSource,resultSet);
		}
	}
	
	public Author getAuthorByAuthorId(Long authorId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Author author = new Author();
		try {
		connection = DataSourceUtils.doGetConnection(dataSource); 
		preparedStatement = connection.prepareStatement(SELECT_AUTHOR_BY_AUTHOR_ID_SQL);
		preparedStatement.setLong(1, authorId);
		resultSet = preparedStatement.executeQuery();
		while(resultSet.next()) {
			
						   author.setId(resultSet.getLong(PARAM_AUTHOR_ID));
						   author.setAuthor_name(resultSet.getNString(PARAM_AUTHOR_NAME));
						   author.setExpired(resultSet.getTimestamp(PARAM_EXPIRED));
						
					
		}
		
		return author;
		} catch(SQLException e) {
			throw new DAOException("Exception while comments deleting",e);
		}finally {
			ConnectionClose.close(connection, preparedStatement,dataSource,resultSet);
		}
	}

	public void updateAuthor(Author author) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
		connection = DataSourceUtils.doGetConnection(dataSource); 
		preparedStatement = connection.prepareStatement(UPDATE_AUTHOR_SQL);
		preparedStatement.setString(1, author.getAuthor_name());
		preparedStatement.setTimestamp(2, author.getExpired());
		preparedStatement.setLong(3, author.getId());
		preparedStatement.executeUpdate();
		} catch(SQLException e) {
			throw new DAOException("Exception while comments deleting",e);
		}finally {
			ConnectionClose.close(connection, preparedStatement,dataSource);
		}
	}

}
