package by.stadnik.epam.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;


import org.springframework.jdbc.datasource.DataSourceUtils;

import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.dao.NewsDAO;
import by.stadnik.epam.entity.News;
import by.stadnik.epam.util.ConnectionClose;
import by.stadnik.epam.util.SearchCriteria;


public class NewsDAOImpl  implements NewsDAO{

	private DataSource dataSource;

	public static final String INSERT_NEWS_SQL = "INSERT INTO news (news_id,title, short_text, full_text, creation_date, modification_date) VALUES (news_seq.NEXTVAL, ?, ?, ?, ?, ?)";
	public static final String DELETE_NEWS_SQL = "DELETE FROM news WHERE news_id = ?";
	public static final String UPDATE_NEWS_SQL = "UPDATE news SET title=?, short_text=?, full_text=?, creation_date=?, modification_date=? WHERE news_id=?";
	public static final String DELETE_COMMENTS_SQL = "DELETE FROM comments WHERE news_id = ?";
	public static final String DELETE_NEWS_AUTHOR_SQL = "DELETE FROM news_author WHERE news_id = ?";
	public static final String DELETE_NEWS_TAG_SQL = "DELETE FROM news_tag WHERE news_id = ?";
	public static final String SELECT_NEWS_SQL = "SELECT news_id,title,short_text,full_text,creation_date,modification_date FROM news WHERE news_id = ?";
	public static final String SELECT_LIST_OF_NEWS_SQL = "SELECT news.news_id,title,short_text,full_text,news.creation_date,news.modification_date,COUNT(comments.comment_id) cnt  FROM news LEFT JOIN comments ON news.news_id = comments.news_id ";
	public static final String JOIN_NEWS_AUTHOR_SQL = "news.news_id IN (SELECT news_author.news_id FROM news_author WHERE author_id=?) GROUP BY news.news_id,title,short_text,full_text,news.creation_date,modification_date ORDER BY cnt DESC NULLS LAST,news.modification_date DESC";
	public static final String JOIN_NEWS_TAG_SQL = " WHERE news.news_id IN (SELECT news_tag.news_id FROM news_tag WHERE tag_id IN (";
	public static final String NEW_TAG = "?,";
	public static final String AND_SQL = " AND ";
	public static final String WHERE_SQL = " WHERE ";

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Long addNews(News news) throws DAOException {
		Long newsId = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource); 
			preparedStatement = connection.prepareStatement(INSERT_NEWS_SQL,new String [] {"NEWS_ID"});
			preparedStatement.setNString(1, news.getTitle());
			preparedStatement.setNString(2, news.getShortText());
			preparedStatement.setNString(3, news.getFullText());
			preparedStatement.setTimestamp(4, news.getCreationDate());
			preparedStatement.setDate(5, news.getModificationDate());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if(resultSet.next()) {
				newsId = Long.parseLong(resultSet.getString(1));
			}

			return newsId;
		} catch (SQLException e) {
			throw new DAOException("Exception while adding news",e);
		} finally {
			ConnectionClose.close(connection,preparedStatement,dataSource,resultSet);
		}

	}

	public void deleteNews(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource); 
			preparedStatement = connection.prepareStatement(DELETE_NEWS_SQL);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			throw new DAOException("Exception while deleting news",e);

		} finally {
			ConnectionClose.close(connection,preparedStatement,dataSource);
		}

	}

	public News getNews(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		News news = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource); 
			preparedStatement = connection.prepareStatement(SELECT_NEWS_SQL);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				news = new News(
						resultSet.getLong("news_id"),
						resultSet.getNString("title"),
						resultSet.getNString("short_text"),
						resultSet.getNString("full_text"),
						resultSet.getTimestamp("creation_date"),
						resultSet.getDate("modification_date")
						);
			}
			return news;

		} catch (SQLException e) {

			throw new DAOException("Exception while getting news",e);

		} finally {
			ConnectionClose.close(connection,preparedStatement,dataSource);
		}
	}

	public void updateNews(News news) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource); 
			preparedStatement = connection.prepareStatement(UPDATE_NEWS_SQL);
			preparedStatement.setNString(1, news.getTitle());
			preparedStatement.setNString(2, news.getShortText());
			preparedStatement.setNString(3, news.getFullText());
			preparedStatement.setTimestamp(4, news.getCreationDate());
			preparedStatement.setDate(5, news.getModificationDate());
			preparedStatement.setLong(6, news.getNewsId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {

			throw new DAOException("Exception while getting news",e);

		} finally {
			ConnectionClose.close(connection,preparedStatement,dataSource);
		}
	}


	public List<News> getListOfNews(SearchCriteria searchCriteria) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<News> newsList = new ArrayList<News>();
		StringBuilder finalStatement = new StringBuilder();
		try {
			connection = DataSourceUtils.doGetConnection(dataSource); 

			finalStatement = finalStatement.append(SELECT_LIST_OF_NEWS_SQL);

			finalStatement = finalStatement.append(appendTags(searchCriteria));

			finalStatement = finalStatement.append(appendAuthor(searchCriteria));

			preparedStatement = connection.prepareStatement(finalStatement.toString());

			for(int i = 0; i  < searchCriteria.getTagIdList().size(); i++) {
				preparedStatement.setLong((i+1),searchCriteria.getTagIdList().get(i));
			}

			if(searchCriteria.getAuthorId() != null) {
				preparedStatement.setLong(searchCriteria.getTagIdList().size()+1,searchCriteria.getAuthorId());
			}

			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				newsList.add( 
						new News(
								resultSet.getLong("news_id"),
								resultSet.getNString("title"),
								resultSet.getNString("short_text"),
								resultSet.getNString("full_text"),
								resultSet.getTimestamp("creation_date"),
								resultSet.getDate("modification_date")
								)
						);
			}
		}catch (SQLException e) {
			throw new DAOException("Exception while getting news by searchCriteria",e);
		} finally {
			ConnectionClose.close(connection,preparedStatement,dataSource,resultSet);
		}
		return newsList;
	}

	private StringBuilder appendTags(SearchCriteria searchCriteria) {
		StringBuilder finalStatement = new StringBuilder();
		if(!searchCriteria.getTagIdList().isEmpty()) {

			finalStatement = finalStatement.append(JOIN_NEWS_TAG_SQL);

			for(int i = 0; i  < searchCriteria.getTagIdList().size(); i++) {
				finalStatement = finalStatement.append(NEW_TAG);
				if(i == searchCriteria.getTagIdList().size()-1) {
					finalStatement = finalStatement.deleteCharAt(finalStatement.length()-1);
					finalStatement = finalStatement.append("))");
				}
			}
		}
		return finalStatement;
	}

	private StringBuilder appendAuthor(SearchCriteria searchCriteria) {
		StringBuilder finalStatement = new StringBuilder();
		if(searchCriteria.getAuthorId() != null) {
			if(!searchCriteria.getTagIdList().isEmpty()) {
				finalStatement = finalStatement.append(AND_SQL);
			}else {
				finalStatement = finalStatement.append(WHERE_SQL);
			}
			finalStatement = finalStatement.append(JOIN_NEWS_AUTHOR_SQL);
		}
		return finalStatement;
	}

}
