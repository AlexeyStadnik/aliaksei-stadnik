package by.stadnik.epam.dao.impl;


import java.sql.Timestamp;

import org.dbunit.DBTestCase;
import org.dbunit.IDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.stadnik.epam.dao.AuthorDAO;
import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.entity.Author;
/**
 * @see AuthorDAOImpl
 * @author Aliaksei_Stadnik
 *
 */
@ContextConfiguration(locations = {"classpath:/applicationContextTest.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class AuthorDAOImplTest extends DBTestCase {

	@Autowired
	private  IDatabaseTester databaseTester;
	@Autowired
	private AuthorDAO authorDAO;

	@Override
	protected IDataSet getDataSet() throws Exception {
		IDataSet dataSet = new FlatXmlDataSetBuilder().build(this.getClass().getResourceAsStream(
				"/script/authorDAO-test.xml"
				));
		return dataSet;
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE_ALL;
	}

	@Before
	public void init() throws Exception  {
		databaseTester.setTearDownOperation(DatabaseOperation.DELETE_ALL);
		databaseTester.setDataSet(getDataSet());
		databaseTester.onSetup();
	}

	@After
	public void onTearDown() throws Exception {
		databaseTester.onTearDown();
	}




	@Test
	public void addAuthorTest() throws DAOException  {
		Author expectedAuthor = new Author();
		expectedAuthor.setAuthor_name("Test Name");
		expectedAuthor.setExpired(new Timestamp(1));
		Long authorId = authorDAO.addAuthor(expectedAuthor);
		Author newAuthor = authorDAO.getAuthorByAuthorId(authorId);
		assertAuthor(newAuthor, expectedAuthor);


	}

	@Test
	public void deleteAuthorTest() throws DAOException {
		Long authorId = Long.parseLong("10");
		Author author = authorDAO.getAuthorByAuthorId(authorId);
		Assert.assertNotNull(author.getAuthor_name());
		authorDAO.deleteAuthor(authorId);
		assertAuthorNull(authorDAO.getAuthorByAuthorId(authorId));
	}

	@Test
	public void updateAuthorTest() throws DAOException {
		Author expectedAuthor = new Author();
		expectedAuthor.setId(Long.parseLong("10"));
		expectedAuthor.setAuthor_name("New Test Name");
		expectedAuthor.setExpired(new Timestamp(2));
		authorDAO.updateAuthor(expectedAuthor);
		Author newAuthor = authorDAO.getAuthorByAuthorId(Long.parseLong("10"));
		assertAuthor(newAuthor,expectedAuthor);


	}

	@Test
	public void addNewsAuthorTest() throws DAOException {
		Long authorId = 30L;
		Long newsId = 30L;
		authorDAO.addNewsAuthor(authorId, newsId);
		Author author = authorDAO.getAuthorByNewsId(newsId);
		Assert.assertEquals(author.getId(), authorId);
	}

	@Test
	public void deleteNewsAuthorTest() throws DAOException {
		Long newsId = 20L;
		authorDAO.deleteNewsAuthor(newsId);
		Author author = authorDAO.getAuthorByNewsId(newsId);
		assertAuthorNull(author);

	}

	@Test
	public void getAuthorByAuthorIdTest() throws DAOException {
		Long authorId = 10L;
		Author author = authorDAO.getAuthorByAuthorId(authorId);
		assertAuthorNotNull(author);
	}

	@Test
	public void getAuthorByNewsIdTest() throws DAOException {
		Long newsId = 20L;
		Author author = authorDAO.getAuthorByNewsId(newsId);
		assertAuthorNotNull(author);
	}





	private void assertAuthor(Author newAuthor,Author expectedAuthor) {
		Assert.assertEquals(newAuthor.getAuthor_name(), expectedAuthor.getAuthor_name());
		Assert.assertEquals(newAuthor.getExpired(),expectedAuthor.getExpired() );
	}

	private void assertAuthorNull(Author author) {
		Assert.assertNull(author.getAuthor_name());
		Assert.assertNull(author.getExpired());
		Assert.assertNull(author.getId());
	}

	private void assertAuthorNotNull(Author author) {
		Assert.assertNotNull(author.getAuthor_name());
		Assert.assertNotNull(author.getExpired());
		Assert.assertNotNull(author.getId());
	}



}
