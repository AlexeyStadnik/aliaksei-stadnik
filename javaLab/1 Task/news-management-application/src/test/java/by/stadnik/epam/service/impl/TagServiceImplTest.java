package by.stadnik.epam.service.impl;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;


import static org.mockito.Mockito.*;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.dao.TagDAO;
import by.stadnik.epam.entity.Tag;
import by.stadnik.epam.service.ServiceException;
import by.stadnik.epam.service.TagService;

@ContextConfiguration(locations = {"classpath:/applicationContextTest.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class TagServiceImplTest {

	@Mock
	private TagDAO mockTagDAO;

	@InjectMocks
	@Autowired
	private TagService tagService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		assertNotNull(tagService);
	}

	@Test
	public void deleteTagTest() throws ServiceException, DAOException {
		Long tagId = null;

		tagService.deleteTag(tagId);

		verify(mockTagDAO,times(1)).deleteTag(tagId);
		verifyNoMoreInteractions(mockTagDAO);
	}

	@Test
	public void addTagTest() throws ServiceException, DAOException {
		Tag mockTag  = mock(Tag.class);

		tagService.addTag(mockTag);

		verify(mockTagDAO,times(1)).addTag(mockTag);
		verifyNoMoreInteractions(mockTagDAO);

	}

	@Test
	public void addNewsTagTest() throws ServiceException, DAOException {
		Long tagId = null;
		Long newsId = null;
		List <Long> tagList = new ArrayList<Long>();
		tagList.add(tagId);

		tagService.addNewsTag(tagList, newsId);
		verify(mockTagDAO,times(1)).addNewsTag(tagList, newsId);
		verifyNoMoreInteractions(mockTagDAO);

	}

	@Test
	public void deleteNewsTagByNewsIdTest() throws ServiceException, DAOException {
		Long newsId = null;

		tagService.deleteNewsTagByNewsId(newsId);
		verify(mockTagDAO,times(1)).deleteNewsTagByNewsId(newsId);
		verifyNoMoreInteractions(mockTagDAO);
	}

	@Test
	public void deleteNewsTagByTagIdTest() throws ServiceException, DAOException {
		Long tagId = null;

		tagService.deleteNewsTagByTagId(tagId);
		verify(mockTagDAO,times(1)).deleteNewsTagByTagId(tagId);
		verifyNoMoreInteractions(mockTagDAO);
	}

	@Test
	public void getTagTest() throws ServiceException, DAOException {
		Long newsId  = null;

		tagService.getTag(newsId);
		verify(mockTagDAO,times(1)).getTag(newsId);
		verifyNoMoreInteractions(mockTagDAO);

	}

	@Test
	public void updateTagTest() throws ServiceException, DAOException {
		Tag mockTag = mock(Tag.class);

		tagService.updateTag(mockTag);
		verify(mockTagDAO,times(1)).updateTag(mockTag);
		verifyNoMoreInteractions(mockTagDAO);
	}

	@Test
	public void getTagByTagIdTest() throws ServiceException, DAOException {
		Long tagId = null;

		tagService.getTagByTagId(tagId);
		verify(mockTagDAO,times(1)).getTagByTagId(tagId);
		verifyNoMoreInteractions(mockTagDAO);
	}

}
