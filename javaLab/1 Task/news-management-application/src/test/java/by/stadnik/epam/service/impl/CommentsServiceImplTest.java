package by.stadnik.epam.service.impl;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;


import static org.mockito.Mockito.*;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.stadnik.epam.dao.CommentsDAO;
import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.entity.Comment;
import by.stadnik.epam.service.CommentsService;
import by.stadnik.epam.service.ServiceException;

@ContextConfiguration(locations = {"classpath:/applicationContextTest.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class CommentsServiceImplTest {

	@Mock
	private CommentsDAO mockCommentsDAO;

	@InjectMocks
	@Autowired
	private CommentsService commentsService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		assertNotNull(commentsService);
	}

	@Test
	public void addCommentTest() throws ServiceException, DAOException {
		Comment mockComment = mock(Comment.class);

		commentsService.addComment(mockComment);

		verify(mockCommentsDAO,times(1)).addComment(mockComment);
		verifyNoMoreInteractions(mockCommentsDAO);


	}

	@Test
	public void deleteCommentByCommentIdTest() throws ServiceException, DAOException {

		Long commentId = null;

		commentsService.deleteCommentByCommentId(commentId);
		verify(mockCommentsDAO,times(1)).deleteCommentByCommentId(commentId);
		verifyNoMoreInteractions(mockCommentsDAO);
	}

	@Test
	public void deleteCommentByNewsIdTest() throws ServiceException, DAOException {

		Long newsId = null;

		commentsService.deleteCommentsByNewsId(newsId);
		verify(mockCommentsDAO,times(1)).deleteCommentsByNewsId(newsId);
		verifyNoMoreInteractions(mockCommentsDAO);
	}

	@Test
	public void updateCommentTest() throws ServiceException, DAOException {
		Comment mockComment = mock(Comment.class);

		commentsService.updateComment(mockComment);
		verify(mockCommentsDAO,times(1)).updateComment(mockComment);
		verifyNoMoreInteractions(mockCommentsDAO);



	}

	@Test
	public void getCommentsTest() throws ServiceException, DAOException {

		Long newsId = null;

		commentsService.getComments(newsId);
		verify(mockCommentsDAO,times(1)).getComments(newsId);
		verifyNoMoreInteractions(mockCommentsDAO);

	}

	@Test
	public void getCommentByCommentIdTest() throws ServiceException, DAOException {

		Long commentId = null;

		commentsService.getCommentByCommentId(commentId);
		verify(mockCommentsDAO,times(1)).getCommentByCommentId(commentId);
		verifyNoMoreInteractions(mockCommentsDAO);

	}

}
