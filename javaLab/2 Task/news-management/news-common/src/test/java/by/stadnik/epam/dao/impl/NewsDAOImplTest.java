package by.stadnik.epam.dao.impl;


import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.dbunit.DBTestCase;
import org.dbunit.IDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.dao.NewsDAO;
import by.stadnik.epam.entity.News;
import by.stadnik.epam.entity.NewsVO;
import by.stadnik.epam.util.SearchCriteria;

@ContextConfiguration(locations = {"classpath:/applicationContextTest.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class NewsDAOImplTest extends DBTestCase {

	@Autowired
	private  IDatabaseTester databaseTester;
	@Autowired
	private NewsDAO newsDAO;

	@Override
	protected IDataSet getDataSet() throws Exception {
		IDataSet dataSet = new FlatXmlDataSetBuilder().build(this.getClass().getResourceAsStream(
				"/script/newsDAO-test.xml"
				));
		return dataSet;
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE_ALL;
	}

	@Before
	public void init() throws Exception  {

		databaseTester.setTearDownOperation(DatabaseOperation.DELETE_ALL);
		databaseTester.setDataSet(getDataSet());
		databaseTester.onSetup();

	}

	@After
	public void onTearDown() throws Exception {
		databaseTester.onTearDown();
	}

	@Test
	public void getNewsTest() throws DAOException {
		News news = newsDAO.getNews(10L);
		Assert.assertNotNull(news);
		Assert.assertNotNull(news.getFullText());
	}

	@Test
	public void addNewsTest() throws DAOException {
		News expectedNews = new News();
		expectedNews.setTitle("Expected title");
		expectedNews.setShortText("Expected short text");
		expectedNews.setFullText("Expected full text");
		expectedNews.setCreationDate(new Timestamp(1L));
		expectedNews.setModificationDate(new Date(1L));
		Long newsId = newsDAO.addNews(expectedNews);
		News actualNews = newsDAO.getNews(newsId);
		assertNewsEquals(actualNews,expectedNews);


	}
	
	@Test
	public void updateNewsTest() throws DAOException {
		Long newsId = 10L;
		News expectedNews = new News();
		expectedNews.setNewsId(newsId);
		expectedNews.setTitle("Expected title");
		expectedNews.setShortText("Expected short text");
		expectedNews.setFullText("Expected full text");
		expectedNews.setCreationDate(new Timestamp(1L));
		expectedNews.setModificationDate(new Date(1L));
		newsDAO.updateNews(expectedNews);
		News actualNews = newsDAO.getNews(newsId);
		assertNewsEquals(actualNews,expectedNews);
	}

	@Test
	public void deleteNewsTest() throws DAOException {
		Long newsId = 10L;
		newsDAO.deleteNews(newsId);
		News news = newsDAO.getNews(newsId);
		Assert.assertNull(news);

	}
	@Test
	public void getListOfNewsTest() throws DAOException {
		List<NewsVO> newsList = null;
		SearchCriteria searchCriteria = new SearchCriteria();
		List<Long> tagList = new ArrayList<Long>();
		tagList.add(10L);
		tagList.add(20L);
		searchCriteria.setTagIdList(tagList);
		searchCriteria.setAuthorId(10L);
		newsList = newsDAO.getListOfNews(searchCriteria,0,3);
		Assert.assertEquals(newsList.size(),2);


	}

	private void assertNewsEquals(News actualNews, News expectedNews ) {
		Assert.assertEquals(actualNews.getTitle(), expectedNews.getTitle());
		Assert.assertEquals(actualNews.getShortText(), expectedNews.getShortText());
		Assert.assertEquals(actualNews.getFullText(), expectedNews.getFullText());
		Assert.assertEquals(actualNews.getCreationDate(), expectedNews.getCreationDate());

	}

}
