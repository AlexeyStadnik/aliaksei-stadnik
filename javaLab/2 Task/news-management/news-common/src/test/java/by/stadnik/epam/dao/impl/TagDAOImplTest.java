package by.stadnik.epam.dao.impl;


import java.util.ArrayList;
import java.util.List;

import org.dbunit.DBTestCase;
import org.dbunit.IDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.dao.TagDAO;
import by.stadnik.epam.entity.Tag;

@ContextConfiguration(locations = {"classpath:/applicationContextTest.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class TagDAOImplTest extends DBTestCase  {

	@Autowired
	private  IDatabaseTester databaseTester;
	@Autowired
	private TagDAO tagDAO;

	@Override
	protected IDataSet getDataSet() throws Exception {
		IDataSet dataSet = new FlatXmlDataSetBuilder().build(this.getClass().getResourceAsStream(
				"/script/tagDAO-test.xml"
				));
		return dataSet;
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE_ALL;
	}

	@Before
	public void init() throws Exception  {

		databaseTester.setTearDownOperation(DatabaseOperation.DELETE_ALL);
		databaseTester.setDataSet(getDataSet());
		databaseTester.onSetup();
	}

	@After
	public void onTearDown() throws Exception {
		databaseTester.onTearDown();
	}

	@Test
	public void deleteTagTest() throws DAOException {
		Long tagId = 10L;
		tagDAO.deleteTag(tagId);
		Tag tag = tagDAO.getTagByTagId(tagId);
		Assert.assertNull(tag);
	}

	@Test
	public void addTagTest() throws DAOException {
		Tag expectedTag = new Tag();
		expectedTag.setTagName("Test tag");
		Long tagId = tagDAO.addTag(expectedTag);
		Tag newTag = tagDAO.getTagByTagId(tagId);
		Assert.assertEquals(expectedTag.getTagName(), newTag.getTagName());
	}

	@Test
	public void updateTagTest() throws DAOException {
		Tag expectedTag = new Tag(10L,"Updated text");
		tagDAO.updateTag(expectedTag);
		Tag newTag = tagDAO.getTagByTagId(10L);
		Assert.assertEquals(expectedTag.getTagName(), newTag.getTagName());
	}

	@Test
	public void addNewsTagTest() throws DAOException {
		Long newsId = 20L;
		Long tagId = 20L;
		List<Long> tagIdList = new ArrayList<Long>();
		tagIdList.add(tagId);
		tagDAO.addNewsTag(tagIdList, newsId);
		Tag tag = tagDAO.getTagByTagId(tagId);
		Assert.assertNotNull(tag.getTagName());
	}

	@Test
	public void deleteNewsTagByNewsIdTest() throws DAOException {
		Long newsId = 20L;
		tagDAO.deleteNewsTagByNewsId(newsId);
		List<Tag> tagList = tagDAO.getTag(newsId);
		Assert.assertEquals(0, tagList.size());
	}

	@Test
	public void deleteNewsTagByTagIdTest() throws DAOException {
		Long tagId = 30L;
		Long newsId = 30L;
		tagDAO.deleteNewsTagByTagId(tagId);
		List<Tag> tagList = tagDAO.getTag(newsId);
		Assert.assertEquals(2, tagList.size());
	}

	@Test
	public void getTagByTagIdTest() throws DAOException {
		Long tagId = 20L;
		Tag tag = tagDAO.getTagByTagId(tagId);
		Assert.assertNotNull(tag.getTagName());
	}

	@Test
	public void getTagTest() throws DAOException {
		Long newsId = 30L;
		List<Tag> tagList = tagDAO.getTag(newsId);
		Assert.assertEquals(tagList.size(), 3);
	}
	
	@Test
	public void getAllTagsTest() throws DAOException {
		int tagListSize = 5;
		List<Tag> tagList = tagDAO.getAllTags();
		Assert.assertEquals(tagList.size(), tagListSize);
	}



}
