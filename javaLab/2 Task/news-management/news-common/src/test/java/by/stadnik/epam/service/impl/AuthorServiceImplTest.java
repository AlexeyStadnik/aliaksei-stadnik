package by.stadnik.epam.service.impl;



import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;





import by.stadnik.epam.dao.AuthorDAO;
import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.entity.Author;
import by.stadnik.epam.service.AuthorService;
import by.stadnik.epam.service.ServiceException;


@ContextConfiguration(locations = {"classpath:/applicationContextTest.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class AuthorServiceImplTest {

	@Mock 
	private AuthorDAO mockAuthorDAO;

	@InjectMocks
	@Autowired
	private AuthorService authorService;



	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		assertNotNull(authorService);
	}

	@Test
	public void addAuthorTest() throws DAOException, ServiceException {

		Author mockAuthor = mock(Author.class);
		authorService.addAuthor(mockAuthor);
		verify(mockAuthorDAO,times(1)).addAuthor(mockAuthor);
		verifyNoMoreInteractions(mockAuthorDAO);


	}

	@Test
	public void addNewsAuthorTest() throws ServiceException, DAOException {

		Long firstId = null;
		Long secondId = null;

		authorService.addNewsAuthor(firstId, secondId);
		verify(mockAuthorDAO,times(1)).addNewsAuthor(firstId,secondId);
		verifyNoMoreInteractions(mockAuthorDAO);


	}

	@Test
	public void deleteAuthorTest() throws ServiceException, DAOException {

		Long authorId = null;

		authorService.deleteAuthor(authorId);
		verify(mockAuthorDAO,times(1)).deleteAuthor(authorId);
		verifyNoMoreInteractions(mockAuthorDAO);
	}

	@Test
	public void deleteNewsAuthorTest() throws ServiceException, DAOException {

		Long newId = null;


		authorService.deleteNewsAuthor(newId);
		verify(mockAuthorDAO,times(1)).deleteNewsAuthor(newId);
		verifyNoMoreInteractions(mockAuthorDAO);
	}

	@Test
	public void getAuthorByNewsIdTest() throws ServiceException, DAOException {

		Long newsId = null;

		authorService.getAuthorByNewsId(newsId);
		verify(mockAuthorDAO,times(1)).getAuthorByNewsId(newsId);
		verifyNoMoreInteractions(mockAuthorDAO);

	}

	@Test
	public void getAuthorByAuthorIdTest() throws ServiceException, DAOException {

		Long authorId = null;

		authorService.getAuthorByAuthorId(authorId);
		verify(mockAuthorDAO,times(1)).getAuthorByAuthorId(authorId);
		verifyNoMoreInteractions(mockAuthorDAO);

	}

	@Test
	public void updateAuthorTest() throws ServiceException, DAOException {

		Author author = mock(Author.class);

		authorService.updateAuthor(author);
		verify(mockAuthorDAO,times(1)).updateAuthor(author);
		verifyNoMoreInteractions(mockAuthorDAO);


	}
	
	@Test
	public void getAllAuthorsTest() throws ServiceException, DAOException {
		authorService.getAllAuthors();
		verify(mockAuthorDAO,times(1)).getAllAuthors();
		verifyNoMoreInteractions(mockAuthorDAO);
	}



}
