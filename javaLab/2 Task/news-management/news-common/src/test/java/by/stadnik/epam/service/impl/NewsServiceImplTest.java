package by.stadnik.epam.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;






import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.dao.NewsDAO;
import by.stadnik.epam.entity.News;
import by.stadnik.epam.service.NewsService;
import by.stadnik.epam.service.ServiceException;
import by.stadnik.epam.util.SearchCriteria;

@ContextConfiguration(locations = {"classpath:/applicationContextTest.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class NewsServiceImplTest {

	@Mock
	private NewsDAO mockNewsDAO;

	@InjectMocks
	@Autowired
	private NewsService newsService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		assertNotNull(newsService);
	}

	@Test
	public void addNewsTest() throws ServiceException, DAOException {

		News mockNews = mock(News.class);

		newsService.addNews(mockNews);

		verify(mockNewsDAO,times(1)).addNews(mockNews);
		verifyNoMoreInteractions(mockNewsDAO);
	}
	
	@Test
	public void updateNewsTest() throws ServiceException, DAOException {
		News mockNews = mock(News.class);
		
		newsService.updateNews(mockNews);
		verify(mockNewsDAO,times(1)).updateNews(mockNews);
		verifyNoMoreInteractions(mockNewsDAO);
	}

	@Test
	public void deleteNewsTest() throws ServiceException, DAOException {
		Long newsId = null;

		newsService.deleteNews(newsId);

		verify(mockNewsDAO,times(1)).deleteNews(newsId);
		verifyNoMoreInteractions(mockNewsDAO);

	}

	@Test
	public void getNewsTest() throws ServiceException, DAOException {
		Long newsId = null;
		newsService.getNews(newsId);
		verify(mockNewsDAO,times(1)).getNews(newsId);
		verifyNoMoreInteractions(mockNewsDAO);
	}

	@Test
	public void getListOfNewsTest() throws ServiceException, DAOException {
		SearchCriteria searchCriteria = mock(SearchCriteria.class);
		newsService.getListOfNews(searchCriteria,0,3);
		verify(mockNewsDAO,times(1)).getListOfNews(searchCriteria,0,3);
		verifyNoMoreInteractions(mockNewsDAO);
	}


}
