package by.stadnik.epam.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.stadnik.epam.entity.Author;
import by.stadnik.epam.entity.News;
import by.stadnik.epam.entity.NewsFormVO;
import by.stadnik.epam.entity.NewsVO;
import by.stadnik.epam.service.AuthorService;
import by.stadnik.epam.service.CommentsService;
import by.stadnik.epam.service.NewsService;
import by.stadnik.epam.service.ServiceException;
import by.stadnik.epam.service.ServiceManager;
import by.stadnik.epam.service.TagService;

@ContextConfiguration(locations = {"classpath:/applicationContextTest.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class ServiceManagerImplTest {

	@Mock
	private AuthorService authorService;
	@Mock
	private TagService tagService;
	@Mock
	private CommentsService commentsService;
	@Mock
	private NewsService newsService;


	@InjectMocks
	@Autowired
	private ServiceManager serviceManager;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		assertNotNull(serviceManager);
	}

	@Test
	public void addNewsTest() throws ServiceException {

		NewsFormVO mockNews = mock(NewsFormVO.class);
		
		InOrder inOrder = inOrder(newsService,tagService,authorService);
		serviceManager.addNews(mockNews);
		inOrder.verify(newsService).addNews(any(News.class));
		inOrder.verify(tagService).addNewsTag(any(List.class), any(Long.class));
		inOrder.verify(authorService).addNewsAuthor( any(Long.class), any(Long.class));
		verifyNoMoreInteractions(newsService);
		verifyNoMoreInteractions(tagService);
		verifyNoMoreInteractions(authorService);
	}

	@Test
	public void deleteNewsTest()  throws ServiceException {
		Long newsId = 1L;
		InOrder inOrder = inOrder(authorService,tagService,commentsService,newsService);
		serviceManager.deleteNews(newsId);
		inOrder.verify(authorService).deleteNewsAuthor(newsId);
		inOrder.verify(tagService,times(1)).deleteNewsTagByNewsId(newsId);
		inOrder.verify(commentsService,times(1)).deleteCommentsByNewsId(newsId);
		inOrder.verify(newsService,times(1)).deleteNews(newsId);
		verifyNoMoreInteractions(authorService);
		verifyNoMoreInteractions(tagService);
		verifyNoMoreInteractions(commentsService);
		verifyNoMoreInteractions(newsService);
	}



	@Test
	public void getNewsVOTest() throws ServiceException {
		Long newsId = 1L;
		serviceManager.getNewsVO(newsId);
		verify(newsService).getNews(newsId);
		verify(tagService).getTag(newsId);
		verify(commentsService).getComments(newsId);
		verify(authorService).getAuthorByNewsId(newsId);

		verifyNoMoreInteractions(newsService);
		verifyNoMoreInteractions(tagService);
		verifyNoMoreInteractions(commentsService);
		verifyNoMoreInteractions(authorService);
	}





}
