package by.stadnik.epam.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.dao.TagDAO;
import by.stadnik.epam.entity.Tag;
import by.stadnik.epam.service.ServiceException;
import by.stadnik.epam.service.TagService;

public class TagServiceImpl implements TagService {
	
	private TagDAO tagDAO;
	
	private static final Logger logger = LoggerFactory.getLogger(TagServiceImpl.class);
	
	
	public void setTagDAO(TagDAO tagDao) {
		this.tagDAO = tagDao;
	}

	public void deleteTag(Long tagId) throws ServiceException {
		try {
			tagDAO.deleteTag(tagId);
		} catch (DAOException e) {
			logger.error("Exception while tag deleting process",e);
			throw new ServiceException("Exception while tag deleting process",e);
		}
		
	}

	public Long addTag(Tag tag) throws ServiceException {
		Long tagId = null;
		try {
			tagId = tagDAO.addTag(tag);
		} catch (DAOException e) {
			logger.error("Exception while tag adding process",e);
			throw new ServiceException("Exception while tag adding process",e);
		}
		return tagId;
	}

	public void addNewsTag(List<Long> tagList, Long newsId) throws ServiceException {
		try {
			tagDAO.addNewsTag(tagList, newsId);
		} catch (DAOException e) {
			logger.error("Exception while tag_news adding process",e);
			throw new ServiceException("Exception while tag_news adding process",e);
		}
		
	}

	public void deleteNewsTagByNewsId(Long newsId) throws ServiceException {
		try {
			tagDAO.deleteNewsTagByNewsId(newsId);
		} catch (DAOException e) {
			logger.error("Exception while tag_news deleting process",e);
			throw new ServiceException("Exception while tag_news deleting process",e);
		}
		
	}
	
	public void deleteNewsTagByTagId(Long tagId) throws ServiceException {
		try {
			tagDAO.deleteNewsTagByTagId(tagId);
		} catch (DAOException e) {
			logger.error("Exception while tag_news deleting process",e);
			throw new ServiceException("Exception while tag_news deleting process",e);
		}
		
	}

	public List<Tag> getTag(Long newsId) throws ServiceException {
		List<Tag> tagList = null;
		try {
			tagList = tagDAO.getTag(newsId);
		} catch (DAOException e) {
			logger.error("Exception while tags geting process",e);
			throw new ServiceException("Exception while tags geting process",e);
		}
		return tagList;
	}

	public void updateTag(Tag tag) throws ServiceException {
		try {
			tagDAO.updateTag(tag);
		} catch (DAOException e) {
			logger.error("Exception while tags updating process",e);
			throw new ServiceException("Exception while tags updating process",e);
		}
		
	}

	public Tag getTagByTagId(Long tagId) throws ServiceException {
		Tag tag = null;
		
		try {
			tag = tagDAO.getTagByTagId(tagId);
		} catch (DAOException e) {
			logger.error("Exception while tags getting process",e);
			throw new ServiceException("Exception while tags getting process",e);
		}
		return tag;
	}
	public List<Tag> getAllTag() throws ServiceException {
		List <Tag> tagList = null;
		try {
			tagList = tagDAO.getAllTags();
		} catch (DAOException e) {
			logger.error("Exception while tags getting process",e);
			throw new ServiceException("Exception while tags getting process",e);
		}
		return tagList;
	}

}
