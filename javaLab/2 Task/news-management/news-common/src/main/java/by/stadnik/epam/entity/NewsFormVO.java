package by.stadnik.epam.entity;

import java.util.ArrayList;
import java.util.List;

public class NewsFormVO {
	
	private News news;
	private Long authorId;
	private List<Long> tagIdList;
	private int commentNumber;
	
	public NewsFormVO() {
		
	}

	public NewsFormVO(News news, Long authorId, List<Long> tagIdList,
			int commentNumber) {
		super();
		this.news = news;
		this.authorId = authorId;
		this.tagIdList = tagIdList;
		this.commentNumber = commentNumber;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public List<Long> getTagIdList() {
		return tagIdList;
	}

	public void setTagIdList(List<Long> tagIdList) {
		this.tagIdList = tagIdList;
	}

	public int getCommentNumber() {
		return commentNumber;
	}

	public void setCommentNumber(int commentNumber) {
		this.commentNumber = commentNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result + commentNumber;
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		result = prime * result
				+ ((tagIdList == null) ? 0 : tagIdList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsFormVO other = (NewsFormVO) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		if (commentNumber != other.commentNumber)
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		if (tagIdList == null) {
			if (other.tagIdList != null)
				return false;
		} else if (!tagIdList.equals(other.tagIdList))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NewsFormVO [news=" + news + ", authorId=" + authorId
				+ ", tagIdList=" + tagIdList + ", commentNumber="
				+ commentNumber + "]";
	}
	
	
	
	
}
