package by.stadnik.epam.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;


import org.springframework.jdbc.datasource.DataSourceUtils;

import by.stadnik.epam.dao.CommentsDAO;
import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.entity.Comment;
import by.stadnik.epam.util.ConnectionClose;


public class CommentsDAOImpl  implements CommentsDAO {
	
	private DataSource dataSource;
	
	public static final String INSERT_COMMENT_SQL = "INSERT INTO comments (comment_id,news_id,comment_text,creation_date) VALUES (comment_seq.NEXTVAL,?, ?, ?)";
	public static final String DELETE_COMMENT_BY_COMMENT_ID_SQL = "DELETE FROM comments WHERE comment_id = ?";
	public static final String DELETE_COMMENT_BY_NEWS_ID_SQL = "DELETE FROM comments WHERE news_id = ?";
	public static final String UPDATE_COMMENT_SQL = "UPDATE comments SET comment_text=? WHERE comment_id=?";
	public static final String SELECT_COMMENTS_BY_NEWS_ID_SQL = "SELECT comment_id,news_id,comment_text,creation_date FROM comments WHERE news_id = ? ORDER BY creation_date DESC";
	public static final String SELECT_COMMENTS_BY_COMMENT_ID_SQL = "SELECT comment_id,news_id,comment_text,creation_date FROM comments WHERE comment_id = ? ";
	
	public static final String PARAM_COMMENT_ID = "COMMENT_ID";
	public static final String PARAM_NEWS_ID = "NEWS_ID";
	public static final String PARAM_COMMENT_TEXT = "COMMENT_TEXT";
	public static final String PARAM_CREATION_DATE = "CREATION_DATE";
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Long addComment(Comment comment) throws DAOException {
		Long commentId = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource); 
			preparedStatement = connection.prepareStatement(INSERT_COMMENT_SQL,new String [] {"comment_id"});
			preparedStatement.setLong(1, comment.getNewsId());
			preparedStatement.setNString(2, comment.getCommentText());
			preparedStatement.setTimestamp(3, comment.getCreationDate());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if(resultSet.next()) {
				commentId = Long.parseLong(resultSet.getString(1));
			}
			
		} catch (SQLException e) {
			throw new DAOException("Exception while comment adding",e);
		}finally {
			ConnectionClose.close(connection, preparedStatement,dataSource,resultSet);
		}
		return commentId;
	}


	public void deleteCommentByCommentId(Long commentId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
		connection = DataSourceUtils.doGetConnection(dataSource); 
		preparedStatement = connection.prepareStatement(DELETE_COMMENT_BY_COMMENT_ID_SQL);
		preparedStatement.setLong(1, commentId);
		preparedStatement.executeUpdate();
		} catch(SQLException e) {
			throw new DAOException("Exception while comments deleting",e);
		}finally {
			ConnectionClose.close(connection, preparedStatement,dataSource);
		}
	}

	public void deleteCommentsByNewsId(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(DELETE_COMMENT_BY_NEWS_ID_SQL);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch(SQLException e) {
			throw new DAOException("Exception while comments deleting",e);
		}finally {
			ConnectionClose.close(connection,preparedStatement,dataSource);
		}
		
	}


	public void updateComment(Comment comment) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource); 
			preparedStatement = connection.prepareStatement(UPDATE_COMMENT_SQL);
			preparedStatement.setString(1, comment.getCommentText());
			preparedStatement.setLong(2, comment.getCommentId());
			preparedStatement.executeUpdate();
		} catch(SQLException e) {
			throw new DAOException("Exception while comments deleting",e);
		}finally {
			ConnectionClose.close(connection, preparedStatement,dataSource);
		}
		
	}
	
	public Comment getCommentByCommentId(Long commentId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Comment comment = null;
		
		try {
			connection = DataSourceUtils.doGetConnection(dataSource); 
			preparedStatement = connection.prepareStatement(SELECT_COMMENTS_BY_COMMENT_ID_SQL);
			preparedStatement.setLong(1, commentId);
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
					
							comment = new Comment(
									resultSet.getLong(PARAM_COMMENT_ID),
									resultSet.getLong(PARAM_NEWS_ID),
									resultSet.getNString(PARAM_COMMENT_TEXT),
									resultSet.getTimestamp(PARAM_CREATION_DATE)
									);
						
			}
			return comment;
			} catch(SQLException e) {
				throw new DAOException("Exception while comment getting",e);
			}finally {
				ConnectionClose.close(connection, preparedStatement,dataSource,resultSet);
			}
		
		
	}


	public List<Comment> getComments(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Comment> commentList = new ArrayList<Comment>();
		try {
		connection = DataSourceUtils.doGetConnection(dataSource); 
		preparedStatement = connection.prepareStatement(SELECT_COMMENTS_BY_NEWS_ID_SQL);
		preparedStatement.setLong(1, newsId);
		resultSet = preparedStatement.executeQuery();
		while(resultSet.next()) {
			commentList.add(	
						new Comment(
								resultSet.getLong(PARAM_COMMENT_ID),
								resultSet.getLong(PARAM_NEWS_ID),
								resultSet.getNString(PARAM_COMMENT_TEXT),
								resultSet.getTimestamp(PARAM_CREATION_DATE)
								)
					);
		}
		return commentList;
		} catch(SQLException e) {
			throw new DAOException("Exception while comments deleting",e);
		}finally {
			ConnectionClose.close(connection, preparedStatement,dataSource,resultSet);
		}
	}

}
