package by.stadnik.epam.dao;

import java.util.List;

import by.stadnik.epam.dao.impl.TagDAOImpl;
import by.stadnik.epam.dao.impl.TagDAOImplTest;
import by.stadnik.epam.entity.Tag;
/**
 * Interface provides methods to work with TAG table
 * @see Tag
 * @see TagDAOImpl - implementation
 * @see TagDAOImplTest - test class for the impelemtation {@link TagDAOImpl}
 * @author Aliaksei_Stadnik
 *
 */
public interface TagDAO {
	
	public void deleteTag(Long tagId) throws DAOException;
	public Long addTag(Tag tag) throws DAOException;
	public void addNewsTag(List<Long> tagList,Long newsId) throws DAOException;
	public void deleteNewsTagByNewsId(Long newsId) throws DAOException;
	public void deleteNewsTagByTagId(Long tagId) throws DAOException;
	public List<Tag> getTag(Long newsId) throws DAOException;
	public Tag getTagByTagId(Long tagId) throws DAOException;
	public void updateTag(Tag tag) throws DAOException;
	public List<Tag> getAllTags() throws DAOException;
	

}
