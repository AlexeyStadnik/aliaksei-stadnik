package by.stadnik.epam.dao;


import java.util.List;

import org.dbunit.util.search.SearchException;

import by.stadnik.epam.dao.impl.NewsDAOImpl;
import by.stadnik.epam.dao.impl.NewsDAOImplTest;
import by.stadnik.epam.entity.News;
import by.stadnik.epam.entity.NewsVO;
import by.stadnik.epam.util.SearchCriteria;
/**
 * Interface to work with NEWS table
 * @see News
 * @see NewsDAOImpl - implementation
 * @see NewsDAOImplTest - test class for implementation
 * @author Aliaksei_Stadnik
 *
 */
public interface NewsDAO {
	
	public Long addNews(News news) throws DAOException;
	public void deleteNews(Long newsId) throws DAOException;
	public News getNews(Long newsId) throws DAOException;
	public void updateNews(News news) throws DAOException;
	public int getNumberOfNews(SearchCriteria searchCriteria) throws DAOException;
	/**
	 * Method for getting news by filter
	 * It's possible to filter news by tags and/or author
	 * If it's necessary to get list of all news one can use empty searchCriteria object
	 * @param searchCriteria - {@link SearchCriteria}  - filter class
	 * @return - List of {@link News} according filter
	 * @throws DAOException
	 */
	public List<NewsVO> getListOfNews(SearchCriteria searchCriteria,int firstIndex,int lastIndex) throws DAOException;
	
	
	
}
