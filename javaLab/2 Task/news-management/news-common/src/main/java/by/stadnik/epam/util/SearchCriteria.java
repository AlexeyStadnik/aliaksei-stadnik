package by.stadnik.epam.util;

import java.util.List;

public class SearchCriteria {

	private Long authorId;
	private List<Long> tagIdList;

	public SearchCriteria() {
	}

	public SearchCriteria(Long authorId, List<Long> tagIdList) {
		this.authorId = authorId;
		this.tagIdList = tagIdList;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public List<Long> getTagIdList() {
		return tagIdList;
	}

	public void setTagIdList(List<Long> tagIdList) {
		this.tagIdList = tagIdList;
	}

	@Override
	public String toString() {
		return "SearchCriteria [authorId=" + authorId + ", tagIdList="
				+ tagIdList + "]";
	}
	
	



}
