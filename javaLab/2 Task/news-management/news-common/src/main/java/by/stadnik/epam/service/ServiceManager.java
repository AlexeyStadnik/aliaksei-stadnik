package by.stadnik.epam.service;

import java.util.List;

import by.stadnik.epam.entity.Author;
import by.stadnik.epam.entity.News;
import by.stadnik.epam.entity.NewsFormVO;
import by.stadnik.epam.entity.NewsVO;
import by.stadnik.epam.service.impl.ServiceManagerImpl;
import by.stadnik.epam.service.impl.ServiceManagerImplTest;
import by.stadnik.epam.util.SearchCriteria;

/**
 * Interface provides complicated methods which required multiple usages of service methods and transactions
 * @see ServiceManagerImpl - implemenation
 * @see ServiceManagerImplTest - test class
 * @author Aliaksei_Stadnik
 *
 */
public interface ServiceManager {

	Long addNews(NewsFormVO newsVO) throws ServiceException;
	void deleteNews(Long newsId) throws ServiceException;
	NewsVO getNewsVO(Long newsId) throws ServiceException;
	NewsFormVO getNewsFormVO(Long newsId) throws ServiceException;
	void updateTags(Long newsId,List<Long> tagList) throws ServiceException; 
	void updateAuthor(Long newsId,Long authorId) throws ServiceException;
	List<NewsVO>getListOfNews(SearchCriteria searchCriteria,int firstIndex,int lastIndex) throws ServiceException;
	Long updateNewsFormVO(NewsFormVO newsFormVO) throws ServiceException ;
	
}
