package by.stadnik.epam.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import by.stadnik.epam.dao.AuthorDAO;
import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.entity.Author;
import by.stadnik.epam.service.AuthorService;
import by.stadnik.epam.service.ServiceException;


public class AuthorServiceImpl implements AuthorService {

	private AuthorDAO authorDAO;
	private static final Logger logger = LoggerFactory.getLogger(AuthorServiceImpl.class);


	public void setAuthorDAO(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}

	public Long addAuthor(Author author) throws ServiceException {
		Long authorId = null;

		try {
			authorId = authorDAO.addAuthor(author);

		} catch (DAOException e) {
			logger.error("Exception while author adding process",e);
			throw new ServiceException("Exception while author adding process",e);
		}

		return authorId;
	}

	public void deleteAuthor(Long authorId) throws ServiceException {
		try {
			authorDAO.deleteAuthor(authorId);
		} catch (DAOException e) {
			logger.error("Exception while author deliting process",e);
			throw new ServiceException("Exception while author deliting process",e);
		}

	}

	public void addNewsAuthor(Long authorId, Long newsId) throws ServiceException {

		try {
			authorDAO.addNewsAuthor(authorId, newsId);
		} catch (DAOException e) {
			logger.error("Exception while news_author adding process",e);
			throw new ServiceException("Exception while news_author adding process",e);
		}

	}

	public void deleteNewsAuthor(Long newsId) throws ServiceException {
		try {
			authorDAO.deleteNewsAuthor(newsId);
		} catch (DAOException e) {
			logger.error("Exception while news_author deliting process",e);
			throw new ServiceException("Exception while author deliting process",e);
		}

	}

	public Author getAuthorByNewsId(Long newsId) throws ServiceException {
		Author author = null;
		try {
			author = authorDAO.getAuthorByNewsId(newsId);
		} catch (DAOException e) {
			logger.error("Exception while author getting process",e);
			throw new ServiceException("Exception while author getting process",e);
		}
		return author;
	}

	public Author getAuthorByAuthorId(Long authorId) throws ServiceException {
		Author author = null;
		try {
			author = authorDAO.getAuthorByAuthorId(authorId);
		} catch (DAOException e) {
			logger.error("Exception while author getting process",e);
			throw new ServiceException("Exception while author getting process",e);
		}
		return author;
	}

	public void updateAuthor(Author author) throws ServiceException {
		try {
			authorDAO.updateAuthor(author);
		} catch (DAOException e) {
			logger.error("Exception while author updating process",e);
			throw new ServiceException("Exception while author updating process",e);
		}

	}
	
	public List<Author> getAllAuthors() throws ServiceException {
		List<Author> authorList = null;
		try {
			authorList = authorDAO.getAllAuthors();
		} catch(DAOException e) {
			logger.error("Exception while author getting process",e);
			throw new ServiceException("Exception while author getting process",e);
		}
		return authorList;
	}

}
