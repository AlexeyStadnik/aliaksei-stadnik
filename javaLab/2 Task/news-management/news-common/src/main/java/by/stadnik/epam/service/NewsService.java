package by.stadnik.epam.service;

import java.util.List;

import by.stadnik.epam.entity.News;
import by.stadnik.epam.entity.NewsVO;
import by.stadnik.epam.util.SearchCriteria;


public interface NewsService {
	
	public Long addNews(News news) throws ServiceException;
	public void deleteNews(Long newsId) throws ServiceException;
	public News getNews(Long newsId) throws ServiceException;
	public void updateNews(News news) throws ServiceException; 
	public int getNumberOfNews(SearchCriteria searchCriteria) throws ServiceException;
	public List<NewsVO>getListOfNews(SearchCriteria searchCriteria,int firstIndex,int lastIndex) throws ServiceException;
}
