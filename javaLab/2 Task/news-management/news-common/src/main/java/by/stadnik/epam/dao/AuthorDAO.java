package by.stadnik.epam.dao;

import java.util.List;

import by.stadnik.epam.dao.impl.AuthorDAOImpl;
import by.stadnik.epam.dao.impl.AuthorDAOImplTest;
import by.stadnik.epam.entity.Author;
/**
 * Interface includes methods to work with AUTHOR and NEWS_AUTHOR tables
 * @see Author
 * @see AuthorDAOImpl - class implements  {@link AuthorDAO}
 * @see AuthorDAOImplTest - test clas for {@link AuthorDAO}
 * @author Aliaksei_Stadnik
 *
 */
public interface AuthorDAO {

	/**Method for adding Author entity in the AUTHOR table
	 * @param author - Author entity object 
	 * @return - auto generated Long id of the created author
	 * @throws DAOException
	 */
	public Long addAuthor(Author author) throws DAOException;

	/**
	 * Method for deleting from AUTHOR table
	 * @param authorId
	 * @throws DAOException
	 */
	public void deleteAuthor(Long authorId) throws DAOException;

	/**
	 * Method for adding in the NEWS_ATHOR table
	 * @param authorId - id of Author from AUTHOR table
	 * @param newsId - id  of News from NEWS table
	 * @throws DAOException
	 */
	public void addNewsAuthor(Long authorId,Long newsId) throws DAOException;

	/**
	 * Method for deleting from NEWS_ATHOR table
	 * @param newsId - id of News for deleting
	 * @throws DAOException
	 */
	public void deleteNewsAuthor(Long newsId) throws DAOException;

	/**
	 * Method for getting Author by id of the News entity
	 * @param newsId - id of New entity for which Author is searched 
	 * @return - object of Author entity
	 * @throws DAOException
	 */
	public Author getAuthorByNewsId(Long newsId) throws DAOException;

	/**
	 * Method for getting Author by id of the Author entity
	 * @param authorId
	 * @return -  object of Author entity
	 * @throws DAOException
	 */
	public Author getAuthorByAuthorId(Long authorId) throws DAOException;

	/**
	 * Method for updating AUTHOR table 
	 * @param author - Author entity with  updated info
	 * @throws DAOException
	 */
	public void updateAuthor(Author author) throws DAOException;
	
	public List<Author> getAllAuthors() throws DAOException;

}
