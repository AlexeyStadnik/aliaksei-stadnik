<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="header">
	<h1><spring:message code="header.header"/></h1>
	
	<table style="float: right;">
	
	<tr>
		<td><span style="float: right"> <a href="?lang=en"><spring:message code="header.en"/></a> | <a
			href="?lang=ru"><spring:message code="header.ru"/> </a>
		</span></td>
	</tr>
	<tr>
	<td style="float: right">
		
	
	</td>
	
	</tr>
	</table>

</div>