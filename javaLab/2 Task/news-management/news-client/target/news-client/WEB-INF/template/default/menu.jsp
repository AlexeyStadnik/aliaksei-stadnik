<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<div class="menu">
Menu
    <ul>
        <li>
            <spring:url value="/adminStartPage" var="newsListUrl" htmlEscape="true"/>
            <a href="${newsListUrl}"><spring:message code="menu.newsList"/></a>
        </li>
        <li>
            <spring:url value="/addNews" var="addNewsUrl" htmlEscape="true"/>
            <a href="${addNewsUrl}"><spring:message code="menu.addNews"/></a>
        </li>
         <li>
            <spring:url value="/addUpdateAuthor" var="addUpdateAuthorUrl" htmlEscape="true"/>
            <a href="${addUpdateAuthorUrl}"><spring:message code="menu.addUpdateAuthor"/></a>
        </li>
         <li>
            <spring:url value="/addUpdateTag" var="addUpdateTagsUrl" htmlEscape="true"/>
            <a href="${addUpdateTagsUrl}"><spring:message code="menu.addUpdateTag"/></a>
        </li>
    </ul>
</div>

