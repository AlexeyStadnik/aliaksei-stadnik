<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">

		<div class="app-body">

			


				



				<div id="watchNews" class="news-form">
					<table>
						<tr>
							<td class="news-title">${newsVO.news.title}</td>
						</tr>
						<tr>
							<td>(by ${newsVO.author.authorName})</td>
						</tr>
						<p class="modification-date">${newsVO.news.modificationDate}</p>

						<tr>
							<td>${newsVO.news.fullText}</td>
						</tr>
						<tr>
							<div>
								<table class="comment-form">
									<c:forEach var="comment" items="${newsVO.commentsList}">
										<tr>
											<td class="comment-creation-date">${comment.creationDate}</td>
										</tr>
										<tr>
											<td class="comment-text">${comment.commentText}<a
												class="delete-button"
												href="/news-admin/deleteComment?commentId=${comment.commentId}&newsId=${newsVO.news.newsId}"></a></td>
										</tr>
									</c:forEach>

									<tr>
										<table>
											<form:form method="POST" action="/news-admin/addComment"
												commandName="comment" modelAttribute="comment">

												<tr>
													<td><form:label path="commentText"></form:label></td>
													<td><form:textarea class="new-comment-text"
															path="commentText" rows="5" cols="30"></form:textarea></td>
													<td><form:errors path="commentText"
															class="error-message"/></td>
												</tr>
												<tr>
													<td><form:hidden path="newsId" /></td>
												</tr>
												<tr>
													<td colspan="2"><input type="submit"
														value="Post comment" /></td>
												</tr>

											</form:form>
										</table>
									</tr>
								</table>
							</div>
						</tr>
					</table>
				</div>

			</div>
		
		<script>
			$(".author-check-box").dropdownchecklist({
				textFormatFunction : function(options) {
					var selectedOptions = options.filter(":selected");
					var countOfSelected = selectedOptions.size();
					var size = options.size();
					switch (countOfSelected) {
					case 0:
						return "No author";
					case 1:
						return selectedOptions.text();
					}
				}
			});
			$(".tag-check-box").dropdownchecklist({
				textFormatFunction : function(options) {
					var selectedOptions = options.filter(":selected");
					var countOfSelected = selectedOptions.size();
					var size = options.size();
					switch (countOfSelected) {
					case 0:
						return "No tags";
					case 1:
						return selectedOptions.text();
					case size:
						return "All tags are selected";
					default:
						return countOfSelected + " Tags";
					}
				}
			});
			$("#tabs").tabs();
			$(".delete-button").button().css({
				width : '5%',
				height : '50%'
			}).html('X');

			//$( ".selected-page-number" ).button().css({ 'color': 'red'});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>