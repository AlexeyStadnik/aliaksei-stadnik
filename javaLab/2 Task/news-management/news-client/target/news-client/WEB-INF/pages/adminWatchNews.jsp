<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">

		<div class="app-body">

			<div id="tabs">
				<ul>
					<li><a href="#watchNews"><spring:message
								code="news.watchNews" /></a></li>
					<li><a href="#updateNews"><spring:message
								code="news.updateNews" /></a></li>
					<li><a href="#updateNewsTag"><spring:message
								code="news.updateTagAuthor" /></a></li>
				</ul>


				<div id="updateNewsTag">
					<form:form method="POST" action="/news-admin/updateNewsTag"
						commandName="newsFormVO">
						<table>
							<tr>
								<td><select name="tagIdList" class="tag-check-box"
									multiple="" style="display: none;">
										<c:forEach var="tag" items="${tagList}">
											<option name="${tag.tagId}" value="${tag.tagId}"
												<c:forEach var="selectedTag" items="${newsVO.tagList}">${selectedTag.tagId == tag.tagId ? 'selected' : ''}</c:forEach>>${tag.tagName}</option>
										</c:forEach>
								</select>
								<td>
							</tr>
							<input type="hidden" name="news.newsId"
								value="${newsVO.news.newsId}" />

							<tr>
								<td><input type="submit"
									class="jquery-button ui-button ui-widget ui-state-default ui-corner-all 
          ui-button-text-only"
									value="<spring:message code="button.update"/>" />
								<td>
							</tr>
						</table>
					</form:form>

					<form:form method="POST" action="/news-admin/updateNewsAuthor"
						commandName="newsFormVO">
						<table>
							<tr>
								<td><select name="authorId" class="author-check-box">
										<option value="">No author</option>
										<c:forEach var="author" items="${authorList}">
											<option value="${author.authorId}" name="${author.authorId}"
												${newsVO.author.authorId == author.authorId ? 'selected' : ''}>${author.authorName}</option>
										</c:forEach>
								</select>
								<td>
							</tr>
							<input type="hidden" name="news.newsId"
								value="${newsVO.news.newsId}" />

							<tr>
								<td><input type="submit"
									class="jquery-button ui-button ui-widget ui-state-default ui-corner-all 
          ui-button-text-only"
									value="<spring:message code="button.update"/>" />
								<td>
							</tr>
						</table>
					</form:form>

				</div>




				<div id="updateNews">
					<form:form method="POST" action="/news-admin/updateNews"
						commandName="news">
						<table>
							<tr>
								<td>Title</td>
								<td><input type="text" name="title"
									value="${newsVO.news.title}" required="required" /></td>
								<td><form:errors path="title" cssStyle="color: #ff0000;" /></td>
							</tr>
							<tr>
								<td>Short text</td>
								<td><textarea rows="7" cols="80" name="shortText"
										required="required">${newsVO.news.shortText}</textarea></td>
								<td><form:errors path="shortText"
										cssStyle="color: #ff0000;" /></td>
							</tr>
							<tr>
								<td>Full text</td>
								<td><textarea rows="15" cols="80" name="fullText"
										required="required">${newsVO.news.fullText}</textarea></td>
								<td><form:errors path="fullText" cssStyle="color: #ff0000;" /></td>
							</tr>

							<input type="hidden" name="newsId" value="${newsVO.news.newsId}" />
							<input type="hidden" name="modificationDate"
								value="${newsVO.news.modificationDate}" />
							<input type="hidden" name="creationDate"
								value="${newsVO.news.creationDate}" />
							<tr>
								<td><input type="submit"
									class="jquery-button ui-button ui-widget ui-state-default ui-corner-all 
          ui-button-text-only"
									value="<spring:message code="button.update"/>" />
								<td>
							</tr>
						</table>
					</form:form>

				</div>

				<div id="watchNews" class="news-form">
					<table>
						<tr>
							<td class="news-title">${newsVO.news.title}</td>
						</tr>
						<tr>
							<td>(by ${newsVO.author.authorName})</td>
						</tr>
						<p class="modification-date">${newsVO.news.modificationDate}</p>

						<tr>
							<td>${newsVO.news.fullText}</td>
						</tr>
						<tr>
							<div>
								<table class="comment-form">
									<c:forEach var="comment" items="${newsVO.commentsList}">
										<tr>
											<td class="comment-creation-date">${comment.creationDate}</td>
										</tr>
										<tr>
											<td class="comment-text">${comment.commentText}<a
												class="delete-button"
												href="/news-admin/deleteComment?commentId=${comment.commentId}&newsId=${newsVO.news.newsId}"></a></td>
										</tr>
									</c:forEach>

									<tr>
										<table>
											<form:form method="POST" action="/news-admin/addComment"
												commandName="comment">

												<tr>
													<td><form:label path="commentText"></form:label></td>
													<td><form:textarea class="new-comment-text"
															path="commentText" rows="5" cols="30"></form:textarea></td>
												</tr>
												<tr>
													<td><form:hidden path="newsId" /></td>
												</tr>
												<tr>
													<td colspan="2"><input type="submit"
														value="Post comment" /></td>
												</tr>

											</form:form>
										</table>
									</tr>
								</table>
							</div>
						</tr>
					</table>
				</div>

			</div>
		</div>
		<script>
			$(".author-check-box").dropdownchecklist({
				textFormatFunction : function(options) {
					var selectedOptions = options.filter(":selected");
					var countOfSelected = selectedOptions.size();
					var size = options.size();
					switch (countOfSelected) {
					case 0:
						return "No author";
					case 1:
						return selectedOptions.text();
					}
				}
			});
			$(".tag-check-box").dropdownchecklist({
				textFormatFunction : function(options) {
					var selectedOptions = options.filter(":selected");
					var countOfSelected = selectedOptions.size();
					var size = options.size();
					switch (countOfSelected) {
					case 0:
						return "No tags";
					case 1:
						return selectedOptions.text();
					case size:
						return "All tags are selected";
					default:
						return countOfSelected + " Tags";
					}
				}
			});
			$("#tabs").tabs();
			$(".delete-button").button().css({
				width : '5%',
				height : '50%'
			}).html('X');

			//$( ".selected-page-number" ).button().css({ 'color': 'red'});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>