$(".author-check-box").dropdownchecklist({
	textFormatFunction : function(options) {
		var selectedOptions = options.filter(":selected");
		var countOfSelected = selectedOptions.size();
		var size = options.size();
		switch (countOfSelected) {
		case 0:
			return "-";
		case 1:
			return selectedOptions.text();
		}
	}
});