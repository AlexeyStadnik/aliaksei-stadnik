package by.news.epam.controller;


import java.util.ArrayList;
import java.util.List;



import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;



import by.news.epam.util.ControllerException;
import by.stadnik.epam.entity.Author;
import by.stadnik.epam.entity.NewsVO;
import by.stadnik.epam.entity.Tag;
import by.stadnik.epam.service.AuthorService;
import by.stadnik.epam.service.NewsService;
import by.stadnik.epam.service.ServiceException;
import by.stadnik.epam.service.ServiceManager;
import by.stadnik.epam.service.TagService;
import by.stadnik.epam.util.SearchCriteria;

@Controller
public class StartPageController {

	public static final int RECORDS_PER_PAGE = 3;
	public static final int STARTING_PAGE = 1;

	public static final String START_PAGE_URL = "startPage";
	public static final String FILTER_URL = "filter";
	public static final String RESET_URL = "resetSearchCriteria";
	public static final String DELETE_URL = "deleteSelectedNews";


	public static final String SEARCH_CRITERIA_PARAM = "searchCriteria";
	public static final String PAGE_PARAM = "page";
	public static final String AUTHOR_LIST_PARAM = "authorList";
	public static final String TAG_LIST_PARAM = "tagList";
	public static final String NEWS_LIST_PARAM = "newsList";
	public static final String NUMBER_OF_PAGES_PARAM = "noOfPages";
	public static final String CURRENT_PAGE_PARAM = "currentPage";

	private static final Logger logger = LoggerFactory.getLogger(StartPageController.class);

	@Autowired
	private TagService tagService;
	@Autowired
	private NewsService newsService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private ServiceManager serviceManager;

	@RequestMapping(value = START_PAGE_URL)  
	public ModelAndView getAdminPage(Model model, HttpServletRequest request) 
			throws ControllerException  {

		SearchCriteria searchCriteria = (SearchCriteria) request.getSession().getAttribute(SEARCH_CRITERIA_PARAM);

		searchCriteria = getSearchCriteria(searchCriteria);

		int page = STARTING_PAGE;
		int recordsPerPage = RECORDS_PER_PAGE;
		if(request.getParameter(PAGE_PARAM) != null) {
			page = Integer.parseInt(request.getParameter(PAGE_PARAM));
		}

		List<NewsVO> newsList;
		int noOfRecords = 0;
		try {
			noOfRecords = newsService.getNumberOfNews(searchCriteria);
			newsList = serviceManager.getListOfNews(searchCriteria,(page-1)*recordsPerPage,(page-1)*recordsPerPage+recordsPerPage);
		} catch (ServiceException e) {
			logger.error("Exception while getting list of news",e);
			throw new ControllerException("Exception while getting list of news",e);
		}


		int noOfPages = (int) Math.floor(noOfRecords * 1.0 / recordsPerPage);


		request.setAttribute(NEWS_LIST_PARAM, newsList);
		request.setAttribute(NUMBER_OF_PAGES_PARAM, noOfPages);
		request.setAttribute(CURRENT_PAGE_PARAM, page);
		return new ModelAndView(START_PAGE_URL, "command", new SearchCriteria());

	}

	@RequestMapping(value=FILTER_URL, method=RequestMethod.POST)
	public String  filter(SearchCriteria searchCriteria,HttpServletRequest request) {	
		request.getSession().setAttribute(SEARCH_CRITERIA_PARAM, searchCriteria);
		return "redirect:" + START_PAGE_URL;
	}

	@RequestMapping(RESET_URL)
	public String resetSearchCriteria(HttpServletRequest request) {
		request.getSession().setAttribute(SEARCH_CRITERIA_PARAM, null);
		return "redirect:" + START_PAGE_URL;
	}



	@ModelAttribute(AUTHOR_LIST_PARAM)
	public List<Author> getAuthorList() throws ControllerException {
		List<Author> authorList = null;
		try {
			authorList = authorService.getAllAuthors();
		} catch (ServiceException e) {
			logger.error("Exception while geting list of authors",e);
			throw new ControllerException("Exception while geting list of authors",e);
		}
		return authorList;
	}


	@ModelAttribute(TAG_LIST_PARAM)
	public List<Tag> getTagList() throws ControllerException {
		List<Tag> tagList = null;
		try {
			tagList = tagService.getAllTag();
		} catch (ServiceException e) {
			logger.error("Exception while geting list of tags",e);
			throw new ControllerException("Exception while geting list of tags",e);
		}
		return tagList;
	}



	private static  SearchCriteria getSearchCriteria(SearchCriteria searchCriteria) {
		if(searchCriteria == null) {
			searchCriteria = new SearchCriteria();
		}
		if(searchCriteria.getTagIdList() == null) {
			searchCriteria.setTagIdList(new ArrayList<Long>());
		}
		return searchCriteria;
	}

}