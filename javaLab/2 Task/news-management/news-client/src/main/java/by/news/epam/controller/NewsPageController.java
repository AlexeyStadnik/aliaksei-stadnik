package by.news.epam.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import by.news.epam.util.ControllerException;
import by.stadnik.epam.entity.Comment;
import by.stadnik.epam.entity.NewsFormVO;
import by.stadnik.epam.entity.NewsVO;
import by.stadnik.epam.service.AuthorService;
import by.stadnik.epam.service.CommentsService;
import by.stadnik.epam.service.NewsService;
import by.stadnik.epam.service.ServiceException;
import by.stadnik.epam.service.ServiceManager;
import by.stadnik.epam.service.TagService;

@Controller
public class NewsPageController {

	private static final Logger logger = LoggerFactory.getLogger(NewsPageController.class);

	public static final String WATCH_NEWS= "watchNews/{newsId}";
	public static final String WATCH_NEWS_URL = "watchNews";
	
	public static final String COMMENT_PARAM = "comment";
	public static final String NEWS_PARAM = "news";
	public static final String NEWS_FORM_VO_PARAM = "newsFormVO";
	public static final String NEWS_VO_PARAM = "newsVO";
	public static final String NEWS_ID_PARAM = "newsId";
	
	
	@Autowired
	private AuthorService authorService;
	@Autowired
	private TagService tagService;
	@Autowired
	private CommentsService commentsService;
	@Autowired
	private NewsService newsService;
	@Autowired
	private ServiceManager serviceManager;
	
	@RequestMapping(value = WATCH_NEWS, method = RequestMethod.GET)
	public String watchNews(@PathVariable(NEWS_ID_PARAM) Long newsId,Model model) throws ControllerException {
		NewsVO newsVO = null;
		try {
			newsVO = serviceManager.getNewsVO(newsId);
		} catch (ServiceException e) {
			throw new ControllerException("Exception while news with id = [" + newsId + "] getting process ",e);
		}
		model.addAttribute(NEWS_VO_PARAM, newsVO);
		final Long finalNewsId = new Long(newsId);
		if(!model.containsAttribute(COMMENT_PARAM)) {
			model.addAttribute(COMMENT_PARAM, (new Comment(){{setNewsId(finalNewsId);}}));
		}
		model.addAttribute(NEWS_PARAM,newsVO.getNews());
		model.addAttribute(NEWS_FORM_VO_PARAM,(new NewsFormVO()));
		return WATCH_NEWS_URL;
	}


	
}
