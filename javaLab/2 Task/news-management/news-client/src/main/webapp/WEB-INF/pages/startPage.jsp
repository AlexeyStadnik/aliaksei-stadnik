<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
	<script src="<c:url value="/resources/js/script.js" />"></script>
		<div class="app-body">
			<form:form method="POST" action="filter" commandName="searchCriteria">


				<select name="authorId" class="author-check-box">
					<option value=""><spring:message code="noAuthor"/></option>
					<c:forEach var="author" items="${authorList}">
						<option value="${author.authorId}" name="${author.authorId}"
							${searchCriteria.authorId == author.authorId ? 'selected' : ''}>${author.authorName}</option>
					</c:forEach>
				</select>

				<select name="tagIdList" class="tag-check-box" multiple=""
					style="display: none;">
					<c:forEach var="tag" items="${tagList}">
						<option name="${tag.tagId}" value="${tag.tagId}"
							<c:forEach var="selectedTag" items="${searchCriteria.tagIdList}">${selectedTag == tag.tagId ? 'selected' : ''}</c:forEach>>${tag.tagName}</option>
					</c:forEach>
				</select>
				<input type="submit" name="submit"
					value=<spring:message code="button.find"/>>
				

			</form:form>
			
				<a href="resetSearchCriteria"><input type="submit" 
					value=<spring:message code="button.reset"/>></a>
			

			

				<c:forEach var="newsVO" items="${newsList}" varStatus="status">
					<div class="news-form">
						<td><p1 class="main-news-title"> <a
								href="watchNews/${newsVO.news.newsId}"><c:out value="${newsVO.news.title}"/></a></p1></td>

						<span class="main-news-author">
							<td>(<spring:message code="news.by" />
								<c:out value="${newsVO.author.authorName}"/>)
						</td>
							<td>${newsVO.news.modificationDate}</td>
						</span> </br> </br> <span class="short-text">
							<td><c:out value="${newsVO.news.shortText}"/></td>
						</span> <span class="main-news-author"> 
						<c:forEach var="tag"
								items="${newsVO.tagList}">
								<td><span class="tag-list"><c:out value="${tag.tagName}"/>, </span></td>
							</c:forEach>
							
							<td>Comments(${newsVO.commentNumber})</td>
							
							
							 </br>

						</span>
					</div>

					
				</c:forEach>

				
			


		<!--  	<c:if test="${currentPage != 1}">
				<td class="page-switcher"><a class="previous-button"
					href="adminStartPage?page=${currentPage - 1}"><spring:message
							code="button.previous" /></a></td>
			</c:if>  -->

			<table class="page-switcher">
				<tr>
					<c:forEach begin="1" end="${noOfPages}" var="i">
						<c:choose>
							<c:when test="${currentPage eq i}">
								<td><input  type="submit"  value="${i}" disabled="disabled"/></td>
							</c:when>
							<c:otherwise>
								<td ><a href="startPage?page=${i}"><input  type="submit"  value="${i}" /></a></td>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</tr>
			</table>

		<!--	<c:if test="${currentPage lt noOfPages}">
				<td><a class="next-button"
					href="adminStartPage?page=${currentPage + 1}"><spring:message
							code="button.next" /></a></td>
			</c:if> -->

		</div>

		<script src="<c:url value="/resources/js/newsListScript.js" />"></script>
	</tiles:putAttribute>
</tiles:insertDefinition>


