<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">

		<div class="app-body">
			<form:form method="POST" action="filter" commandName="searchCriteria">

				<select name="authorId" class="author-check-box">
					<option></option>
					<c:forEach var="author" items="${authorList}">
						<option value="${author.authorId}" label="${author.authorName}" />
								${author.authorName}
						</c:forEach>
				</select>

				<select name="tagIdList" class="tag-check-box" multiple=""
					style="display: none;">
					<c:forEach var="tag" items="${tagList}">
						<option name="${tag.tagId}" value="${tag.tagId}">${tag.tagName}</option>
					</c:forEach>
				</select>
				<input type="submit" name="submit" value="Filter">
				<INPUT TYPE="RESET" value="Reset">

			</form:form>
			
			
			<c:forEach var="newsVO" items="${newsList}">
				<div class="news-form">
					<td><p1 class="main-news-title"> <a
							href="watchNews?newsId=${newsVO.news.newsId}">${newsVO.news.title}</a></p1></td>

					<span class="main-news-author">
						<td>(by ${newsVO.author.authorName})</td>
						<td>${newsVO.news.modificationDate}</td>
					</span> </br> </br> <span class="short-text">
						<td>${newsVO.news.shortText}</td>
					</span> <span class="main-news-author"> <c:forEach var="tag"
							items="${newsVO.tagList}">
							<td><span class="tag-list">${tag.tagName}, </span></td>
						</c:forEach>
						<td>Comments(${newsVO.commentNumber})</td> </br>

					</span>
				</div>
			</c:forEach>


			
			<c:if test="${currentPage != 1}">
				<td class="page-switcher"><a class="previous-button"
					href="admin?page=${currentPage - 1}">Previous</a></td>
			</c:if>

			<table class="page-switcher">
				<tr>
					<c:forEach begin="1" end="${noOfPages}" var="i">
						<c:choose>
							<c:when test="${currentPage eq i}">
								<td class="selected-page-number"">${i}</td>
							</c:when>
							<c:otherwise>
								<td class="page-number"><a href="admin?page=${i}">${i}</a></td>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</tr>
			</table>

			<c:if test="${currentPage lt noOfPages}">
				<td><a class="next-button" href="admin?page=${currentPage + 1}">Next</a></td>
			</c:if>

		</div>

		<script>
		$(".author-check-box").dropdownchecklist();
		$(".tag-check-box").dropdownchecklist({ textFormatFunction: function(options) {
	        var selectedOptions = options.filter(":selected");
	        var countOfSelected = selectedOptions.size();
	        var size = options.size();
	        switch(countOfSelected) {
	            case 0: return "No tag selected";
	            case 1: return selectedOptions.text();
	            case size: return "All tags are selected";
	            default: return countOfSelected + " Tags";
	        }
	    } });
		$( ".page-number" ).button().css({'width':'12px','height':'12px'});
		$( ".selected-page-number" ).button();
		$( ".selected-page-number" ).prop("disabled", true).addClass("ui-state-disabled");
		
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>


