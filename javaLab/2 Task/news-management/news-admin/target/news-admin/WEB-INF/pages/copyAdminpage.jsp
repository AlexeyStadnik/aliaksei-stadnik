<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
	
<script src="<c:url value="/resources/jquery-1.11.3.js" />"></script>	
<script src="<c:url value="/resources/jquery-1.6.1.min.js" />"></script>	
<script src="<c:url value="/resources/jquery-ui-1.8.13.custom.min.js" />"></script>	
<script src="<c:url value="/resources/ui.dropdownchecklist.js" />"></script>
<link href="<c:url value="/resources/ui.dropdownchecklist.themeroller.css" />" rel="stylesheet">	
<link href="<c:url value="/resources/jquery-ui-1.8.4.custom.css" />" rel="stylesheet">	
	
		<style type="text/css">
.news-form {
	text-align: inherit;
	padding: 6%;
	overflow: auto;
	
}
<td><input type="submit"
							value="<spring:message code="label.login"/>" /></td> 
.tag-list {
	color: gray;
	font-size: 120%;
}

.news-title {
	float: left;
	font-size: 120%;
	color: black;
}

.news-author {
	float: right;
} 

.short-text {
	float: left;
}

.news-tag {
	float: right;
}
.page-switcher {
 margin: 0 auto;
 text-align: center;

}
.page-switcher-top {
 
 padding-top: 10%;
}

</style>

	<div class="app-body">
	
	
	<h1>Form</h1>
		<form:form method="POST" action="addNewsForm">
			<table>
				<tr>
					<td><form:label path="title">Title</form:label></td>
					<td><form:input path="title" /></td>
				</tr>
				<tr>
					<td><form:label path="shortText">ShortText</form:label></td>
					<td><form:input path="shortText" /></td>
				</tr>
				<tr>
					<td><form:label path="fullText">id</form:label></td>
					<td><form:input path="fullText" /></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="Submit" /></td>
				</tr>
			</table>
		</form:form>

		<!--	<form action="filter">
				<select id="s1" class="s1" name="author"  tabindex="8" >
						
					<c:forEach var="author" items="${authorList}">
						<option value="${author.authorId}">${author.authorName}</option>
					</c:forEach>
				</select> 
				
				 <select id="s7" name="tags" class="s7" multiple="" style="display: none;">
					<c:forEach var="tag" items="${tagList}">
						<option name="${tag.tagId}" value="${tag.tagId}">${tag.tagName}</option>  
						<form:checkbox  path="${tag.tagId}" value="${tag.tagId}" />${tag.tagName}
					</c:forEach>
				</select> 
				
				
				<input id="myId" type="submit" value="Filter">
				
				
				
				
			</form> -->
			
			


	
		<!-- 	<td>
	  		<span>Nothing preselected</span><br>
	  		<span>(but with explicit tabindex)</span><br>
	        <select id="s1" class="s1" multiple="" tabindex="8" >
	            <option>Low</option>
	            <option>Medium</option>
	            <option>High</option>
	        </select>
	        </td> -->
	        
	 <!--        <td>
	  		<span>As radio buttons</span><br>
	        <select id="s7" class="s7" style="display: none;">
	            <option></option>
	            <option>Aries</option>
	            <option>Taurus</option>
	            <option>Gemini</option>
	            <option>Cancer</option>
	            <option>Leo	Leo</option>
	            <option>Virgo</option>
	            <option>Libra</option>
	            <option>Scorpius</option>
	            <option>Ophiuchus</option>
	            <option>Sagittarius</option>
	            <option>Capricornus</option>
	            <option>Aquarius</option>
	            <option>Pisces</option>
			</select>
	    	</td>-->
	
		
			<c:forEach var="newsVO" items="${newsList}">
				<div class="news-form">
					<td><p1 class="news-title">
						<a href="home?newsId=${newsVO.news.newsId}">${newsVO.news.title}</a></p1></td>

					<span class="news-author">
						<td>(by ${newsVO.author.authorName})</td>
						<td>${newsVO.news.modificationDate}</td>
					</span> </br>  </br> 
					<span class="short-text">
						<td>${newsVO.news.shortText}</td>
					</span> <span class="news-tag"> 
						<c:forEach var="tag" items="${newsVO.tagList}">
							<td><span class="tag-list">${tag.tagName}, </span></td>
						</c:forEach>
						<td>Comments(${newsVO.commentNumber})</td> </br>
						
					</span>
				</div>
			</c:forEach>

			
			<%--For displaying Previous link except for the 1st page --%>
			<c:if test="${currentPage != 1}">
				<td class="page-switcher"><a href="admin?page=${currentPage - 1}">Previous</a></td>
			</c:if>

			<%--For displaying Page numbers. 
    The when condition does not display a link for the current page--%>
			<table  class="page-switcher">
				<tr>
					<c:forEach begin="1" end="${noOfPages}" var="i">
						<c:choose>
							<c:when test="${currentPage eq i}">
								<td>${i}</td>
							</c:when>
							<c:otherwise>
								<td><a href="admin?page=${i}">${i}</a></td>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</tr>
			</table>

			<%--For displaying Next link --%>
			<c:if test="${currentPage lt noOfPages}">
				<td><a href="admin?page=${currentPage + 1}">Next</a></td>
			</c:if>

		

		</div>

		<script>
		$(".s1").dropdownchecklist();
		$("#s7").dropdownchecklist();
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>

