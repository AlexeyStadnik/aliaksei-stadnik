<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
	<h1>ERROR PAGE</h1>
	<table>
		<tr>
		<td>Exception</td>
		<td>${exception}</td>
		</tr>
		
		<tr>
		<td>Url</td>
		<td>${url}</td>
		</tr>
		
	</table>
		
	</tiles:putAttribute>
</tiles:insertDefinition>