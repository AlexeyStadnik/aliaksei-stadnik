<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
 
<style type="text/css">
.news-form {
	text-align: inherit;
	padding: 6%;
	overflow: auto;
}

.tag-list {
	color: gray;
	font-size: 120%;
}

.news-title {
	float: left;
	font-size: 120%;
	color: black;
}

.news-author {
	float: left;
}

.modification-date {
	float: right;
}

.short-text {
	float: left;
}

.comment-form {
	margin: 0 auto;
	text-align: center;
	padding: 10%;
	padding-top:10%;
	width:70%;
	height: 15%;
	overflow: scroll;
}

.page-switcher {
	margin: 0 auto;
	text-align: center;
}

.page-switcher-top {
	padding-top: 10%;
}
</style>
 
       			
					<td><p1 class="news-title">
						${newsVO.news.title}</a></p1></td>
					<span class="news-author">
						<td>(by ${newsVO.author.authorName})</td>
						
					</span>
					<span class="modification-date">
					<td>${newsVO.news.modificationDate}</td> 
					</span>
					</br>  </br> 
					<span class="short-text">
						<td>${newsVO.news.shortText}</td>
					</span> 
					</br>
						<c:forEach var="tag" items="${newsVO.commentsList}">
							<td><span class="news-form">${tag.commentText}, </span></td>
							</br>
						</c:forEach>
						<td>Comments(${newsVO.commentNumber})</td> </br>
				
			
 	
 

    </tiles:putAttribute>
</tiles:insertDefinition>