<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">

		

			<form method="post" style="   margin-bottom: 0em;"
				action="<c:url value='j_spring_security_check' />">
				<table class="login-form">
					<tr>
						<td colspan="2" style="color: red">${message}</td>
					</tr>
					<tr>
						<td><spring:message code="login.username" /></td>
						<td><input type="text" name="username" /></td>
					</tr>
					<tr>
						<td><spring:message code="login.password" /></td>
						<td><input type="password" name="password" /></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit"
							value="<spring:message code="login.login"/>" /></td>
					</tr>
				</table>
			</form>
		

	</tiles:putAttribute>
</tiles:insertDefinition>

</html>
