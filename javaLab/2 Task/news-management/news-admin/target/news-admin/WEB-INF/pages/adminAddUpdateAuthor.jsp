<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">


		<div class="app-body">
			<div class="news-form">

				<div class="accordion">
					<c:forEach var="author" items="${authorList}">

						<h3>${author.authorName}</h3>
						<div>
							<table>
								<form:form method="POST" action="updateAuthor">

									<tr>
										<td><spring:message code="author.authorName" /></td>
										<td><input type="text" name="authorName"
											value="${author.authorName}" /></td>
									</tr>
									<input type="hidden" name="authorId" value="${author.authorId}" />
									<tr>
										<td><input type="submit"
											class="jquery-button ui-button ui-widget ui-state-default ui-corner-all 
          ui-button-text-only"
											value="<spring:message code="button.updateAuthor"/>" /></td>
								</form:form>

								<td><a href="deleteAuthor?authorId=${author.authorId}">
										<input type="submit"
										class="jquery-button ui-button ui-widget ui-state-default ui-corner-all 
          ui-button-text-only"
										value="<spring:message code="button.deleteAuthor"/>" />
								</a></td>
								</tr>

							</table>


						</div>

					</c:forEach>
				</div>

				<table>
					<form:form method="POST" action="/news-admin/addAuthor">

						<tr>
							<td><form:label path="authorName"></form:label></td>
							<td><form:input class="new-comment-text" path="authorName"></form:input></td>
							<td><form:errors path="authorName"
									class="ui-state-error ui-corner-all" /></td>


						</tr>
						<tr>
							<td colspan="2"><input type="submit"
								value="<spring:message code="button.addAuthor"/>" /></td>
						</tr>

					</form:form>
				</table>


			</div>
		</div>
<script src="<c:url value="/resources/js/script.js" />"></script>
	</tiles:putAttribute>
</tiles:insertDefinition>