package by.news.epam.controller;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import by.news.epam.util.ControllerException;
import by.stadnik.epam.entity.Comment;
import by.stadnik.epam.entity.NewsFormVO;
import by.stadnik.epam.entity.NewsVO;
import by.stadnik.epam.service.AuthorService;
import by.stadnik.epam.service.CommentsService;
import by.stadnik.epam.service.NewsService;
import by.stadnik.epam.service.ServiceException;
import by.stadnik.epam.service.ServiceManager;
import by.stadnik.epam.service.TagService;

@Controller
public class NewsPageController {

	private static final Logger logger = LoggerFactory.getLogger(NewsPageController.class);

	public static final String WATCH_NEWS= "adminWatchNews/{newsId}";
	public static final String WATCH_NEWS_URL = "adminWatchNews";
	public static final String START_PAGE_URL = "adminStartPage";
	public static final String DELETE_COMMENT_URL = "deleteComment";
	public static final String ADD_COMMENT_URL = "addComment";
	public static final String UPDATE_NEWS_URL = "updateNews";
	public static final String UPDATE_NEWS_TAG_URL = "updateNewsTag";
	public static final String UPDATE_NEWS_AUTHOR_URL = "updateNewsAuthor";

	public static final String COMMENT_PARAM = "comment";
	public static final String NEWS_PARAM = "news";
	public static final String NEWS_FORM_VO_PARAM = "newsFormVO";
	public static final String NEWS_FORM_PARAM = "newsForm";
	public static final String NEWS_VO_PARAM = "newsVO";
	public static final String NEWS_ID_PARAM = "newsId";
	public static final String COMMENT_ID_PARAM = "commentId";
	public static final String AUTHOR_LIST_PARAM = "authorList";
	public static final String TAG_LIST_PARAM = "tagList";

	@Autowired
	private AuthorService authorService;
	@Autowired
	private TagService tagService;
	@Autowired
	private CommentsService commentsService;
	@Autowired
	private NewsService newsService;
	@Autowired
	private ServiceManager serviceManager;
	
	@RequestMapping(value = WATCH_NEWS, method = RequestMethod.GET)
	public String watchNews(@PathVariable(NEWS_ID_PARAM) Long newsId,Model model) throws ControllerException {
		NewsVO newsVO = null;
		try {
			newsVO = serviceManager.getNewsVO(newsId);
		} catch (ServiceException e) {
			throw new ControllerException("Exception while news with id = [" + newsId + "] getting process ",e);
		}
		model.addAttribute(NEWS_VO_PARAM, newsVO);
		final Long finalNewsId = new Long(newsId);
		if(!model.containsAttribute("comment")) {
			model.addAttribute("comment", (new Comment(){{setNewsId(finalNewsId);}}));
		}
		model.addAttribute(NEWS_PARAM,newsVO.getNews());
		model.addAttribute(NEWS_FORM_VO_PARAM,(new NewsFormVO()));
		return WATCH_NEWS_URL;
	}



	@RequestMapping(value = DELETE_COMMENT_URL)
	public String deleteComment(@RequestParam(NEWS_ID_PARAM) Long newsId,@RequestParam(COMMENT_ID_PARAM) Long commentId) throws ControllerException {

		try {
			commentsService.deleteCommentByCommentId(commentId);
		}  catch (ServiceException e) {
			logger.error("Exception while comment with id = [" + commentId + "] deleting process ",e);
			throw new ControllerException("Exception while comment with id = [" + commentId + "] deleting process ",e);
		}

		return "redirect:" + WATCH_NEWS_URL + "/" + newsId;
	}

	
}
