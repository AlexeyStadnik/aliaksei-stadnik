package by.news.epam.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import by.news.epam.util.ControllerException;
import by.stadnik.epam.entity.Tag;
import by.stadnik.epam.service.ServiceException;
import by.stadnik.epam.service.TagService;

@Controller
public class TagController {
	
	public static final String TAG_LIST_PARAM = "tagList";
	
	@Autowired
	private TagService tagService;
	
	@Autowired
	@Qualifier("tagValidator")
	private Validator validator;

	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}
	
	
	private static final Logger logger = LoggerFactory.getLogger(TagController.class);
	
	
	@RequestMapping(value="adminAddUpdateTag")
	public String addUpdateTag(Model model) throws ControllerException {
		model.addAttribute(TAG_LIST_PARAM, getTagList());
		model.addAttribute("command", new Tag());
		return "adminAddUpdateTag";
		
	}
	
	@RequestMapping(value="adminDeleteTag")
	public String deleteTag(@RequestParam("tagId") Long tagId) throws ControllerException {
		try {
			tagService.deleteTag(tagId);
		} catch (ServiceException e) {
			logger.error("Exception while deleting tag with id [" + tagId + "]" ,e);
			throw new ControllerException("Exception while deleting tag with id [" + tagId + "]" ,e);
		}
		return "redirect:adminAddUpdateTag";
	}
	
	@RequestMapping(value="adminAddTag")
	public String addTag(@ModelAttribute("command") @Validated Tag tag,BindingResult result) throws ControllerException {
		if(result.hasErrors()) {
			return "adminAddUpdateTag";
		}
		
		Long tagId = null;
		
		try {
			tagId = tagService.addTag(tag);
		} catch (ServiceException e) {
			logger.error("Exception while adding tag with id [" + tagId + "]" ,e);
			throw new ControllerException("Exception while adding tag with id [" + tagId + "]" ,e);
		}
		
		return "redirect:adminAddUpdateTag";
	}
	
	@RequestMapping(value="adminUpdateTag")
	public String updateTag(@ModelAttribute("command") @Validated Tag tag,BindingResult result) throws ControllerException {
		if(result.hasErrors()) {
			return "adminAddUpdateTag";
		}
		try {
			tagService.updateTag(tag);
		} catch (ServiceException e) {
			logger.error("Exception while adding tag" ,e);
			throw new ControllerException("Exception while adding" ,e);
		}
		return "redirect:adminAddUpdateTag";
	}
	
	
	@ModelAttribute(TAG_LIST_PARAM)
	public List<Tag> getTagList() throws ControllerException {
		List<Tag> tagList = null;
		try {
			tagList = tagService.getAllTag();
		} catch (ServiceException e) {
			logger.error("Exception while geting list of tags",e);
			throw new ControllerException("Exception while geting list of tags",e);
		}
		return tagList;
	}

}
