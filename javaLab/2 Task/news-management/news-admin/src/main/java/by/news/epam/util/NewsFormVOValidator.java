package by.news.epam.util;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import by.stadnik.epam.entity.NewsFormVO;
import by.stadnik.epam.service.AuthorService;
import by.stadnik.epam.service.ServiceException;

public class NewsFormVOValidator implements Validator {

	public static final int TITLE_LENGTH = 30;
	public static final int SHORT_TEXT_LENGTH = 100;
	public static final int FULL_TEXT_LENGTH= 2000;


	@Autowired
	AuthorService authorService;

	private static final Logger logger = LoggerFactory.getLogger(NewsFormVOValidator.class);

	public boolean supports(Class<?> clazz) {
		return NewsFormVO.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {

		NewsFormVO newsFormVO = (NewsFormVO) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.title", "valid.newsTitle");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.shortText", "valid.shortText");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.fullText", "valid.fullText");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "authorId", "valid.authorId");

		try {
			
			if(newsFormVO.getAuthorId() != null && authorService.getAuthorByAuthorId(newsFormVO.getAuthorId()).getExpired() != null) {
				errors.rejectValue("authorId","valid.authorExpired");
			}
		} catch (ServiceException e) {
			logger.error("Exception while getting author during thr validation process",e);
		}

		if(newsFormVO.getNews().getTitle() != null && newsFormVO.getNews().getTitle().length() > TITLE_LENGTH) {
			errors.rejectValue("news.title","valid.titleToLong");
		}
		if(newsFormVO.getNews().getShortText() != null && newsFormVO.getNews().getShortText().length() > SHORT_TEXT_LENGTH) {
			errors.rejectValue("news.shortText","valid.shortTextToLong");
		}
		if(newsFormVO.getNews().getFullText() != null && newsFormVO.getNews().getFullText().length() > FULL_TEXT_LENGTH) {
			errors.rejectValue("news.fullText","valid.fullTextToLong");
		}

		if(errors.hasErrors()) {
			logger.debug("Adding news proccess doesnt pass validation");
		}



	}

}

