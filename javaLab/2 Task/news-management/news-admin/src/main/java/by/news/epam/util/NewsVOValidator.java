package by.news.epam.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import by.stadnik.epam.entity.News;

public class NewsVOValidator implements Validator {

	public static final int TITLE_LENGTH = 30;
	public static final int SHORT_TEXT_LENGTH = 100;
	public static final int FULL_TEXT_LENGTH= 2000;

	private static final Logger logger = LoggerFactory.getLogger(NewsVOValidator.class);

	public boolean supports(Class<?> clazz) {
		return News.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {

		News news = (News) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "valid.newsTitle");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shortText", "valid.shortText");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fullText", "valid.fullText");

		if(news.getTitle().length() > TITLE_LENGTH) {
			errors.rejectValue("title","valid.newsTitle");
		}
		if(news.getShortText().length() > SHORT_TEXT_LENGTH) {
			errors.rejectValue("shortText","valid.shortTextToLong");
		}
		if(news.getFullText().length() > FULL_TEXT_LENGTH) {
			errors.rejectValue("fullText","valid.fullTextToLong");
		}

		if(errors.hasErrors()) {
			logger.debug("Adding news proccess doesnt pass validation");
		}

	}

}
