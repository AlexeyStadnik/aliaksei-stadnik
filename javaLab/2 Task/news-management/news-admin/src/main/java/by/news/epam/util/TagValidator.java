package by.news.epam.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import by.stadnik.epam.entity.Tag;

public class TagValidator implements Validator {
	
	private static final Logger logger = LoggerFactory.getLogger(TagValidator.class);
	public static final int TAG_NAME_LENGTH = 30;


	public boolean supports(Class<?> clazz) {
		return Tag.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {
		Tag tag = (Tag) target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tagName", "valid.tagName");
		
		if(tag.getTagName() != null && tag.getTagName().length() > TAG_NAME_LENGTH) {
			errors.rejectValue("tagName","valid.tagNameIsToLong");
		}
		
		if(errors.hasErrors()) {
			logger.debug("Adding new tag doesnt pass validation process");
		}
		
		
	}

}
