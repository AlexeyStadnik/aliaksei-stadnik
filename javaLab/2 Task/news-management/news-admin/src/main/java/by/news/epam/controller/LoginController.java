package by.news.epam.controller;

import java.util.Locale;
import java.util.ResourceBundle;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class LoginController {

	public static final String LOGIN_URL = "login";
	public static final String ERROR_URL = "403page";
	public static final String DENIED_URL = "redirect:login?denied";

	public static final String MESSAGE_PARAM = "message";
	public static final String LABEL_PARAM = "label";

	@RequestMapping("/")
	public String startPage(ModelMap map) {
		return "redirect:adminStartPage";
		//return "adminStartPage";
	}

	@RequestMapping(LOGIN_URL)  
	public ModelAndView getLoginForm(Locale locale,  
			@RequestParam(required = false) String authfailed, String logout,  
			String denied) {  
		ResourceBundle bundle = ResourceBundle.getBundle(LABEL_PARAM,locale);
		String message = "";  
		if (authfailed != null) {  
			message = bundle.getString("login.error.invalidUsername");  
		} else if (logout != null) {  
			message = bundle.getString("login.loggedOut");
		} else if (denied != null) {  
			message = bundle.getString("login.error.accessDenied");  
		}  
		return new ModelAndView(LOGIN_URL, MESSAGE_PARAM, message);  
	} 

	@RequestMapping(ERROR_URL)  
	public String ge403denied() {  
		return DENIED_URL;  
	}

}
