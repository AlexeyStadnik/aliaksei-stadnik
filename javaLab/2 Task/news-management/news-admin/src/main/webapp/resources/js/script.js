
$(".accordion").accordion({
	header : "h3",
	collapsible : true,
	active : false
});


$(function() {
	$(".datepicker").datepicker({
		changeMonth : true,
		changeYear : true,
		numberOfMonths : 1,
		altField: "#creationDate",
		altFormat : 'yy-mm-dd 00:00:00',
		dateFormat : 'yy-mm-dd',
		beforeShow : function() {
			$(".ui-datepicker").css('font-size', 12)
			$(".ui-datepicker-month").css('color', '#41A2D7')
		},
		minDate : 0,
		maxDate : "+1Y"
	});
	//$( "#datepicker" ).datepicker( "option", $.datepicker.regional['fr'] );
});


$('input[type="submit"]').button().size(20).css(' font-weight', 'normal');

$(".author-check-box").dropdownchecklist({
	textFormatFunction : function(options) {
		var selectedOptions = options.filter(":selected");
		var countOfSelected = selectedOptions.size();
		var size = options.size();
		switch (countOfSelected) {
		case 0:
			return "-";
		case 1:
			return selectedOptions.text();
		}
	}
});
$(".tag-check-box").dropdownchecklist({
	textFormatFunction : function(options) {
		var selectedOptions = options.filter(":selected");
		var countOfSelected = selectedOptions.size();
		var size = options.size();
		switch (countOfSelected) {
		case 0:
			return "-";
		
		default:
			return countOfSelected + " Tags";
		}
	}
});
$(".page-number").button().css({
	'width' : '12px',
	'height' : '12px'
});
$(".selected-page-number").button();
$(".selected-page-number").prop("disabled", true).addClass(
		"ui-state-disabled");