<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">

		<h1 style="margin: 0em;">
			<spring:message code="addNews" />
		</h1>
		<form:form method="POST" action="/news-admin/adminAddNews"
			style="margin-bottom: 0em;">
			<table>
				<tr>
					<td><form:label path="news.title">
							<spring:message code="newsForm.title" />
						</form:label></td>
					<td><form:input path="news.title" /></td>
					<td><form:errors path="news.title" class="error-message" /></td>
					<td><form:hidden path="news.newsId"
							value="${command.news.newsId}" />
				</tr>
				<tr>
					<td><form:label path="news.creationDate">
							<spring:message code="newsForm.date" />
						</form:label></td>

					<td><input type="text" class="datepicker" value=" <fmt:formatDate value='${command.news.creationDate}'/>" /></td>
					<td><form:input type="hidden" id="creationDate"
							path="news.creationDate" value="${news.creationDate}" /></td>
					<td><form:errors path="news.creationDate"
							class="error-message" /></td>
				</tr>

				<tr>
					<td><form:label path="news.shortText">
							<spring:message code="newsForm.shortText" />
						</form:label></td>
					<td><form:textarea path="news.shortText" rows="7" cols="60" /></td>
					<td><form:errors path="news.shortText"
							class="error-message" /></td>
				</tr>
				<tr>
					<td><form:label path="news.fullText">
							<spring:message code="newsForm.fullText" />
						</form:label></td>
					<td><form:textarea path="news.fullText" rows="15" cols="60" /></td>
					<td><form:errors path="news.fullText"
							class="error-message" /></td>
				</tr>
				<tr>
					<td><select name="authorId" class="author-check-box">
							<option value="">No author</option>
							<c:forEach var="author" items="${authorList}">
								<option value="${author.authorId}" name="${author.authorId}"
									${command.authorId == author.authorId ? 'selected' : ''}><c:if test="${author.expired != null}"><span class="error-message"><spring:message code="author.expired" /></span></c:if>${author.authorName}</option>
							</c:forEach>
					</select></td>
					<td><form:errors path="authorId" class="error-message" /></td>
				</tr>
				<tr>

					<td><select name="tagIdList" class="tag-check-box" multiple=""
						style="display: none;">
							<c:forEach var="tag" items="${tagList}">
								<option name="${tag.tagId}" value="${tag.tagId}"
									<c:forEach var="selectedTag" items="${command.tagIdList}">${selectedTag == tag.tagId ? 'selected' : ''}</c:forEach>>${tag.tagName}</option>
							</c:forEach>
					</select></td>

					<td><form:errors path="tagIdList" class="error-message" /></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit"
						value="<spring:message code="button.add"/>" /></td>
				</tr>
			</table>
		</form:form>
	<script src="<c:url value="/resources/js/script.js" />"></script>
	<script src="<c:url value="/resources/js/authorCheckBox.js" />"></script>
	</tiles:putAttribute>
</tiles:insertDefinition>
