<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
   
		<h1>Form</h1>
		<form:form method="POST" action="addNews">
			<table>
				<tr>
					<td><form:label path="news.title">Title</form:label></td>
					<td><form:input path="news.title" /></td>
					<td><form:errors path="news.title" cssStyle="color: #ff0000;"/></td>
					
				</tr>
				<tr>
					<td><form:label path="news.shortText">ShortText</form:label></td>
					<td><form:input path="news.shortText" /></td>
					<td><form:errors path="news.shortText" cssStyle="color: #ff0000;"/></td>
				</tr>
				<tr>
					<td><form:label path="news.fullText">FullText</form:label></td>
					<td><form:textarea path="news.fullText"  rows="5" cols="30"  /></td>
					<td><form:errors path="news.fullText" cssStyle="color: #ff0000;"/></td>
				</tr>
				
				<select name="authorId" class="author-check-box">
						<option value="" label="" /> -
						<c:forEach var="author" items="${authorList}">
							<option value="${author.authorId}" label="${author.authorName}" />
								${author.authorName}
						</c:forEach>
					</select>
					<td><form:errors path="authorId" cssStyle="color: #ff0000;"/></td>
					<select  name="tagIdList" class="tag-check-box" multiple=""
						style="display: none;">
						<c:forEach var="tag" items="${tagList}">
							<option name="${tag.tagId}" value="${tag.tagId}">${tag.tagName}</option>
						</c:forEach>
					</select>
				
				<td><form:errors path="tagIdList" cssStyle="color: #ff0000;"/></td>
				<tr>
					<td colspan="2"><input type="submit" value="Submit" /></td>
				</tr>
			</table>
		</form:form>
		
		<script>
		$(".author-check-box").dropdownchecklist();
		$(".tag-check-box").dropdownchecklist();	
		$( ".page-number .selected-page-number" ).button().css({'width':'15%'});
		//$( ".selected-page-number" ).button().css({ 'color': 'red'});
		
		
		</script>

	</tiles:putAttribute>
</tiles:insertDefinition>
