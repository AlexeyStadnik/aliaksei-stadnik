<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">

	
		<div class="app-body">
			<div class="news-form">
				<table>

					<tr>
						<td class="news-title">${newsVO.news.title}</td>
					</tr>

					<tr>
						<td>(by ${newsVO.author.authorName})</td>
					</tr>

					<p class="modification-date">${newsVO.news.modificationDate}</p>


					<tr>
						<td>${newsVO.news.fullText}</td>
					</tr>
					<tr>
						<div>
							<table class="comment-form">
								<c:forEach var="comment" items="${newsVO.commentsList}">
									<tr>
										<td class="comment-creation-date">${comment.creationDate}</td>
									</tr>
									<tr>
										<td class="comment-text">${comment.commentText}<a
											class="delete-button"
											href="deleteComment?commentId=${comment.commentId}"></a></td>
									</tr>
								</c:forEach>

									<tr>
									<table>
								<form:form method="POST" action="addComment">
									
										<tr>
					<td ><form:label path="commentText"></form:label></td>	
					<td ><form:textarea class="new-comment-text" path="commentText" rows="5" cols="30" ></form:textarea></td>
				</tr>
										<tr>
											<td colspan="2"><input type="submit" value="Post comment" /></td>
										</tr>
									
								</form:form>
								</table>
									</tr>
							</table>
						</div>


					</tr>
				</table>
			</div>
		</div>
		<script>
			$(".delete-button").button().css({
				width : '5%',
				height : '50%'
			}).html('X');

			//$( ".selected-page-number" ).button().css({ 'color': 'red'});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>