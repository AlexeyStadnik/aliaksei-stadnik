<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<h1>
			<spring:message code="error.errorPage" />
		</h1>
		<table>

			<tr>
				<td><spring:message code="error.errorText" /></td>
			</tr>


			<tr>
				<td><spring:message code="error.exception" /></td>
				<td>LOCK</td>
			</tr>

			<tr>
				<td><spring:message code="error.url" /></td>
				<td>LOCK</td>
			</tr>


		</table>

	</tiles:putAttribute>
</tiles:insertDefinition>