<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/resources/style/tiles.css" />"
	rel="stylesheet">
<link
	href="<c:url value="/resources/ui.dropdownchecklist.themeroller.css" />"
	rel="stylesheet">
<script src="<c:url value="/resources/jquery-1.11.3.js" />"></script>
<script src="<c:url value="/resources/jquery-1.6.1.min.js" />"></script>

<script
	src="<c:url value="/resources/jquery-ui-1.8.13.custom.min.js" />"></script>
<script src="<c:url value="/resources/ui.dropdownchecklist.js" />"></script>
<link
	href="<c:url value="/resources/style/ui.dropdownchecklist.themeroller.css" />"
	rel="stylesheet">
<link
	href="<c:url value="/resources/style/jquery-ui-1.8.4.custom.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/style/page-style.css" />"
	rel="stylesheet">
	<link href="<c:url value="/resources/style/jquery-ui.css" />"
	rel="stylesheet">
	<link href="<c:url value="/resources/style/jquery-ui.theme.css" />"
	rel="stylesheet">
	

<title>News portal</title>
</head>
<body>
	<div class="page">
		<tiles:insertAttribute name="header" />
		<div class="content">
			<tiles:insertAttribute name="menu" />
			<tiles:insertAttribute name="body" />
		</div>
		<tiles:insertAttribute name="footer" />
	</div>


</body>
</html>