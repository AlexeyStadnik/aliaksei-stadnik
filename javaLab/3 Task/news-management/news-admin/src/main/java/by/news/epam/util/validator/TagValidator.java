package by.news.epam.util.validator;

import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import by.stadnik.epam.entity.Tag;

public class TagValidator implements Validator {
	private static final Logger logger = LoggerFactory.getLogger(TagValidator.class);
	private static final String BUNDLE_PARAM = "properties";
	public boolean supports(Class<?> clazz) {
		return Tag.class.equals(clazz);
	}
	public void validate(Object target, Errors errors) {
		int tagLength = Integer.parseInt(ResourceBundle.getBundle(BUNDLE_PARAM).getString("tag.name.length"));
		Tag tag = (Tag) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tagName", "valid.tagName");

		if(tag.getTagName() != null && tag.getTagName().length() > tagLength) {
			errors.rejectValue("tagName","valid.tagNameIsToLong");
		}

		if(errors.hasErrors()) {
			logger.debug("Adding new tag doesnt pass validation process");
		}	
	}
}