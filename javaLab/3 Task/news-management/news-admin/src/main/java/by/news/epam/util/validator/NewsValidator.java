package by.news.epam.util.validator;

import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import by.stadnik.epam.entity.News;

public class NewsValidator implements Validator {
	private static final String BUNDLE_PARAM = "properties";
	private static final Logger logger = LoggerFactory.getLogger(NewsValidator.class);
	public boolean supports(Class<?> clazz) {
		return News.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {
		ResourceBundle resourceBundle = ResourceBundle.getBundle(BUNDLE_PARAM);
		int titleLength = Integer.parseInt(resourceBundle.getString("title.length"));
		int shortTextLength = Integer.parseInt(resourceBundle.getString("short.text.length"));
		int fullTextLength= Integer.parseInt(resourceBundle.getString("full.text.length"));

		News news = (News) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "valid.newsTitle");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shortText", "valid.shortText");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fullText", "valid.fullText");
		if(news.getTitle().length() > titleLength) {
			errors.rejectValue("title","valid.newsTitle");
		}
		if(news.getShortText().length() > shortTextLength) {
			errors.rejectValue("shortText","valid.shortTextToLong");
		}
		if(news.getFullText().length() > fullTextLength) {
			errors.rejectValue("fullText","valid.fullTextToLong");
		}
		if(errors.hasErrors()) {
			logger.debug("Adding news proccess doesnt pass validation");
		}
	}
}
