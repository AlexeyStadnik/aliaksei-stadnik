package by.news.epam.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import by.news.epam.util.ControllerException;
import by.stadnik.epam.entity.Comment;
import by.stadnik.epam.entity.News;
import by.stadnik.epam.entity.NewsFormVO;
import by.stadnik.epam.service.CommentsService;
import by.stadnik.epam.service.ServiceException;
import by.stadnik.epam.service.ServiceManager;

@Controller
public class NewsPageController {
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(NewsPageController.class);

	public static final String WATCH_NEWS= "adminWatchNews/{newsId}";
	public static final String WATCH_NEWS_URL = "adminWatchNews";
	public static final String DELETE_COMMENT_URL = "deleteComment";

	public static final String NEWS_FORM_PARAM = "newsForm";
	public static final String NEWS_ID_PARAM = "newsId";
	public static final String COMMENT_ID_PARAM = "commentId";

	@Autowired
	private CommentsService commentsService;
	@Autowired
	private ServiceManager serviceManager;

	@SuppressWarnings("serial")
	@RequestMapping(value = WATCH_NEWS, method = RequestMethod.GET)
	public String watchNews(@PathVariable(NEWS_ID_PARAM) Long newsId,Model model) throws ControllerException {
		News news = null;
		try {
			news = serviceManager.getNewsVO(newsId);
		} catch (ServiceException e) {
			throw new ControllerException("Exception while news with id = [" + newsId + "] getting process ",e);
		}
		if(!model.containsAttribute("comment")) {
			final Long finalNewsId = new Long(newsId);
			model.addAttribute("comment", (new Comment(){{setNews(new News(){{setNewsId(finalNewsId);}});}}));
		}
		model.addAttribute("newsVO",news);
		model.addAttribute("news",news);
		model.addAttribute("newsFormVO",(new NewsFormVO()));
		return WATCH_NEWS_URL;
	}

	@RequestMapping(value = DELETE_COMMENT_URL)
	public String deleteComment(@RequestParam(NEWS_ID_PARAM) Long newsId,@RequestParam(COMMENT_ID_PARAM) Long commentId) throws ControllerException {
		try {
			commentsService.deleteCommentByCommentId(commentId);
		}  catch (ServiceException e) {
			throw new ControllerException("Exception while comment with id = [" + commentId + "] deleting process ",e);
		}
		return "redirect:" + WATCH_NEWS_URL + "/" + newsId;
	}
}
