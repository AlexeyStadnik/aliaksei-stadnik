package by.news.epam.controller;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;




import org.eclipse.persistence.exceptions.OptimisticLockException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.HibernateOptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;

import by.news.epam.util.ControllerException;
import by.stadnik.epam.dao.DAOOptimisticLockException;
import by.stadnik.epam.entity.Author;
import by.stadnik.epam.entity.News;
import by.stadnik.epam.entity.NewsFormVO;
import by.stadnik.epam.entity.Tag;
import by.stadnik.epam.service.AuthorService;
import by.stadnik.epam.service.CommentsService;
import by.stadnik.epam.service.NewsService;
import by.stadnik.epam.service.ServiceException;
import by.stadnik.epam.service.ServiceManager;
import by.stadnik.epam.service.TagService;

@Controller
public class AddNewsController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AddNewsController.class);

	public static final String ADD_NEWS_URL = "adminAddNews";
	public static final String ADD_WATCH_NEWS_URL = "adminWatchNews";

	public static final String VALIDATOR_PARAM = "newsFormVOValidator";
	public static final String AUTHOR_LIST_PARAM = "authorList";
	public static final String TAG_LIST_PARAM = "tagList"; 
	public static final String LABEL_PARAM = "label";

	@Autowired
	@Qualifier(VALIDATOR_PARAM)
	private Validator validator;

	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@Autowired
	private TagService tagService;
	@Autowired
	private NewsService newsService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private CommentsService commentsService;
	@Autowired
	private ServiceManager serviceManager;

	@RequestMapping("/adminAddEditNews/add")
	public String editNews(Model model) throws ControllerException {

		NewsFormVO newsFormVO = new NewsFormVO();
		newsFormVO.setNews(new News());
		newsFormVO.getNews().setCreationDate(new Timestamp(System.currentTimeMillis()));
		newsFormVO.setTagIdList(new ArrayList<Long>());

		model.addAttribute("command",  newsFormVO);
		return ADD_NEWS_URL;
	}

	@RequestMapping("/adminAddEditNews/edit/{newsId}")
	public String addNews(@PathVariable Long newsId,Model model) throws ControllerException {
		NewsFormVO newsFormVO = null;
		try {
			newsFormVO = serviceManager.getNewsFormVO(newsId);
		}  
		catch (ServiceException e) {
			throw new ControllerException("Exception while news getting process with id [" + newsId + "]",e);
		}
		model.addAttribute("command",  newsFormVO);
		return ADD_NEWS_URL;
	}


	@RequestMapping("/adminAddNews")
	public String addNews(@ModelAttribute("command") @Validated NewsFormVO newsFormVO,
			BindingResult result,Model model,Locale locale) throws ControllerException {
		if(result.hasErrors()) {
			return ADD_NEWS_URL;
		}
		newsFormVO.getNews().setModificationDate(new Date(System.currentTimeMillis()));
		Long newsId = null;
		try {
			if(newsFormVO.getNews().getNewsId() == null) {
				newsFormVO.getNews().setCreationDate(new Timestamp(System.currentTimeMillis()));
				newsId = serviceManager.addNews(newsFormVO);
			} else {
				try {
					newsId = serviceManager.updateNewsFormVO(newsFormVO);
				}catch(DAOOptimisticLockException lockException) {
					LOGGER.error("Optimistic lock exception while updating news",lockException);
					ResourceBundle bundle = ResourceBundle.getBundle(LABEL_PARAM,locale);
					model.addAttribute("optimisticLockMessage", bundle.getString("optimisticLockMessage"));
					return ADD_NEWS_URL;
				}
			}
		} catch (ServiceException e) {
			throw new ControllerException("Exception while adding news process",e);
		}
		return "redirect:"+ ADD_WATCH_NEWS_URL + "/" + newsId;
	}

	@ModelAttribute(AUTHOR_LIST_PARAM)
	public List<Author> getAuthorList() throws ControllerException {
		List<Author> authorList = null;
		try {
			authorList = authorService.getAllAuthors();
		} catch (ServiceException e) {
			throw new ControllerException("Exception while geting list of authors",e);
		}
		return authorList;
	}

	@ModelAttribute(TAG_LIST_PARAM)
	public List<Tag> getTagList() throws ControllerException {
		List<Tag> tagList = null;
		try {
			tagList = tagService.getAllTag();
		} catch (ServiceException e) {
			throw new ControllerException("Exception while geting list of tags",e);
		}
		return tagList;
	}
}
