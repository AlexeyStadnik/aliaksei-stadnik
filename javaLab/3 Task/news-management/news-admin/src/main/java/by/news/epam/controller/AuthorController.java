package by.news.epam.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import by.news.epam.util.ControllerException;
import by.stadnik.epam.entity.Author;
import by.stadnik.epam.service.AuthorService;
import by.stadnik.epam.service.ServiceException;

@Controller
public class AuthorController {
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthorController.class);

	public static final String ADD_AUPDATE_AUTHOR_URL = "adminAddUpdateAuthor";
	public static final String ADD_AUTHOR_URL = "adminAddAuthor";
	public static final String ADMIN_ADD_AUPDATE_AUTHOR_URL = "adminAddUpdateAuthor";
	public static final String DELETE_AUTHOR_URL = "adminDeleteAuthor";

	public static final String AUTHOR_LIST_PARAM = "authorList";
	public static final String AUTHOR_ID_PARAM = "authorId";
	public static final int YEAR_PARAM = 116;

	public static final String AUTHOR_VALIDATOR = "authorValidator";

	@Autowired
	AuthorService authorService;

	@Autowired
	@Qualifier(AUTHOR_VALIDATOR)
	private Validator validator;

	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@RequestMapping(value=ADD_AUPDATE_AUTHOR_URL)
	public String addUpdateAuthor(Model model) throws ControllerException {
		model.addAttribute(AUTHOR_LIST_PARAM, getAuthorList());
		model.addAttribute("command",new Author());
		return ADMIN_ADD_AUPDATE_AUTHOR_URL;
	}

	@RequestMapping(value=DELETE_AUTHOR_URL)
	public String deleteAuthor(@RequestParam(AUTHOR_ID_PARAM) Long authorId) throws ControllerException {
		try {
			authorService.deleteAuthor(authorId);
		} catch (ServiceException e) {
			throw new ControllerException("Exception while deleting author with id [" + authorId + "]" ,e);
		}
		return "redirect:" + ADD_AUPDATE_AUTHOR_URL;
	}

	@RequestMapping(value="adminAddAuthor")
	public String addAuthor(Model model,@ModelAttribute("command") @Validated Author author,BindingResult result) throws ControllerException {
		if(result.hasErrors()) {
			return ADMIN_ADD_AUPDATE_AUTHOR_URL;
		}
		Long authorId = null;
		try {
			authorId = authorService.addAuthor(author);
		} catch (ServiceException e) {
			throw new ControllerException("Exception while adding author with id [" + authorId + "]" ,e);
		}
		return "redirect:" + ADD_AUPDATE_AUTHOR_URL;
	}

	@RequestMapping(value="adminUpdateAuthor")
	public String updateAuthor(@ModelAttribute("command") @Validated Author author,BindingResult result) throws ControllerException {
		if(result.hasErrors()) {
			return ADMIN_ADD_AUPDATE_AUTHOR_URL;
		}
		try {
			authorService.updateAuthor(author);
		} catch (ServiceException e) {
			throw new ControllerException("Exception while updating author" ,e);
		}
		return "redirect:" + ADD_AUPDATE_AUTHOR_URL;
	}

	@ModelAttribute(AUTHOR_LIST_PARAM)
	public List<Author> getAuthorList() throws ControllerException {
		List<Author> authorList = null;
		try {
			authorList = authorService.getAllAuthors();
		} catch (ServiceException e) {
			throw new ControllerException("Exception while geting list of authors",e);
		}
		return authorList;
	}
}
