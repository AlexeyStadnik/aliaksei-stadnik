package by.news.epam.controller;

import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import by.news.epam.util.ControllerException;
import by.stadnik.epam.entity.Comment;
import by.stadnik.epam.service.AuthorService;
import by.stadnik.epam.service.CommentsService;
import by.stadnik.epam.service.NewsService;
import by.stadnik.epam.service.ServiceException;
import by.stadnik.epam.service.ServiceManager;
import by.stadnik.epam.service.TagService;

@Controller
public class AddCommentController {
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(AddCommentController.class);
	
	public static final String WATCH_NEWS= "adminWatchNews/{newsId}";
	public static final String WATCH_NEWS_URL = "adminWatchNews";	
	public static final String ADD_COMMENT_URL = "addComment";
	@Autowired
	private AuthorService authorService;
	@Autowired
	private TagService tagService;
	@Autowired
	private CommentsService commentsService;
	@Autowired
	private NewsService newsService;
	@Autowired
	private ServiceManager serviceManager;

	@Autowired
	@Qualifier("commentValidator")
	private Validator validator;

	@InitBinder("comment")
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@RequestMapping(value = ADD_COMMENT_URL, method=RequestMethod.POST)
	public String addComment(RedirectAttributes redirectAttributes,@Validated @ModelAttribute("comment") Comment comment,BindingResult result) throws ControllerException {	
		if(result.hasErrors()) {
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.comment", result);
			redirectAttributes.addFlashAttribute("comment", comment);
			return "redirect:" + WATCH_NEWS_URL  + "/" + comment.getNews().getNewsId();

		}
		comment.setCreationDate(new Timestamp(System.currentTimeMillis()));
		try {
			commentsService.addComment(comment);
		} catch (ServiceException e) {
			throw new ControllerException("Exception while comment with id = [" + comment.getCommentId() + "] adding process ",e);
		}
		return "redirect:" + WATCH_NEWS_URL  + "/" + comment.getNews().getNewsId();
	}

}
