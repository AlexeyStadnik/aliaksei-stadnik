package by.news.epam.controller;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import by.news.epam.util.ControllerException;
import by.stadnik.epam.entity.Tag;
import by.stadnik.epam.service.ServiceException;
import by.stadnik.epam.service.TagService;

@Controller
public class TagController {

	private static final String ADD_UPDATE_TAG_URL = "adminAddUpdateTag";

	private static final String TAG_LIST_PARAM = "tagList";
	private static final String LABEL_PARAM = "label";

	@Autowired
	private TagService tagService;

	@Autowired
	@Qualifier("tagValidator")
	private Validator validator;

	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(TagController.class);

	@RequestMapping(value=ADD_UPDATE_TAG_URL)
	public String addUpdateTag(Model model) throws ControllerException {
		model.addAttribute(TAG_LIST_PARAM, getTagList());
		model.addAttribute("command", new Tag());
		return ADD_UPDATE_TAG_URL;
	}

	@RequestMapping(value="adminDeleteTag")
	public String deleteTag(@ModelAttribute("command")  Tag tag,@RequestParam(required = false)Long tagId,
			Locale locale,Model model) throws ControllerException {
		try {
			if(tagId != null) {
				tagService.deleteTag(tagId);
			}
		}catch (DataIntegrityViolationException e) {
			LOGGER.error("DataIntegrityViolationException while deleting tag",e);
			ResourceBundle bundle = ResourceBundle.getBundle(LABEL_PARAM,locale);
			model.addAttribute("dataIntegrityViolationMessage", bundle.getString("tagDeleteErrorMessage"));
			return "/" + ADD_UPDATE_TAG_URL;
		} catch (ServiceException e) {
			LOGGER.error("Exception while deleting tag with id [" + tagId + "]" ,e);
			throw new ControllerException("Exception while deleting tag with id [" + tagId + "]" ,e);
		}
		return "redirect:" + ADD_UPDATE_TAG_URL;
	}

	@RequestMapping(value="adminAddTag")
	public String addTag(@ModelAttribute("command") @Validated Tag tag,BindingResult result) throws ControllerException {
		if(result.hasErrors()) {
			return ADD_UPDATE_TAG_URL;
		}
		Long tagId = null;
		try {
			tagId = tagService.addTag(tag);
		} catch (ServiceException e) {
			LOGGER.error("Exception while adding tag with id [" + tagId + "]" ,e);
			throw new ControllerException("Exception while adding tag with id [" + tagId + "]" ,e);
		}

		return "redirect:" + ADD_UPDATE_TAG_URL;
	}

	@RequestMapping(value="adminUpdateTag")
	public String updateTag(@ModelAttribute("command") @Validated Tag tag,BindingResult result) throws ControllerException {
		if(result.hasErrors()) {
			return ADD_UPDATE_TAG_URL;
		}
		try {
			tagService.updateTag(tag);
		} catch (ServiceException e) {
			LOGGER.error("Exception while adding tag" ,e);
			throw new ControllerException("Exception while adding" ,e);
		}
		return "redirect:" + ADD_UPDATE_TAG_URL;
	}

	@ModelAttribute(TAG_LIST_PARAM)
	public List<Tag> getTagList() throws ControllerException {
		List<Tag> tagList = null;
		try {
			tagList = tagService.getAllTag();
		} catch (ServiceException e) {
			LOGGER.error("Exception while geting list of tags",e);
			throw new ControllerException("Exception while geting list of tags",e);
		}
		return tagList;
	}
}
