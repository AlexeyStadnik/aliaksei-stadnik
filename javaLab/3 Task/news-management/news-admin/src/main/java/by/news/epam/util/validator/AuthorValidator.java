package by.news.epam.util.validator;

import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import by.stadnik.epam.entity.Author;

public class AuthorValidator implements Validator {

	private static final String BUNDLE_PARAM = "properties";
	private static final Logger logger = LoggerFactory.getLogger(AuthorValidator.class);

	public boolean supports(Class<?> clazz) {
		return Author.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {
		int authorNameLength = Integer.parseInt(ResourceBundle.getBundle(BUNDLE_PARAM).getString("author.name.length"));
		Author author = (Author) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "authorName", "valid.authorName");
		if(author.getAuthorName() != null && author.getAuthorName().length() > authorNameLength) {
			errors.rejectValue("authorName","valid.authorNameIsToLong");
		}
		if(errors.hasErrors()) {
			logger.debug("Adding new author doesnt pass validation process");
		}
	}
}