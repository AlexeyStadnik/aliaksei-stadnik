package by.news.epam.util;

import java.util.List;

public class NewsListWrapper {

	private List<Long> newsIdList;

	public List<Long> getNewsIdList() {
		return newsIdList;
	}

	public void setNewsIdList(List<Long> newsId) {
		this.newsIdList = newsId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((newsIdList == null) ? 0 : newsIdList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsListWrapper other = (NewsListWrapper) obj;
		if (newsIdList == null) {
			if (other.newsIdList != null)
				return false;
		} else if (!newsIdList.equals(other.newsIdList))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NewsListWrapper [newsIdList=" + newsIdList + "]";
	}
}
