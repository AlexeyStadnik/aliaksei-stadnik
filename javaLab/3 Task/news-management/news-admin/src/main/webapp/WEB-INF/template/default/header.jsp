<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<div class="header">
	<h1><spring:message code="header.header"/></h1>
	
	<table style="float: right;">
	<tr>
		<c:url value="/j_spring_security_logout" var="logoutUrl" />
		<td><a style="float: right" href="${logoutUrl}"><spring:message code="header.logout"/> </a></td>
	</tr>
	<tr>
		<td><span style="float: right"> <a href="?lang=en"><spring:message code="header.en"/></a> | <a
			href="?lang=ru"><spring:message code="header.ru"/> </a>
		</span></td>
	</tr>
	<tr>
	<td style="float: right">
		<sec:authorize access="hasRole('ROLE_ADMIN')">
		<spring:message code="header.hello"/> <sec:authentication property="principal.username" />
	</sec:authorize>
	
	</td>
	
	</tr>
	</table>

</div>