$(".tag-check-box").dropdownchecklist({
				textFormatFunction : function(options) {
					var selectedOptions = options.filter(":selected");
					var countOfSelected = selectedOptions.size();
					var size = options.size();
					switch (countOfSelected) {
					case 0:
						return "-";
					
					default:
						return countOfSelected + " Tags";
					}
				}
			});