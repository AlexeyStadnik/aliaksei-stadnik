<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">

		<h1>
			<spring:message code="addNews" />
		</h1>
		<form:form method="POST" action="addNews">
			<table>
				<tr>
					<td><form:label path="news.title">
							<spring:message code="newsForm.title" />
						</form:label></td>
					<td><form:input path="news.title" /></td>
					<td><form:errors path="news.title" cssStyle="color: #ff0000;" /></td>

				</tr>
				<tr>
					<td><form:label path="news.shortText">
							<spring:message code="newsForm.shortText" />
						</form:label></td>
					<td><form:textarea path="news.shortText" rows="7" cols="60" /></td>
					<td><form:errors path="news.shortText"
							cssStyle="color: #ff0000;" /></td>
				</tr>
				<tr>
					<td><form:label path="news.fullText">
							<spring:message code="newsForm.fullText" />
						</form:label></td>
					<td><form:textarea path="news.fullText" rows="15" cols="60" /></td>
					<td><form:errors path="news.fullText"
							cssStyle="color: #ff0000;" /></td>
				</tr>
				<tr>
					<td><select name="authorId" class="author-check-box">
							<c:forEach var="author" items="${authorList}">
								<option value="${author.authorId}" label="${author.authorName}" />
								${author.authorName}
						</c:forEach>
					</select></td>
					<td><form:errors path="authorId" cssStyle="color: #ff0000;" /></td>
				</tr>
				<tr>

					<td><select name="tagIdList" class="tag-check-box" multiple=""
						style="display: none;">
							<c:forEach var="tag" items="${tagList}">
								<option name="${tag.tagId}" value="${tag.tagId}">${tag.tagName}</option>
							</c:forEach>
					</select></td>

					<td><form:errors path="tagIdList" cssStyle="color: #ff0000;" /></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit"
						value="<spring:message code="button.add"/>" /></td>
				</tr>
			</table>
		</form:form>

		<script>
			$(".author-check-box").dropdownchecklist();
			$(".tag-check-box").dropdownchecklist();
			$(".page-number .selected-page-number").button().css({
				'width' : '15%'
			});
		</script>

	</tiles:putAttribute>
</tiles:insertDefinition>
