<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">


		<div class="app-body">
			<div class="news-form">

				<div class="accordion">
					<c:forEach var="tag" items="${tagList}">
						<h3>${tag.tagName}</h3>
						<div>
							<form:form method="POST" action="updateTag">
								<input type="text" name="tagName" value="${tag.tagName}" />
								<input type="hidden" name="tagId" value="${tag.tagId}" />
								<input type="submit"
									class="jquery-button ui-button ui-widget ui-state-default ui-corner-all 
          ui-button-text-only"
									value="<spring:message code="button.updateAuthor"/>" />

							</form:form>

							<a href="deleteTag?tagId=${tag.tagId}"> <input type="submit"
								class="jquery-button ui-button ui-widget ui-state-default ui-corner-all 
          ui-button-text-only"
								value="<spring:message code="button.deleteAuthor"/>" />
							</a>


						</div>
					</c:forEach>
				</div>


				<tr>
					<table>
						<form:form method="POST" action="addTag">

							<tr>
								<td><form:label path="tagName"></form:label></td>
								<td><form:input class="new-comment-text" path="tagName"></form:input></td>
								<td><form:errors path="tagName" cssStyle="color: #ff0000;" /></td>

							</tr>
							</tr>

							<tr>
								<td colspan="2"><input type="submit"
									value="<spring:message code="button.add"/>" /></td>
							</tr>

						</form:form>
					</table>
				</tr>


			</div>
		</div>
		<script>
		$( ".accordion" ).accordion({ header: "h3", collapsible: true, active: false });
		$('input[type="submit"]').button().size(20);

		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>