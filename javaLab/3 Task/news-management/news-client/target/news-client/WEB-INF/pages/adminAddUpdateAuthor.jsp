<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">


		<div class="app-body">
			<div class="news-form">

				<div class="accordion">
					<c:forEach var="author" items="${authorList}">

						<h3>${author.authorName}</h3>
						<div>
							<form:form method="POST" action="updateAuthor">
								<input type="text" class="datepicker" name="expired"
									value="${author.expired}" class="date ui-corner-left" required
									autocomplete="off" onclick="this.blur();" />
								<input type="text" name="authorName"
									value="${author.authorName}" />
								<input type="hidden" name="authorId" value="${author.authorId}" />
								<input type="submit"
									class="jquery-button ui-button ui-widget ui-state-default ui-corner-all 
          ui-button-text-only"
									value="<spring:message code="button.updateAuthor"/>" />

							</form:form>

							<a href="deleteAuthor?authorId=${author.authorId}"> <input
								type="submit"
								class="jquery-button ui-button ui-widget ui-state-default ui-corner-all 
          ui-button-text-only"
								value="<spring:message code="button.deleteAuthor"/>" />
							</a>


						</div>

					</c:forEach>
				</div>

				<table>
					<form:form method="POST" action="addAuthor">

						<tr>
							<td><form:label path="authorName"></form:label></td>
							<td><form:input class="new-comment-text" path="authorName"></form:input></td>
							<td><form:errors path="authorName"
									class="ui-state-error ui-corner-all" /></td>

							<td><input type="text" class="datepicker" name="expired"
								value="${author.expired}" class="date ui-corner-left" required
								autocomplete="off" onclick="this.blur();" /></td>

						</tr>
						<tr>
							<td colspan="2"><input type="submit"
								value="<spring:message code="button.addAuthor"/>" /></td>
						</tr>

					</form:form>
				</table>


			</div>
		</div>
		<script>
			$(".accordion").accordion({
				header : "h3",
				collapsible : true,
				active : false
			});
			$('input[type="submit"]').button().size(20);
			$(function() {
				$(".datepicker").datepicker({
					changeMonth : true,
					changeYear : true,
					numberOfMonths : 1,
					dateFormat : 'yy-mm-dd 00:00:00.000',
					beforeShow : function() {
						$(".ui-datepicker").css('font-size', 12)
						$(".ui-datepicker-month").css('color', '#41A2D7')
					},
					minDate : 0,
					maxDate : "+1Y"
				});

			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>