<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
	<script src="<c:url value="/resources/js/script.js" />"></script>
	<script src="/news-client/resources/js/jquery.simplePagination.js"></script>
	<link
	href="<c:url value="/resources/style/simplePagination.css" />"
	rel="stylesheet">
	<script>$(function() {
		    $('#light-pagination').pagination({
		        
		        pages: '${noOfPages}',
		        currentPage:'${currentPage}',
		        hrefTextPrefix:'startPage?page='
		    });
		});</script>
		<div class="app-body">
			<form:form method="POST" action="filter" commandName="searchCriteria">


				<select name="authorId" class="author-check-box">
					<option value=""><spring:message code="noAuthor"/></option>
					<c:forEach var="author" items="${authorList}">
						<option value="${author.authorId}" name="${author.authorId}"
							${searchCriteria.authorId == author.authorId ? 'selected' : ''}>${author.authorName}</option>
					</c:forEach>
				</select>

				<select name="tagIdList" class="tag-check-box" multiple=""
					style="display: none;">
					<c:forEach var="tag" items="${tagList}">
						<option name="${tag.tagId}" value="${tag.tagId}"
							<c:forEach var="selectedTag" items="${searchCriteria.tagIdList}">${selectedTag == tag.tagId ? 'selected' : ''}</c:forEach>>${tag.tagName}</option>
					</c:forEach>
				</select>
				<input type="submit" name="submit"
					value=<spring:message code="button.find"/>>
				

			</form:form>
			
				<a href="resetSearchCriteria"><input type="submit" 
					value=<spring:message code="button.reset"/>></a>
			

			

				<c:forEach var="news" items="${newsList}" varStatus="status">
					<div class="news-form">
						<td><p1 class="main-news-title"> <a
								href="watchNews/${news.newsId}"><c:out value="${news.title}"/></a></p1></td>

						<span class="main-news-author">
							<td>(<spring:message code="news.by" />
								<c:out value="${news.author.authorName}"/>)
						</td>
							<td>${news.modificationDate}</td>
						</span> </br> </br> <span class="short-text">
							<td><c:out value="${news.shortText}"/></td>
						</span> <span class="main-news-author"> 
						${news.getTags().size()}
						<c:forEach var="tag"
								items="${news.tags}">
								<td><span class="tag-list">${tag.tagName}, </span></td>
							</c:forEach>
							
							<td>Comments(${fn:length(news.comments)})</td>
							
							 </br>

						</span>
					</div>

					
				</c:forEach>

				
			


		<div  id="light-pagination" class="pagination"></div>

		</div>

		<script src="<c:url value="/resources/js/newsListScript.js" />"></script>
	</tiles:putAttribute>
</tiles:insertDefinition>


