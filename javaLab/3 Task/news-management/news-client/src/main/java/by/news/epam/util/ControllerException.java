package by.news.epam.util;


public class ControllerException extends Exception {

	private static final long serialVersionUID = 6195132014524442082L;
	public ControllerException() {
		super();

	}

	public ControllerException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

	public ControllerException(String message, Throwable cause) {
		super(message, cause);

	}

	public ControllerException(String message) {
		super(message);

	}

	public ControllerException(Throwable cause) {
		super(cause);

	}
}
