package by.news.epam.util;

import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import by.stadnik.epam.entity.Comment;

public class CommentValidator implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(CommentValidator.class);
	private static final String BUNDLE_PARAM = "properties";
	public boolean supports(Class<?> clazz) {
		return Comment.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {
		int commentLength = Integer.parseInt(ResourceBundle.getBundle(BUNDLE_PARAM).getString("comment.text.length"));
		Comment comment = (Comment) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "commentText", "valid.commentText");
		if(comment.getCommentText() != null && comment.getCommentText().length() > commentLength) {
			errors.rejectValue("commentText","valid.commentTextToLong");
		}
		if(errors.hasErrors()) {
			logger.debug("Adding new comment does not pass validation process");
		}
	}
}