package by.news.epam.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;


@ControllerAdvice
public class ExceptionController {
	private static final Logger logger = LoggerFactory.getLogger(ExceptionController.class);

	public static final String EXCEPTION_PARAM = "exception";
	public static final String URL_PARAM = "url";
	public static final String ERROR_URL = "error";

	@ExceptionHandler(Exception.class)
	public ModelAndView handleError(HttpServletRequest request, Exception exception) {
		logger.error("Request: " + request.getRequestURL() + " raised " + exception);
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject(EXCEPTION_PARAM, exception);
		modelAndView.addObject(URL_PARAM, request.getRequestURL());
		modelAndView.setViewName(ERROR_URL);
		return modelAndView;
	}
}
