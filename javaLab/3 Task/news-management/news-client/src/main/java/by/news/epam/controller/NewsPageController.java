package by.news.epam.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import by.news.epam.util.ControllerException;
import by.stadnik.epam.entity.Comment;
import by.stadnik.epam.entity.News;
import by.stadnik.epam.entity.NewsFormVO;
import by.stadnik.epam.service.ServiceException;
import by.stadnik.epam.service.ServiceManager;


@Controller
public class NewsPageController {

	private static final Logger LOGGER = LoggerFactory.getLogger(NewsPageController.class);

	public static final String WATCH_NEWS= "watchNews/{newsId}";
	public static final String WATCH_NEWS_URL = "watchNews";
	public static final String NEWS_ID_PARAM = "newsId";

	@Autowired
	private ServiceManager serviceManager;
	
	@RequestMapping(value = WATCH_NEWS, method = RequestMethod.GET)
	public String watchNews(@PathVariable(NEWS_ID_PARAM) Long newsId,Model model) throws ControllerException {
		News news = null;
		try {
			news = serviceManager.getNewsVO(newsId);
		} catch (ServiceException e) {
			throw new ControllerException("Exception while news with id = [" + newsId + "] getting process ",e);
		}
		final Long finalNewsId = new Long(newsId);
		if(!model.containsAttribute("comment")) {
			model.addAttribute("comment", (new Comment(){{setNews(new News(){{setNewsId(finalNewsId);}});}}));
		}
		model.addAttribute("newsVO",news);
		model.addAttribute("news",news);
		model.addAttribute("newsFormVO",(new NewsFormVO()));
		return WATCH_NEWS_URL;
	}
}
