
			$(".author-check-box").dropdownchecklist({
				textFormatFunction : function(options) {
					var selectedOptions = options.filter(":selected");
					var countOfSelected = selectedOptions.size();
					var size = options.size();
					switch (countOfSelected) {
					case 0:
						return "-";
					case 1:
						return selectedOptions.text();
					}
				}
			});
			$(".tag-check-box").dropdownchecklist({
				textFormatFunction : function(options) {
					var selectedOptions = options.filter(":selected");
					var countOfSelected = selectedOptions.size();
					var size = options.size();
					switch (countOfSelected) {
					case 0:
						return "-";
					
					default:
						return countOfSelected + " Tags";
					}
				}
			});
			$(".page-number").button().css({
				'width' : '12px',
				'height' : '12px'
			});
			$(".selected-page-number").button();
			$(".selected-page-number").prop("disabled", true).addClass(
					"ui-state-disabled");
