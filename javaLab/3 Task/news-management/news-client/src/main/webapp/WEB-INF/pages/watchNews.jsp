<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">

		<div class="app-body">

				<div id="watchNews" class="news-form">
					<table>
					<tr><td class=""><a
								href="/news-client/startPage">Back</a></td></tr>
						<tr>
						
							<td class="news-title">${newsVO.title}</td>
							
						</tr>
						<tr>
							<td>(by ${newsVO.author.authorName})</td>
						</tr>
						<p class="modification-date">${newsVO.modificationDate} </p>
						
						

						<tr>
							<td>${newsVO.fullText}</td>
						</tr>
						<tr>
							<div>
								<table class="comment-form1">
									<c:forEach var="comment" items="${newsVO.comments}">
										<tr>
											<td class="comment-creation-date">${comment.creationDate}</td>
											
										</tr>
										<tr>
											<td class="comment-text"><span style="white-space: pre"><c:out value="${comment.commentText}"/></span></td>
										</tr>
									</c:forEach>

									<tr>
										<table>
											<form:form method="POST" action="/news-client/addComment"
												commandName="comment" modelAttribute="comment">

												<tr>
													<td><form:label path="commentText"></form:label></td>
													<td><form:textarea class="new-comment-text"
															path="commentText" rows="5" cols="30"></form:textarea></td>
													<td><form:errors path="commentText"
															class="error-message"/></td>
												</tr>
												<tr>
													<td><form:hidden path="news.newsId" /></td>
												</tr>
												<tr>
													<td colspan="2"><input type="submit"
														value="Post comment" /></td>
												</tr>

											</form:form>
										</table>
									</tr>
								</table>
							</div>
						</tr>
					</table>
				</div>

			</div>
		
		<script src="<c:url value="/resources/js/script.js" />"></script>
	</tiles:putAttribute>
</tiles:insertDefinition>