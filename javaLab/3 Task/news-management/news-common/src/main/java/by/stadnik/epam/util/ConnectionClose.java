package by.stadnik.epam.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.DataSourceUtils;

public class ConnectionClose {

	private static final Logger logger = LoggerFactory.getLogger(ConnectionClose.class);

	public static void close(Connection connection, Statement statement,DataSource dataSource ) {
		if(statement != null) {
			try {
				statement.close();
				logger.debug("statement has been closed");
			} catch (SQLException e) {
				logger.error("Exception while statement closing process");
			}
		}
		DataSourceUtils.releaseConnection(connection, dataSource);
		logger.info("connection has been closed");
	}

	public static void close(Connection connection, Statement statement,DataSource dataSource , ResultSet resultSet) {
		close(connection,statement,dataSource);
		if(resultSet != null) {
			try {
				resultSet.close();
				logger.debug("resultSet has been closed");
			} catch (SQLException e) {
				logger.error("Exception while resultSet closing process");
			}
		}
	}

}
