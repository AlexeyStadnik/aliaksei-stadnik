package by.stadnik.epam.service.jdbcImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import by.stadnik.epam.entity.News;
import by.stadnik.epam.entity.NewsFormVO;
import by.stadnik.epam.entity.Tag;
import by.stadnik.epam.service.AuthorService;
import by.stadnik.epam.service.CommentsService;
import by.stadnik.epam.service.NewsService;
import by.stadnik.epam.service.ServiceException;
import by.stadnik.epam.service.ServiceManager;
import by.stadnik.epam.service.TagService;
import by.stadnik.epam.util.SearchCriteria;
@Transactional (rollbackFor = Exception.class)
public class JDBCServiceManagerImpl implements ServiceManager  {

	private AuthorService authorService;
	private TagService tagService;
	private CommentsService commentsService;
	private NewsService newsService;

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	public void setCommentsService(CommentsService commentsService) {
		this.commentsService = commentsService;
	}

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public Long addNews(NewsFormVO newsVO) throws ServiceException {

		Long newsId = newsService.addNews(newsVO.getNews());
		if(newsVO.getTagIdList() != null) {
			tagService.addNewsTag(newsVO.getTagIdList(), newsId);
		}
		authorService.addNewsAuthor(newsVO.getAuthorId(), newsId);

		return newsId;
	}

	public void deleteNews(Long newsId) throws ServiceException {
		authorService.deleteNewsAuthor(newsId);
		tagService.deleteNewsTagByNewsId(newsId);
		commentsService.deleteCommentsByNewsId(newsId);
		newsService.deleteNews(newsId);
		return;
	}

	public News getNewsVO(Long newsId) throws ServiceException {
		News news = newsService.getNews(newsId);
		news.setAuthor(authorService.getAuthorByNewsId(newsId));	
		news.setComments(commentsService.getComments(newsId));	
		news.setTags(tagService.getTag(newsId)	);

		return news;
	}

	public NewsFormVO getNewsFormVO(Long newsId) throws ServiceException {
		NewsFormVO newsFormVO = new NewsFormVO();

		newsFormVO.setNews(newsService.getNews(newsId));
		newsFormVO.setAuthorId(authorService.getAuthorByNewsId(newsId).getAuthorId());
		newsFormVO.setTagIdList(getTagIdList(tagService.getTag(newsId)));

		return newsFormVO;
	}

	public Long updateNewsFormVO(NewsFormVO newsFormVO) throws ServiceException {
		newsService.updateNews(newsFormVO.getNews());
		updateTags(newsFormVO.getNews().getNewsId(), newsFormVO.getTagIdList());
		updateAuthor(newsFormVO.getNews().getNewsId(), newsFormVO.getAuthorId());
		return newsFormVO.getNews().getNewsId();
	}

	public void updateTags(Long newsId, List<Long> tagList) throws ServiceException {

		tagService.deleteNewsTagByNewsId(newsId);
		if(tagList != null) {
			tagService.addNewsTag(tagList, newsId);
		}
		return;
	}

	public void updateAuthor(Long newsId, Long authorId) throws ServiceException {

		authorService.deleteNewsAuthor(newsId);
		authorService.addNewsAuthor(authorId, newsId);
		return;
	}

	public List<News> getListOfNews(SearchCriteria searchCriteria,int firstIndex, int lastIndex) throws ServiceException {

		List<News> newsList = newsService.getListOfNews(searchCriteria,firstIndex,lastIndex);
		for(News news:newsList) {
			news.setTags(tagService.getTag(news.getNewsId()));
		}
		return newsList;
	}

	private List<Long> getTagIdList(List<Tag> tagList) {

		List<Long> tagIdList = new ArrayList<Long>();
		for(Tag tag:tagList) {
			tagIdList.add(tag.getTagId());
		}
		return tagIdList;
	}
}
