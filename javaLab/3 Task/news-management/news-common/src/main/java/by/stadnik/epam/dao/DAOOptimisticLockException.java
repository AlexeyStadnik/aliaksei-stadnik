package by.stadnik.epam.dao;
/**
 * <p><b>Purpose</b>:  This exception is used when optimistic locking feature is used.
 * It will be raised if the object being updated or deleted was changed or deleted from the database since
 * it as last read.
 */
public class DAOOptimisticLockException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 875843921318465758L;

	public DAOOptimisticLockException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DAOOptimisticLockException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public DAOOptimisticLockException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DAOOptimisticLockException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DAOOptimisticLockException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}	
}
