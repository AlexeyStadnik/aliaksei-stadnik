package by.stadnik.epam.dao.hibernateImpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.dao.TagDAO;
import by.stadnik.epam.entity.News;
import by.stadnik.epam.entity.Tag;

@Transactional (rollbackFor = Exception.class)
public class HibernateTagDAOImpl implements TagDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(HibernateTagDAOImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void deleteTag(Long tagId) throws DAOException {	
		LOGGER.debug("Deleting existing tag");
		Session session = sessionFactory.getCurrentSession();
		try {
			Tag tag = new Tag();
			tag.setTagId(tagId);
			session.delete(tag);
		} catch(HibernateException e) {
			throw new DAOException("Exception while tag deleting process. Tag id  = [" + tagId + "]",e);
		}
	}

	public Long addTag(Tag tag) throws DAOException {
		LOGGER.debug("Adding new tag");
		Session session = sessionFactory.getCurrentSession();
		try {
			session.save(tag);
		} catch(HibernateException e) {
			throw new DAOException("Exception while tag getting process. Tag - [" + tag + "]",e);
		}
		return tag.getTagId();
	}

	@SuppressWarnings("unchecked")
	public void addNewsTag(List<Long> tagList, Long newsId) throws DAOException {
		LOGGER.debug("Adding news-tag");
		Session session = sessionFactory.getCurrentSession();
		try {
			News news = (News) session.get(News.class, newsId);
			List<Tag> tags = (session.createCriteria(Tag.class).add(Restrictions.in("tagId",tagList)).list());
			news.setTags(tags);
			session.save(news);
		} catch(HibernateException e) {
			throw new DAOException("Exception while adding news tag process. News id - [" + newsId + "]",e);
		}

	}

	public void deleteNewsTagByNewsId(Long newsId) throws DAOException {
		LOGGER.debug("Deleting news-tag");
		Session session = sessionFactory.getCurrentSession();
		try {
			Query query = session.createQuery("FROM News as a LEFT JOIN FETCH  a.tags WHERE a.newsId="+ newsId);
			News news =  (News) query.uniqueResult();
			news.setTags(new ArrayList<Tag>());
			session.update(news);
		} catch(HibernateException e) {
			throw new DAOException("Exception while deleting news tag process. News id - [" + newsId + "]",e);
		}
	}

	public void deleteNewsTagByTagId(Long tagId) throws DAOException {
		LOGGER.debug("Deleting news-tag");
		Session session = sessionFactory.getCurrentSession();
		try {
			Query deleteQuery = session.createSQLQuery(
					"delete from NEWS_TAG WHERE tag_id=?");
			deleteQuery.setLong(0, tagId);
			deleteQuery.executeUpdate();
		} catch(HibernateException e) {
			throw new DAOException("Exception while deleting news tag process. Tag id - [" + tagId + "]",e);
		}

	}

	public List<Tag> getTag(Long newsId) throws DAOException {
		LOGGER.debug("Getting tag");
		News news = null;
		try {
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery("FROM News as a LEFT JOIN FETCH  a.tags WHERE a.newsId="+ newsId);
			news =  (News) query.uniqueResult();
		} catch(HibernateException e) {
			throw new DAOException("Exception while getting  tag process. News id = [" + newsId + "]",e);
		}
		return news.getTags();
	}

	public Tag getTagByTagId(Long tagId) throws DAOException {
		LOGGER.debug("Retrieving tag by tag id");  
		Session session = sessionFactory.getCurrentSession();
		Tag tag = null;
		try {
			tag = (Tag) session.get(Tag.class, tagId);
		} catch(HibernateException e) {
			throw new DAOException("Exception while getting tag process. Tag id - [" + tagId + "]",e);
		}
		return tag;
	}

	public void updateTag(Tag tag) throws DAOException {
		LOGGER.debug("Updating tag");  
		Session session = sessionFactory.getCurrentSession();
		try {
			session.update(tag);
		} catch(HibernateException e) {
			throw new DAOException("Exception while updating  tag process. Tag - [" + tag + "]",e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<Tag> getAllTags() throws DAOException {
		LOGGER.debug("Retrieving all tags");
		Session session = sessionFactory.getCurrentSession();
		Query query = null;
		try {
			query = session.createQuery("FROM  Tag");
		} catch(HibernateException e) {
			throw new DAOException("Exception while getting all  tag process.",e);
		}
		return  query.list();
	}

}
