package by.stadnik.epam.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.dao.NewsDAO;
import by.stadnik.epam.entity.News;
import by.stadnik.epam.service.NewsService;
import by.stadnik.epam.service.ServiceException;
import by.stadnik.epam.util.SearchCriteria;
@Transactional (rollbackFor = Exception.class)
public class NewsServiceImpl implements NewsService {

	private NewsDAO newsDAO;

	private static final Logger logger = LoggerFactory.getLogger(NewsServiceImpl.class);

	public void setNewsDAO(NewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}

	public Long addNews(News news) throws ServiceException {
		Long newsId = null;
		try {
			newsId = newsDAO.addNews(news);
		} catch (DAOException e) {
			logger.error("Exception while news adding process",e);
			throw new ServiceException("Exception while news adding process",e);
		}
		return newsId;
	}

	public void deleteNews(Long newsId) throws ServiceException {
		try {
			newsDAO.deleteNews(newsId);
		} catch (DAOException e) {
			logger.error("Exception while news deleting process",e);
			throw new ServiceException("Exception while news deleting process",e);
		}

	}

	public void updateNews(News news) throws ServiceException {
		try {
			newsDAO.updateNews(news);
		} catch (DAOException e) {
			logger.error("Exception while news updating process",e);
			throw new ServiceException("Exception while news updating process",e);
		}
	}

	public News getNews(Long newsId)  throws ServiceException  {
		News news = null;
		try {
			news = newsDAO.getNews(newsId);
		} catch (DAOException e) {
			logger.error("Exception while news getting process",e);
			throw new ServiceException("Exception while news getting process",e);
		}
		return news;
	}

	public List<News>getListOfNews(SearchCriteria searchCriteria,int firstIndex,int lastIndex) throws ServiceException {
		List<News> newsList = null;

		try {
			newsList = newsDAO.getListOfNews(searchCriteria,firstIndex,lastIndex);
			if(newsList != null) {
				for(News news:newsList) {
					if(news.getTags() != null) {
						news.getTags().size();
					}
					if(news.getComments() != null) {
						news.getComments().size();
					}
				}
			}
		} catch (DAOException e) {
			logger.error("Exception while news getting process",e);
			throw new ServiceException("Exception while news getting process",e);
		}
		return newsList;
	}

	public int getNumberOfNews(SearchCriteria searchCriteria)
			throws ServiceException {
		int returnNumber = 0;
		try {
			returnNumber = newsDAO.getNumberOfNews(searchCriteria);
		} catch (DAOException e) {
			logger.error("Exception while number of news getting process",e);
			throw new ServiceException("Exception while number of news getting process",e);
		}
		return returnNumber;
	}

}
