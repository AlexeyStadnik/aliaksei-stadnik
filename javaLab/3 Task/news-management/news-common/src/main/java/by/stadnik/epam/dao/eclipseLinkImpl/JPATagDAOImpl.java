package by.stadnik.epam.dao.eclipseLinkImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.dao.TagDAO;
import by.stadnik.epam.entity.News;
import by.stadnik.epam.entity.Tag;

@Transactional (rollbackFor = Exception.class)
public class JPATagDAOImpl implements TagDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(JPATagDAOImpl.class);

	@PersistenceContext
	EntityManager entityManager;


	public void deleteTag(Long tagId) throws DAOException {
		LOGGER.debug("Deleting existing tag");
		try {
			entityManager.remove(entityManager.find(Tag.class, tagId));
		} catch(PersistenceException e) {
			throw new DAOException("Exception while tag deleting process. Tag id  = [" + tagId + "]",e);
		}
	}

	public Long addTag(Tag tag) throws DAOException {
		LOGGER.debug("Adding new tag");
		try {
			entityManager.persist(tag);
		} catch(PersistenceException e) {
			throw new DAOException("Exception while tag getting process. Tag - [" + tag + "]",e);
		}
		return tag.getTagId();
	}

	public void addNewsTag(List<Long> tagIdList, Long newsId) throws DAOException {
		LOGGER.debug("Adding news-tag");
		List<Tag> tagList = new ArrayList<Tag>();
		try {
			News news = entityManager.find(News.class,newsId);
			for(Long tagId: tagIdList) {
				tagList.add(entityManager.find(Tag.class, tagId));
			}
			news.setTags(tagList);
		} catch(PersistenceException e) {
			throw new DAOException("Exception while adding news tag process. News id - [" + newsId + "]",e);
		}
	}

	public void deleteNewsTagByNewsId(Long newsId) throws DAOException {
		throw new UnsupportedOperationException("This operation is not supported by JPA implementation of TagDAO");
	}

	public void deleteNewsTagByTagId(Long tagId) throws DAOException {
		throw new UnsupportedOperationException("This operation is not supported by JPA implementation of TagDAO");
	}

	public List<Tag> getTag(Long newsId) throws DAOException {
		LOGGER.debug("Getting tag");
		News news = null;
		try {
			news = entityManager.find(News.class, newsId);
		} catch(PersistenceException e) {
			throw new DAOException("Exception while getting tag process. Tag id - [" + newsId + "]",e);
		}
		return news.getTags();
	}

	public Tag getTagByTagId(Long tagId) throws DAOException {
		LOGGER.debug("Getting tag");
		try {
			return entityManager.find(Tag.class, tagId);
		} catch(PersistenceException e) {
			throw new DAOException("Exception while deleting news tag process. Tag id - [" + tagId + "]",e);
		}
	}

	public void updateTag(Tag tag) throws DAOException {
		LOGGER.debug("Updating tag");
		try {
			entityManager.merge(tag);
		} catch(PersistenceException e) {
			throw new DAOException("Exception while updating news tag process. Tag - [" + tag + "]",e);
		}

	}

	public List<Tag> getAllTags() throws DAOException {
		LOGGER.debug("Getting all tags");
		List<Tag> tagList = null;
		try {
			tagList = entityManager.createQuery( "from Tag t", Tag.class ).getResultList();
		} catch(PersistenceException e) {
			throw new DAOException("Exception while getting all tags process",e);
		}
		return tagList;
	}
}
