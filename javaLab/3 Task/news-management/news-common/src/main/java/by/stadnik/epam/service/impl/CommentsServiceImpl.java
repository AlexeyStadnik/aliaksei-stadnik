package by.stadnik.epam.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import org.springframework.transaction.annotation.Transactional;

import by.stadnik.epam.dao.CommentsDAO;
import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.entity.Comment;
import by.stadnik.epam.service.CommentsService;
import by.stadnik.epam.service.ServiceException;
@Transactional (rollbackFor = Exception.class)
public class CommentsServiceImpl implements CommentsService {
	
	private CommentsDAO commentsDAO;
	private static final Logger logger = LoggerFactory.getLogger(CommentsServiceImpl.class);
	
	

	public void setCommentsDAO(CommentsDAO commentsDAO) {
		this.commentsDAO = commentsDAO;
	}

	public Long addComment(Comment comment) throws ServiceException {
		Long commentId = null;
		try {
			commentId = commentsDAO.addComment(comment);
		} catch (DAOException e) {
			logger.error("Exception while comment adding process",e);
			throw new ServiceException("Exception while comment adding process",e);
		}
		return commentId;
	}

	public void deleteCommentByCommentId(Long commentId)
			throws ServiceException {
		
		try {
			commentsDAO.deleteCommentByCommentId(commentId);
		} catch (DAOException e) {
			logger.error("Exception while comment deleting process",e);
			throw new ServiceException("Exception while comment deleting process",e);
		}
		
	}

	public void deleteCommentsByNewsId(Long newsId) throws ServiceException {
		try {
			commentsDAO.deleteCommentsByNewsId(newsId);
		} catch (DAOException e) {
			logger.error("Exception while comment deleting process",e);
			throw new ServiceException("Exception while comment deleting process",e);
		}
		
	}

	public void updateComment(Comment comment) throws ServiceException {
		try {
			commentsDAO.updateComment(comment);
		} catch (DAOException e) {
			logger.error("Exception while comment updating process",e);
			throw new ServiceException("Exception while comment updating process",e);
		}
		
	}

	public List<Comment> getComments(Long newsId) throws ServiceException {
		List<Comment> commentList = null;
		try {
			commentList = commentsDAO.getComments(newsId);
		} catch (DAOException e) {
			logger.error("Exception while comment getting process",e);
			throw new ServiceException("Exception while comment getting process",e);
		}
		return commentList;
	}

	public Comment getCommentByCommentId(Long commentId) throws ServiceException {
		Comment comment = null;
		try {
			comment = commentsDAO.getCommentByCommentId(commentId);
		} catch (DAOException e) {
			logger.error("Exception while comment getting process",e);
			throw new ServiceException("Exception while comment getting process",e);
		}
		return comment;
	}

}
