package by.stadnik.epam.dao.eclipseLinkImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import by.stadnik.epam.dao.CommentsDAO;
import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.entity.Comment;
import by.stadnik.epam.entity.News;

public class JPACommentsDAOImpl implements CommentsDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(JPACommentsDAOImpl.class);

	@PersistenceContext
	EntityManager entityManager;

	public Long addComment(Comment comment) throws DAOException {
		LOGGER.debug("Adding new comment");
		News news = null;
		try {
			news = entityManager.find(News.class, comment.getNews().getNewsId());
			comment.setNews(news);
			news.getComments().add(comment);
			entityManager.merge(news);
		} catch(PersistenceException e) {
			throw new DAOException("Exception while comment saving process. Comments - [" + comment + "]",e);
		}
		return comment.getCommentId();
	}

	public void deleteCommentByCommentId(Long commentId) throws DAOException {
		LOGGER.debug("Deleting comment by commentId");
		Comment comment = null;
		try {
			comment = entityManager.find(Comment.class,commentId);
			entityManager.remove(comment);
			entityManager.getEntityManagerFactory().getCache().evict(News.class,comment.getNews().getNewsId());
		} catch(PersistenceException e) {
			throw new DAOException("Exception while comment deleting process. Comment id = [" + commentId + "]",e);
		}
	}

	public void deleteCommentsByNewsId(Long newsId) throws DAOException {
		LOGGER.debug("Deleting existing comments based on news's id");
		try {
			entityManager.createQuery( "DELETE from Comment c where newsId=" + newsId, Comment.class );	
		} catch(PersistenceException e) {
			throw new DAOException("Exception while comments deleting process. News id = [" + newsId + "]",e);
		}
	}

	public void updateComment(Comment comment) throws DAOException {
		LOGGER.debug("Editing existing creditCard");
		try {
			entityManager.merge(comment);
		} catch(PersistenceException e) {
			throw new DAOException("Exception while comment updating process. Comment = [" + comment + "]",e);
		}
	}

	public List<Comment> getComments(Long newsId) throws DAOException {
		LOGGER.debug("Retrieving all comment by newsId");
		List<Comment> commentList = null;
		try {
			commentList = entityManager.createQuery( "from Comment c where newsId=" + newsId, Comment.class ).getResultList();
		} catch(HibernateException e) {
			throw new DAOException("Exception while comment retrieving  process. News id = [" + newsId + "]",e);
		}
		return commentList;
	}

	public Comment getCommentByCommentId(Long commentId) throws DAOException {
		LOGGER.debug("Retrieving comment by commentId");  
		try {
			return entityManager.find(Comment.class,commentId);
		} catch(HibernateException e) {
			throw new DAOException("Exception while comment retrieving  process. Comment id = [" + commentId + "]",e);
		}
	}
}
