package by.stadnik.epam.service.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import by.stadnik.epam.entity.Author;
import by.stadnik.epam.entity.News;
import by.stadnik.epam.entity.NewsFormVO;
import by.stadnik.epam.entity.Tag;
import by.stadnik.epam.service.AuthorService;
import by.stadnik.epam.service.CommentsService;
import by.stadnik.epam.service.NewsService;
import by.stadnik.epam.service.ServiceException;
import by.stadnik.epam.service.ServiceManager;
import by.stadnik.epam.service.TagService;
import by.stadnik.epam.util.SearchCriteria;


@Transactional (rollbackFor = Exception.class)
public class ServiceManagerImpl implements ServiceManager  {

	private AuthorService authorService;
	private TagService tagService;
	private CommentsService commentsService;
	private NewsService newsService;

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	public void setCommentsService(CommentsService commentsService) {
		this.commentsService = commentsService;
	}

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public Long addNews(NewsFormVO newsVO) throws ServiceException {
		News news = newsVO.getNews();
		news.setAuthor(new Author());
		news.getAuthor().setAuthorId(newsVO.getAuthorId());
		List<Tag> tags = new ArrayList<Tag>();
		if (newsVO.getTagIdList() != null) {
			for (Long tagId : newsVO.getTagIdList()) {
				Tag tag = new Tag();
				tag.setTagId(tagId);
				tags.add(tag);
			}
		}
		news.setTags(tags);
		Long newsId = newsService.addNews(news);

		return newsId;
	}

	public void deleteNews(Long newsId) throws ServiceException {
		newsService.deleteNews(newsId);
		return;
	}

	public News getNewsVO(Long newsId) throws ServiceException {

		News news = newsService.getNews(newsId);
		news.getAuthor().getAuthorId();
		news.getTags().size();
		news.getComments().size();

		return news;
	}

	public NewsFormVO getNewsFormVO(Long newsId) throws ServiceException {

		News news = getNewsVO(newsId);
		NewsFormVO newsFormVO = new NewsFormVO();
		newsFormVO.setNews(news);
		newsFormVO.setAuthorId(news.getAuthor().getAuthorId());
		newsFormVO.setTagIdList(getTagIdList(news.getTags()));

		return newsFormVO;
	}

	public Long updateNewsFormVO(NewsFormVO newsFormVO) throws ServiceException {
		News news = new News();
		news.setVersion(newsFormVO.getNews().getVersion());
		news.setNewsId(newsFormVO.getNews().getNewsId());
		news.setTitle(newsFormVO.getNews().getTitle());
		news.setShortText(newsFormVO.getNews().getShortText());
		news.setFullText(newsFormVO.getNews().getFullText());
		news.setCreationDate(newsFormVO.getNews().getCreationDate());
		news.setModificationDate(new Date(System.currentTimeMillis()));
		news.setAuthor(new Author());
		news.getAuthor().setAuthorId(newsFormVO.getAuthorId());

		List<Tag> tags = new ArrayList<Tag>();
		if (newsFormVO.getTagIdList() != null) {
			for (Long tagId : newsFormVO.getTagIdList()) {
				Tag tag = new Tag();
				tag.setTagId(tagId);
				tags.add(tag);
			}
		}
		news.setTags(tags);

		newsService.updateNews(news);
		return newsFormVO.getNews().getNewsId();
	}
	@Deprecated
	public void updateTags(Long newsId, List<Long> tagList) throws ServiceException {

		tagService.deleteNewsTagByNewsId(newsId);
		if(tagList != null) {
			tagService.addNewsTag(tagList, newsId);
		}
		return;
	}
	@Deprecated
	public void updateAuthor(Long newsId, Long authorId) throws ServiceException {

		authorService.deleteNewsAuthor(newsId);
		authorService.addNewsAuthor(authorId, newsId);
		return;
	}

	public List<News> getListOfNews(SearchCriteria searchCriteria,
			int firstIndex, int lastIndex) throws ServiceException {
		
		List<News> newsList = newsService.getListOfNews(searchCriteria,firstIndex,lastIndex);
		return newsList;
	}

	private List<Long> getTagIdList(List<Tag> tagList) {

		List<Long> tagIdList = new ArrayList<Long>();
		for(Tag tag:tagList) {
			tagIdList.add(tag.getTagId());
		}
		return tagIdList;
	}

}
