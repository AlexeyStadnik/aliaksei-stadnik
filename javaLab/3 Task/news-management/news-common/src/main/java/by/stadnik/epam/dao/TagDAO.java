package by.stadnik.epam.dao;

import java.util.List;

import by.stadnik.epam.dao.impl.TagDAOImpl;
import by.stadnik.epam.entity.Tag;
/**
 * Interface provides methods to work with TAG table
 * @see Tag
 * @see TagDAOImpl - implementation
 * @see TagDAOImplTest - test class for the impelemtation {@link TagDAOImpl}
 * @author Aliaksei_Stadnik
 *
 */
public interface TagDAO {
	/**
	 * 
	 * @param tagId
	 * @throws DAOException
	 */
	public void deleteTag(Long tagId) throws DAOException;
	/**
	 * 
	 * @param tag
	 * @return
	 * @throws DAOException
	 */
	public Long addTag(Tag tag) throws DAOException;
	/**
	 * 
	 * @param tagList
	 * @param newsId
	 * @throws DAOException
	 */
	public void addNewsTag(List<Long> tagList,Long newsId) throws DAOException;
	/**
	 * 
	 * @param newsId
	 * @throws DAOException
	 */
	public void deleteNewsTagByNewsId(Long newsId) throws DAOException;
	/**
	 * 
	 * @param tagId
	 * @throws DAOException
	 */
	public void deleteNewsTagByTagId(Long tagId) throws DAOException;
	/**
	 * 
	 * @param newsId
	 * @return
	 * @throws DAOException
	 */
	public List<Tag> getTag(Long newsId) throws DAOException;
	/**
	 * 
	 * @param tagId
	 * @return
	 * @throws DAOException
	 */
	public Tag getTagByTagId(Long tagId) throws DAOException;
	/**
	 * 
	 * @param tag
	 * @throws DAOException
	 */
	public void updateTag(Tag tag) throws DAOException;
	/**
	 * 
	 * @return
	 * @throws DAOException
	 */
	public List<Tag> getAllTags() throws DAOException;
}
