package by.stadnik.epam.service;

import java.util.List;

import by.stadnik.epam.entity.Author;

/**
 * Interface provides methods to work with Author, News_author tables.
 * @author Aliaksei_Stadnik
 *
 */
public interface AuthorService {
	/**
	 * 
	 * @param author
	 * @return
	 * @throws ServiceException
	 */
	public Long addAuthor(Author author) throws ServiceException;
	/**
	 * 
	 * @param authorId
	 * @throws ServiceException
	 */
	public void deleteAuthor(Long authorId) throws ServiceException;
	/**
	 * 
	 * @param authorId
	 * @param newsId
	 * @throws ServiceException
	 */
	public void addNewsAuthor(Long authorId,Long newsId) throws ServiceException;
	/**
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteNewsAuthor(Long newsId) throws ServiceException;
	/**
	 * 
	 * @param newsId
	 * @return
	 * @throws ServiceException
	 */
	public Author getAuthorByNewsId(Long newsId) throws ServiceException;
	/**
	 * 
	 * @param authorId
	 * @return
	 * @throws ServiceException
	 */
	public Author getAuthorByAuthorId(Long authorId) throws ServiceException;
	/**
	 * 
	 * @param author
	 * @throws ServiceException
	 */
	public void updateAuthor(Author author) throws ServiceException;
	/**
	 * 
	 * @return
	 * @throws ServiceException
	 */
	public List<Author> getAllAuthors() throws ServiceException;
}
