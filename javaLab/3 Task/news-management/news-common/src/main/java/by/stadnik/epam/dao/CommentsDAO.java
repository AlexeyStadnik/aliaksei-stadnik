package by.stadnik.epam.dao;

import java.util.List;

import by.stadnik.epam.dao.impl.CommentsDAOImpl;
import by.stadnik.epam.entity.Comment;
/**
 * Interface includes methods to work with COMMENTS table
 * @see Comment
 * @see CommentsDAOImpl - class implements  CommentsDAO
 * @see CommentsDAOImplTest - test class for CommentsDAO
 * @author Aliaksei_Stadnik
 */
public interface CommentsDAO {
	/**
	 * 
	 * @param comment - {@link Comment} entity object 
	 * @return - id of the comment which has been added
	 * @throws DAOException
	 */
	public Long addComment(Comment comment) throws DAOException;
	/**
	 * @see Comment - {@link Comment} entity object 
	 * @param commentId
	 * @throws DAOException
	 */
	public void deleteCommentByCommentId(Long commentId) throws DAOException;
	/**
	 * 
	 * @param newsId
	 * @throws DAOException
	 */
	public void deleteCommentsByNewsId(Long newsId) throws DAOException;
	/**
	 * 
	 * @param comment - {@link Comment} entity object 
	 * @throws DAOException
	 */
	public void updateComment(Comment comment) throws DAOException;
	/**
	 * 
	 * @param newsId
	 * @return
	 * @throws DAOException
	 */
	public List<Comment> getComments(Long newsId) throws DAOException;
	/**
	 * 
	 * @param commentId
	 * @return
	 * @throws DAOException
	 */
	public Comment getCommentByCommentId(Long commentId) throws DAOException;

}
