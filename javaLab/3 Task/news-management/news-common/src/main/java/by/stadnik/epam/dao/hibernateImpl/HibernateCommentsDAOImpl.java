package by.stadnik.epam.dao.hibernateImpl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import by.stadnik.epam.dao.CommentsDAO;
import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.entity.Comment;
import by.stadnik.epam.entity.News;

@Transactional (rollbackFor = Exception.class)
public class HibernateCommentsDAOImpl implements CommentsDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(HibernateCommentsDAOImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Long addComment(Comment comment) throws DAOException  {
		LOGGER.debug("Adding new comment");
		Session session = sessionFactory.getCurrentSession();
		Long commentId = null;
		try {
			News news = (News) session.load(News.class, comment.getNews().getNewsId());
			comment.setNews(news);
			news.getComments().add(comment);	
			commentId = (Long)session.save(comment);
		} catch(HibernateException e) {
			throw new DAOException("Exception while comment saving process. Comments - [" + comment + "]",e);
		}
		return commentId;
	}

	public void deleteCommentByCommentId(Long commentId) throws DAOException  {
		LOGGER.debug("Deleting comment by commentId");
		Session session = sessionFactory.getCurrentSession();
		try {
			Query query = session.createQuery("DELETE FROM Comment WHERE comment_Id=" + commentId);
			query.executeUpdate();
		} catch(HibernateException e) {
			throw new DAOException("Exception while comment deleting process. Comment id = [" + commentId + "]",e);
		}

	}

	public void deleteCommentsByNewsId(Long newsId) throws DAOException  {
		LOGGER.debug("Deleting existing comments based on news's id");
		Session session = sessionFactory.getCurrentSession();
		try {
			Query query = session.createQuery("DELETE FROM Comment WHERE news_Id=" + newsId);
			query.executeUpdate();
		} catch(HibernateException e) {
			throw new DAOException("Exception while comments deleting process. News id = [" + newsId + "]",e);
		}
	}

	public void updateComment(Comment comment) throws DAOException  {
		LOGGER.debug("Editing existing creditCard");
		Session session = sessionFactory.getCurrentSession();
		Comment oldComment = (Comment) session.get(Comment.class, comment.getCommentId());
		try {
			oldComment.setCommentText(comment.getCommentText());
			oldComment.setCreationDate(comment.getCreationDate());;
			session.save(oldComment);
		} catch(HibernateException e) {
			throw new DAOException("Exception while comment updating process. Comment = [" + comment + "]",e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<Comment> getComments(Long newsId) throws DAOException  {
		LOGGER.debug("Retrieving all comment by newsId");
		Session session = sessionFactory.getCurrentSession();
		Query query = null;
		try {
			query = session.createQuery("FROM Comment WHERE news_Id=" + newsId);
		} catch(HibernateException e) {
			throw new DAOException("Exception while comment retrieving  process. News id = [" + newsId + "]",e);
		}
		return  query.list();
	}

	public Comment getCommentByCommentId(Long commentId) throws DAOException  {
		LOGGER.debug("Retrieving comment by commentId");  
		Session session = sessionFactory.getCurrentSession();
		Comment comment  = null;
		try {
			comment = (Comment) session.get(Comment.class, commentId);
		} catch(HibernateException e) {
			throw new DAOException("Exception while comment retrieving  process. Comment id = [" + commentId + "]",e);
		}
		return comment;
	}
}
