package by.stadnik.epam.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.dao.TagDAO;
import by.stadnik.epam.entity.Tag;
import by.stadnik.epam.util.ConnectionClose;

public class TagDAOImpl  implements TagDAO {

	private DataSource dataSource;

	public static final String DELETE_TAG_SQL = "DELETE FROM tag WHERE tag_id = ?";
	public static final String INSERT_TAG_SQL = "INSERT INTO tag (tag_id,tag_name) VALUES (tag_seq.NEXTVAL,?)";
	public static final String INSERT_NEWS_TAG_SQL = "INSERT INTO news_tag (NEWS_ID,TAG_ID) VALUES (?, ?)";
	public static final String DELETE_NEWS_TAG_BY_NEWS_ID_SQL = "DELETE FROM news_tag WHERE news_id = ?";
	public static final String DELETE_NEWS_TAG_BY_TAG_ID_SQL = "DELETE FROM news_tag WHERE tag_id = ?";
	public static final String SELECT_TAGS_BY_NEWS_ID_SQL = "SELECT tag.tag_id,tag_name FROM tag JOIN news_tag ON tag.tag_id = news_tag.tag_id WHERE news_tag.news_id = ?";
	public static final String UPDATE_TAG_SQL = "UPDATE tag SET tag_name=? WHERE tag_id=?";
	public static final String SELECT_TAG_BY_TAG_ID_SQL ="SELECT tag.tag_id,tag.tag_name FROM tag WHERE tag.tag_id=?";
	public static final String SELECT_ALL_TAGS_SQL = "SELECT tag_id,tag_name FROM tag";

	public static final String PARAM_TAG_ID = "TAG_ID";
	public static final String PARAM_TAG_NAME = "TAG_NAME";

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void deleteTag(Long tagId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource); 
			preparedStatement = connection.prepareStatement(DELETE_TAG_SQL);
			preparedStatement.setLong(1, tagId);
			preparedStatement.executeUpdate();
		} catch(SQLException e) {
			throw new DAOException("Exception while tag deleting",e);
		}finally {
			ConnectionClose.close(connection, preparedStatement,dataSource);
		}

	}

	public Long addTag(Tag tag) throws DAOException {
		Long tagId = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource); 
			preparedStatement = connection.prepareStatement(INSERT_TAG_SQL,new String [] {"TAG_ID"});
			preparedStatement.setNString(1, tag.getTagName());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if(resultSet.next()) {
				tagId = Long.parseLong(resultSet.getString(1));
			}

		} catch (SQLException e) {
			throw new DAOException("Exception while tag adding",e);
		}finally {
			ConnectionClose.close(connection, preparedStatement,dataSource,resultSet);
		}
		return tagId;
	}

	public void addNewsTag(List<Long>tagList, Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource); 
			preparedStatement = connection.prepareStatement(INSERT_NEWS_TAG_SQL);

			for(Long tag:tagList) {
				preparedStatement.setLong(1, newsId);
				preparedStatement.setLong(2, tag);
				preparedStatement.addBatch();
			}

			preparedStatement.executeBatch();

		}catch(SQLException e) {
			throw new DAOException("Exception while news_tag adding process",e);

		} finally {
			ConnectionClose.close(connection,preparedStatement,dataSource);
		}

	}

	public void deleteNewsTagByNewsId(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource); 
			preparedStatement = connection.prepareStatement(DELETE_NEWS_TAG_BY_NEWS_ID_SQL);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();

		}catch(SQLException e) {
			throw new DAOException("Exception while news_author deleting process",e);

		} finally {
			ConnectionClose.close(connection,preparedStatement,dataSource);
		}

	}

	public void deleteNewsTagByTagId(Long tagId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource); 
			preparedStatement = connection.prepareStatement(DELETE_NEWS_TAG_BY_TAG_ID_SQL);
			preparedStatement.setLong(1, tagId);
			preparedStatement.executeUpdate();

		}catch(SQLException e) {
			throw new DAOException("Exception while news_tag deleting process",e);

		} finally {
			ConnectionClose.close(connection,preparedStatement,dataSource);
		}

	}

	public List<Tag> getTag(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Tag> tagList = new ArrayList<Tag>();
		try {
			connection = DataSourceUtils.doGetConnection(dataSource); 
			preparedStatement = connection.prepareStatement(SELECT_TAGS_BY_NEWS_ID_SQL);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				tagList.add(
						new Tag(
								resultSet.getLong(PARAM_TAG_ID),
								resultSet.getNString(PARAM_TAG_NAME)
								)
						);


			}

			return tagList;
		} catch(SQLException e) {
			throw new DAOException("Exception while getting list of tags",e);
		}finally {
			ConnectionClose.close(connection, preparedStatement,dataSource,resultSet);
		}
	}

	public Tag getTagByTagId(Long tagId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Tag tag = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource); 
			preparedStatement = connection.prepareStatement(SELECT_TAG_BY_TAG_ID_SQL);
			preparedStatement.setLong(1, tagId);
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				tag = new Tag(
						resultSet.getLong("tag_id"),
						resultSet.getNString("tag_name")
						);
			}
			return tag;
		}catch(SQLException e) {
			throw new DAOException("Exception while tag getting",e);
		}finally {
			ConnectionClose.close(connection, preparedStatement,dataSource,resultSet);
		}
	}

	public void updateTag(Tag tag) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource); 
			preparedStatement = connection.prepareStatement(UPDATE_TAG_SQL);
			preparedStatement.setString(1, tag.getTagName());
			preparedStatement.setLong(2, tag.getTagId());
			preparedStatement.executeUpdate();
		} catch(SQLException e) {
			throw new DAOException("Exception while tags deleting",e);
		}finally {
			ConnectionClose.close(connection, preparedStatement,dataSource);
		}
	}

	public List<Tag> getAllTags() throws DAOException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		List<Tag> tagList = new ArrayList<Tag>();
		try {
			connection = DataSourceUtils.doGetConnection(dataSource); 
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SELECT_ALL_TAGS_SQL);
			while(resultSet.next()) {
				tagList.add( new Tag(
						resultSet.getLong("tag_id"),
						resultSet.getNString("tag_name")
						));
			}
			return tagList;
		}catch(SQLException e) {
			throw new DAOException("Exception while tag getting",e);
		}finally {
			ConnectionClose.close(connection, statement,dataSource,resultSet);
		}
	}
}
