package by.stadnik.epam.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.Version;

import org.eclipse.persistence.annotations.BatchFetch;
import org.eclipse.persistence.annotations.BatchFetchType;
import org.hibernate.annotations.BatchSize;

@Entity
@Cacheable(false)
@Table(name = "NEWS")
public class News implements Serializable {

	private static final long serialVersionUID = 5924361831551833717L;

	@Id
	@Column(name = "NEWS_ID")
	@SequenceGenerator(name = "newsSeq", sequenceName = "news_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "newsSeq")
	private Long newsId;

	@Column(name = "TITLE")
	private String title;

	@Column(name = "SHORT_TEXT")
	private String shortText;

	@Column(name = "FULL_TEXT")
	private String fullText;

	@Column(name = "CREATION_DATE")
	private Timestamp creationDate;

	@Column(name = "MODIFICATION_DATE")
	private Date modificationDate;
	
	@Version
	@Column(name = "VERSION")
	private Long version;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinTable(name = "News_Author", joinColumns = { 
			@JoinColumn(columnDefinition="integer",name = "NEWS_ID") }, 
			inverseJoinColumns = { @JoinColumn(columnDefinition="integer",name = "AUTHOR_ID") })
	@BatchFetch( value = BatchFetchType.JOIN)
	private Author author;

	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name = "News_Tag", joinColumns = { 
			@JoinColumn(columnDefinition="integer",name = "NEWS_ID") }, 
			inverseJoinColumns = { @JoinColumn(columnDefinition="integer",name = "TAG_ID") })
	@BatchFetch( value = BatchFetchType.IN)
	@BatchSize(size=100)
	private List<Tag> tags;
	
	@OneToMany(cascade=CascadeType.REMOVE,fetch = FetchType.LAZY, mappedBy = "news")
	@BatchFetch( value = BatchFetchType.IN)
	@BatchSize(size=100)
	@OrderBy("creationDate DESC")
	private List<Comment> comments;
	
	
	@OneToOne
	@JoinColumn(name="NEWS_ID",referencedColumnName="VIEW_NEWS_ID", insertable = false, updatable = false)
	@BatchFetch( value = BatchFetchType.JOIN)
	private CommentCountView commentCountView;
	
	public void setCommentCountView(CommentCountView commentCountView) {
		this.commentCountView = commentCountView;
	}

	public CommentCountView getCommentCountView() {
		return commentCountView;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public List<Comment> getComments() {
		return comments;
	}
    
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public News() {

	}
	
	public News(Long newsId, String title, String shortText, String fullText,
			Timestamp creationDate, Date modificationDate) {
		super();
		this.newsId = newsId;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	public News(Long newsId, String title, String shortText, String fullText,
			Timestamp creationDate, Date modificationDate, Long version) {
		super();
		this.newsId = newsId;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
		this.version = version;
	}

	public News(Long newsId, String title, String shortText, String fullText,
			Timestamp creationDate, Date modificationDate, Long version,
			Author author) {
		super();
		this.newsId = newsId;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
		this.version = version;
		this.author = author;
	}

	public News(Long newsId, String title, String shortText, String fullText,
			Timestamp creationDate, Date modificationDate, Long version,
			Author author, List<Tag> tags) {
		super();
		this.newsId = newsId;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
		this.version = version;
		this.author = author;
		this.tags = tags;
	}

	public News(Long newsId, String title, String shortText, String fullText,
			Timestamp creationDate, Date modificationDate, Long version,
			Author author, List<Tag> tags, List<Comment> comments) {
		super();
		this.newsId = newsId;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
		this.version = version;
		this.author = author;
		this.tags = tags;
		this.comments = comments;
	}

	public News(Long newsId, String title, String shortText, String fullText,
			Timestamp creationDate, Date modificationDate, Long version,
			Author author, List<Tag> tags, List<Comment> comments,
			CommentCountView commentCountView) {
		this.newsId = newsId;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
		this.version = version;
		this.author = author;
		this.tags = tags;
		this.comments = comments;
		this.commentCountView = commentCountView;
	}
	
	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((fullText == null) ? 0 : fullText.hashCode());
		result = prime
				* result
				+ ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		result = prime * result
				+ ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "News [newsId=" + newsId + ", title=" + title + ", shortText="
				+ shortText + ", fullText=" + fullText + ", creationDate="
				+ creationDate + ", modificationDate=" + modificationDate
				+ ", author=" + author + ", tags=" + tags + "]";
	}
}