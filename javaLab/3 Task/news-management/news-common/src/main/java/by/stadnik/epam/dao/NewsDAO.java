package by.stadnik.epam.dao;


import java.util.List;

import by.stadnik.epam.dao.impl.NewsDAOImpl;
import by.stadnik.epam.entity.News;
import by.stadnik.epam.util.SearchCriteria;
/**
 * Interface to work with NEWS table
 * Provides basic CRUD methods and filter methods.
 * @see News  - {@link News} entity
 * @see NewsDAOImpl - implementation
 * @see NewsDAOImplTest - test class for implementation
 * @author Aliaksei_Stadnik
 *
 */
public interface NewsDAO {
	/**
	 * 
	 * @param news  - {@link News} entity
	 * @return
	 * @throws DAOException
	 */
	public Long addNews(News news) throws DAOException;
	/**
	 * 
	 * @param newsId
	 * @throws DAOException
	 */
	public void deleteNews(Long newsId) throws DAOException;
	/**
	 * 
	 * @param newsId
	 * @return
	 * @throws DAOException
	 */
	public News getNews(Long newsId) throws DAOException;
	/**
	 * 
	 * @param news  - {@link News} entity
	 * @throws DAOException
	 */
	public void updateNews(News news) throws DAOException;
	/**
	 * Method for getting number of news by filter
	 * It's possible to filter news by tags and/or author
	 * If it's necessary to get list of all news one can use empty searchCriteria object
	 * @param searchCriteria - {@link SearchCriteria}  - filter class
	 * @return - List of {@link News} according filter
	 * @throws DAOException
	 */
	public int getNumberOfNews(SearchCriteria searchCriteria) throws DAOException;
	/**
	 * Method for getting news by filter
	 * It's possible to filter news by tags and/or author
	 * If it's necessary to get list of all news one can use empty searchCriteria object
	 * @param searchCriteria - {@link SearchCriteria}  - filter class
	 * @return - List of {@link News} according filter
	 * @throws DAOException
	 */
	public List<News> getListOfNews(SearchCriteria searchCriteria,int firstIndex,int lastIndex) throws DAOException;
}
