package by.stadnik.epam.service;

import java.util.List;

import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.entity.News;
import by.stadnik.epam.util.SearchCriteria;
/**
 * Interface provides methods to work with NEWS table
 * @see News
 * @author Aliaksei_Stadnik
 *
 */
public interface NewsService {

	public Long addNews(News news) throws ServiceException;
	public void deleteNews(Long newsId) throws ServiceException;
	public News getNews(Long newsId) throws ServiceException;
	public void updateNews(News news) throws ServiceException; 
	/**
	 * Method for getting number of news by filter
	 * It's possible to filter news by tags and/or author
	 * If it's necessary to get list of all news one can use empty searchCriteria object
	 * @param searchCriteria - {@link SearchCriteria}  - filter class
	 * @return - List of {@link News} according filter
	 * @throws DAOException
	 */
	public int getNumberOfNews(SearchCriteria searchCriteria) throws ServiceException;
	/**
	 * Method for getting news by filter
	 * It's possible to filter news by tags and/or author
	 * If it's necessary to get list of all news one can use empty searchCriteria object
	 * @param searchCriteria - {@link SearchCriteria}  - filter class
	 * @return - List of {@link News} according filter
	 * @throws DAOException
	 */
	public List<News>getListOfNews(SearchCriteria searchCriteria,int firstIndex,int lastIndex) throws ServiceException;
}
