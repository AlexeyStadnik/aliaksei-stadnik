package by.stadnik.epam.service;

import java.util.List;

import by.stadnik.epam.entity.Comment;
/**
 * Interface provides methods to work with COMMENT table.
 * @see Comment
 * @author Aliaksei_Stadnik
 *
 */
public interface CommentsService {
	/**
	 * 
	 * @param comment
	 * @return
	 * @throws ServiceException
	 */
	public Long addComment(Comment comment) throws ServiceException;
	/**
	 * 
	 * @param commentId
	 * @throws ServiceException
	 */
	public void deleteCommentByCommentId(Long commentId) throws ServiceException;
	/**
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteCommentsByNewsId(Long newsId) throws ServiceException;
	/**
	 * 
	 * @param comment
	 * @throws ServiceException
	 */
	public void updateComment(Comment comment) throws ServiceException;
	/**
	 * 
	 * @param newsId
	 * @return
	 * @throws ServiceException
	 */
	public List<Comment> getComments(Long newsId) throws ServiceException;
	/**
	 * 
	 * @param commentId
	 * @return
	 * @throws ServiceException
	 */
	public Comment getCommentByCommentId(Long commentId) throws ServiceException;
}
