package by.stadnik.epam.dao.hibernateImpl;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import by.stadnik.epam.dao.AuthorDAO;
import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.entity.Author;
import by.stadnik.epam.entity.News;

@Transactional (rollbackFor = Exception.class)
public class HibernateAuthorDAOImpl implements AuthorDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(HibernateAuthorDAOImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Long addAuthor(Author author) throws DAOException  {
		LOGGER.debug("Adding new author");
		Session session = sessionFactory.getCurrentSession();
		try {
			session.save(author);
		}catch(HibernateException e) {
			throw new DAOException("Exception while author saving process. Author - [" + author + "]",e);
		}
		return author.getAuthorId();
	}

	public void deleteAuthor(Long authorId) throws DAOException  {
		LOGGER.debug("Deleting existing author");
		Session session = sessionFactory.getCurrentSession();
		try {
			Author author = (Author) session.get(Author.class, authorId);
			author.setExpired(new Timestamp(System.currentTimeMillis()));
			session.update(author);
		}catch(HibernateException e) {
			throw new DAOException("Exception while author deleting process. Author id = " + authorId,e);
		}
	}

	public void addNewsAuthor(Long authorId, Long newsId) throws DAOException  {
		LOGGER.debug("Adding news author");
		Session session = sessionFactory.getCurrentSession();
		try {
			News news = (News) session.get(News.class, newsId);
			Author author = (Author) session.get(Author.class, authorId);
			news.setAuthor(author);
			session.save(news);
		}catch(HibernateException e) {
			throw new DAOException("Exception while adding news author process. Author id = " + authorId +
					" . News id = " + newsId,e);
		}
	}

	public void deleteNewsAuthor(Long newsId) throws DAOException  {
		LOGGER.debug("Delete news author");
		Session session = sessionFactory.getCurrentSession();
		try {
			Query query = session.createQuery("FROM News as a LEFT JOIN FETCH  a.author WHERE a.newsId="+ newsId);
			News news =  (News) query.uniqueResult();
			news.setAuthor(null);
			session.update(news);
		}catch(HibernateException e) {
			throw new DAOException("Exception while deleting news author process. News id = " + newsId,e);
		}
	}

	public Author getAuthorByNewsId(Long newsId) throws DAOException  {
		LOGGER.debug("Getting author by new id");
		Session session = sessionFactory.getCurrentSession();
		News news = null;
		try {
			Query query = session.createQuery("FROM News as a LEFT JOIN FETCH  a.author WHERE a.newsId="+ newsId);
			news =  (News) query.uniqueResult();
		}catch(HibernateException e) {
			throw new DAOException("Exception while author retrieving process. News id = " + newsId,e);
		}
		Author author = news.getAuthor();
		return author;
	}

	public Author getAuthorByAuthorId(Long authorId) throws DAOException  {
		LOGGER.debug("Retrieving author by author id");  
		Session session = sessionFactory.getCurrentSession();
		Author author = null;
		try {
			author = (Author) session.get(Author.class, authorId);
		}catch(HibernateException e) {
			throw new DAOException("Exception while author retrieving process. Author id = " + authorId,e);
		}
		return author;
	}

	public void updateAuthor(Author author) throws DAOException  {
		LOGGER.debug("Updating author");
		Session session = sessionFactory.getCurrentSession();
		try {
			session.update(author);
		}catch(HibernateException e) {
			throw new DAOException("Exception while updating author process. Author - [" + author + "]",e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<Author> getAllAuthors() throws DAOException  {
		LOGGER.debug("Retrieving all authors");
		Session session = sessionFactory.getCurrentSession();
		Query query = null;
		try {
			query = session.createQuery("FROM  Author");
		}catch(HibernateException e) {
			throw new DAOException("Exception while retrieving all authors process",e);
		}
		return  query.list();
	}
}
