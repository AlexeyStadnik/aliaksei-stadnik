package by.stadnik.epam.service;

import java.util.List;

import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.entity.Author;
import by.stadnik.epam.entity.News;
import by.stadnik.epam.entity.NewsFormVO;
import by.stadnik.epam.entity.Tag;
import by.stadnik.epam.service.impl.ServiceManagerImpl;
import by.stadnik.epam.util.SearchCriteria;

/**
 * Interface provides complicated methods which required multiple usages of service methods and transactions
 * @see ServiceManagerImpl - implemenation
 * @see ServiceManagerImplTest - test class
 * @author Aliaksei_Stadnik
 *
 */
public interface ServiceManager {
	/**
	 * Method to add NewsFornVO entity.
	 * NewsFormVO entity consist of <ul>
	 * <li> {@link News} - entity</li>
	 * <li> {@link Author}  - consists of author id </li>
	 * <li> {@link Tag} - list of ids of entities</li>
	 * </ul>
	 * @param newsVO
	 * @return
	 * @throws ServiceException
	 */
	Long addNews(NewsFormVO newsVO) throws ServiceException;
	void deleteNews(Long newsId) throws ServiceException;
	News getNewsVO(Long newsId) throws ServiceException;
	NewsFormVO getNewsFormVO(Long newsId) throws ServiceException;
	void updateTags(Long newsId,List<Long> tagList) throws ServiceException; 
	void updateAuthor(Long newsId,Long authorId) throws ServiceException;
	/**
	 * Method for getting news by filter
	 * It's possible to filter news by tags and/or author
	 * If it's necessary to get list of all news one can use empty searchCriteria object
	 * @param searchCriteria - {@link SearchCriteria}  - filter class
	 * @return - List of {@link News} according filter
	 * @throws DAOException
	 */
	List<News>getListOfNews(SearchCriteria searchCriteria,int firstIndex,int lastIndex) throws ServiceException;
	Long updateNewsFormVO(NewsFormVO newsFormVO) throws ServiceException ;

}
