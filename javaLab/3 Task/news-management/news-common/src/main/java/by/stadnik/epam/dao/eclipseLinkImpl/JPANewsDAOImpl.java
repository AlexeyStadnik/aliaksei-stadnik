package by.stadnik.epam.dao.eclipseLinkImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.dao.DAOOptimisticLockException;
import by.stadnik.epam.dao.NewsDAO;
import by.stadnik.epam.entity.Author;
import by.stadnik.epam.entity.Comment;
import by.stadnik.epam.entity.News;
import by.stadnik.epam.entity.Tag;
import by.stadnik.epam.util.SearchCriteria;

@Transactional (rollbackFor = Exception.class)
public class JPANewsDAOImpl implements NewsDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(JPANewsDAOImpl.class);

	@PersistenceContext
	EntityManager entityManager;

	public Long addNews(News news) throws DAOException {
		LOGGER.debug("Adding new news");
		try {
			entityManager.persist(news);
		} catch(PersistenceException e) {
			throw new DAOException("Exception while news saving process. News - [" + news + "]",e);
		}
		return news.getNewsId();
	}

	public void deleteNews(Long newsId) throws DAOException {
		LOGGER.debug("Deleting existing news");
		try {
			entityManager.remove(entityManager.find(News.class,newsId));
		} catch(PersistenceException e) {
			throw new DAOException("Exception while news deleting process. News id = [" + newsId + "]",e);
		}
	}

	public News getNews(Long newsId) throws DAOException {
		LOGGER.debug("Getting existing news");
		try {
			return entityManager.find(News.class, newsId);
		} catch(PersistenceException e) {
			throw new DAOException("Exception while news getting process. News id = [" + newsId + "]",e);
		}
	}

	public void updateNews(News news) throws DAOException {
		LOGGER.debug("Editing existing news");
		try {
			entityManager.merge(news);
		}catch(javax.persistence.OptimisticLockException e) {
			throw new DAOOptimisticLockException("Locking exception. News - [" + news + "]",e);
		}catch(PersistenceException e) {
			throw new DAOException("Exception while news updationg process. News = [" + news + "]",e);
		}
	}

	public int getNumberOfNews(SearchCriteria searchCriteria)
			throws DAOException {
		LOGGER.debug("Building criteria to retriew number of news");
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<News> criteria = criteriaBuilder.createQuery( News.class );
			Root<News> newsRoot = criteria.from( News.class );
			criteria = buildCriteria(criteriaBuilder, criteria, newsRoot, searchCriteria);
			criteria.distinct(true);
			return entityManager.createQuery(criteria).getResultList().size();
		} catch(PersistenceException e) {
			throw new DAOException("Exception while buildingsearch criteria proces. SearchCriteria - [" + searchCriteria + "]",e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<News> getListOfNews(SearchCriteria searchCriteria,
			int firstIndex, int lastIndex) throws DAOException {
		LOGGER.debug("Building criteria to retriew list of news");
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<News> criteria = criteriaBuilder.createQuery( News.class );
			Root<News> newsRoot = criteria.from( News.class );
			criteria = buildCriteria(criteriaBuilder, criteria, newsRoot, searchCriteria);
			criteria.orderBy(criteriaBuilder.desc(newsRoot.get("commentCountView").get("commentCount")), criteriaBuilder.desc(newsRoot.get("modificationDate")));
			criteria.distinct(true);
			Query query = entityManager.createQuery(criteria);
			query.setFirstResult(firstIndex);
			query.setMaxResults(lastIndex - firstIndex);

			return query.getResultList();
		} catch(PersistenceException e) {
			throw new DAOException("Exception while buildingsearch criteria proces. SearchCriteria - [" + searchCriteria + "]",e);
		}
	}
	/**
	 * Method for building criteria query for filter news entities.
	 * Joins lazy collections of {@link News} entity consists of {@link Author}, {@link Comment}, {@link Tag}.
	 * 
	 * @param criteriaBuilder - {@link CriteriaBuilder} to ad and/equal predicate.
	 * @param criteria - main {@link CriteriaQuery} with all filters and orders
	 * @param newsRoot - {@link Root} to join author and tags.
	 * @param searchCriteria  - {@link SearchCriteria} includes all parameters to filter by.
	 * @return  {@link CriteriaQuery} with all required filters.
	 */
	private CriteriaQuery<News> buildCriteria(CriteriaBuilder criteriaBuilder ,CriteriaQuery<News> criteria,Root<News> newsRoot,SearchCriteria searchCriteria) {
		Predicate predicate = criteriaBuilder.disjunction();
		if(searchCriteria.getAuthorId() != null) {
			Join<News, Author> authors = newsRoot.join("author",JoinType.LEFT); 
			predicate = criteriaBuilder.equal(authors.get("authorId"),searchCriteria.getAuthorId());
			criteria.where(predicate); 
		}
		if(searchCriteria.getTagIdList() != null && !searchCriteria.getTagIdList().isEmpty()) {
			Join<News, Tag> tags = newsRoot.join("tags",JoinType.INNER); 
			predicate = criteriaBuilder.and(predicate,tags.get("tagId").in(searchCriteria.getTagIdList() ));
			criteria.where(predicate); 
		}
		return criteria;
	}

}


