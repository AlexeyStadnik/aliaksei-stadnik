package by.stadnik.epam.dao.eclipseLinkImpl;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import by.stadnik.epam.dao.AuthorDAO;
import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.entity.Author;
import by.stadnik.epam.entity.News;

@Transactional (rollbackFor = Exception.class)
public class JPAAuthorDAOImpl implements AuthorDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(JPAAuthorDAOImpl.class);

	@PersistenceContext
	EntityManager entityManager;

	public Long addAuthor(Author author) throws DAOException {
		LOGGER.debug("Adding new author");
		try {
			entityManager.persist(author);
		} catch (PersistenceException e) {  
			throw new DAOException("Exception while author saving process. Author - [" + author + "]",e);
		}
		return author.getAuthorId();
	}

	public void deleteAuthor(Long authorId) throws DAOException {
		LOGGER.debug("Deleting existing author");
		try {
			Author author = entityManager.find(Author.class, authorId);
			author.setExpired(new Timestamp(System.currentTimeMillis()));
			entityManager.merge(author);
		} catch (PersistenceException e) {  
			throw new DAOException("Exception while author deleting process. Author id = ",e);
		}
	}

	public void addNewsAuthor(Long authorId, Long newsId) throws DAOException {
		LOGGER.debug("Adding news author");
		try {
			Author author = entityManager.find(Author.class, authorId);
			News news = entityManager.find(News.class, newsId);
			news.setAuthor(author);
		}catch(PersistenceException e) {
			throw new DAOException("Exception while adding news author process. Author id = " + authorId +
					" . News id = " + newsId,e);
		}
	}

	public void deleteNewsAuthor(Long newsId) throws DAOException {
		throw new UnsupportedOperationException("This operation is not supported by JPA implementation of AuthorDAO");
	}

	public Author getAuthorByNewsId(Long newsId) throws DAOException {
		LOGGER.debug("Getting author by new id");
		News news = null;
		try {
			news = entityManager.find(News.class, newsId);
		}catch(PersistenceException e) {
			throw new DAOException("Exception while author retrieving process. News id = " + newsId,e);
		}
		return news.getAuthor();
	}

	public Author getAuthorByAuthorId(Long authorId) throws DAOException {
		LOGGER.debug("Retrieving author by author id"); 
		try {
			return entityManager.find(Author.class, authorId);
		}catch(PersistenceException e) {
			throw new DAOException("Exception while author retrieving process. Author id = " + authorId,e);
		}
	}

	public void updateAuthor(Author author) throws DAOException {
		LOGGER.debug("Updating author");
		try {
			entityManager.merge(author);
		}catch(PersistenceException e) {
			throw new DAOException("Exception while updating author process. Author - [" + author + "]",e);
		}
	}

	public List<Author> getAllAuthors() throws DAOException {
		LOGGER.debug("Retrieving all authors");
		List<Author> authorList =null;
		try {
			authorList = entityManager.createQuery( "from Author a", Author.class ).getResultList();
		}catch(PersistenceException e) {
			throw new DAOException("Exception while retrieving all authors process",e);
		}
		return authorList;
	}
}
