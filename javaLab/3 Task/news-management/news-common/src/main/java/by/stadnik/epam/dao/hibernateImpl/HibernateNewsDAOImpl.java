package by.stadnik.epam.dao.hibernateImpl;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StaleObjectStateException;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateOptimisticLockingFailureException;
import org.springframework.transaction.annotation.Transactional;

import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.dao.DAOOptimisticLockException;
import by.stadnik.epam.dao.NewsDAO;
import by.stadnik.epam.entity.News;
import by.stadnik.epam.util.SearchCriteria;

@Transactional (rollbackFor = Exception.class)
public class HibernateNewsDAOImpl implements NewsDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(HibernateNewsDAOImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Long addNews(News news) throws DAOException  {
		LOGGER.debug("Adding new news");
		Session session = sessionFactory.getCurrentSession();
		try {
			session.save(news);
		} catch(HibernateException e) {
			throw new DAOException("Exception while news saving process. News - [" + news + "]",e);
		}
		return news.getNewsId();
	}

	public void deleteNews(Long newsId) throws DAOException {
		LOGGER.debug("Deleting existing news");
		Session session = sessionFactory.getCurrentSession();
		try {
			News news = (News) session.load(News.class, newsId);
			session.delete(news);
		} catch(HibernateException e) {
			throw new DAOException("Exception while news deleting process. News id = [" + newsId + "]",e);
		}

	}

	public News getNews(Long newsId) throws DAOException {
		LOGGER.debug("Getting existing news");
		Session session = sessionFactory.getCurrentSession();
		News news  = null;
		try {
			news = (News) session.get(News.class, newsId);
		} catch(HibernateException e) {
			throw new DAOException("Exception while news deleting process. News id = [" + newsId + "]",e);
		}
		return news;
	}

	public void updateNews(News news) throws DAOException {
		LOGGER.debug("Editing existing news");
		Session session = sessionFactory.getCurrentSession();
		try {
			session.merge(news);
		}catch(StaleObjectStateException e) {
			throw new DAOOptimisticLockException("Locking exception. News - [" + news + "]",e);
		} catch(HibernateException e) {
			throw new DAOException("Exception while news updating process. News  [" + news + "]",e);
		}
	}

	@SuppressWarnings("unchecked")
	public int getNumberOfNews(SearchCriteria searchCriteria)
			throws DAOException {
		LOGGER.debug("Building criteria to retriew number of news");
		try {
			Criteria criteria = buildNewsCriteria(searchCriteria);
			Collection<News> result = new LinkedHashSet<News>(criteria.list());
			return result.size();
		} catch(HibernateException e) {
			throw new DAOException("Exception while buildingsearch criteria proces. SearchCriteria - [" + searchCriteria + "]",e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<News> getListOfNews(SearchCriteria searchCriteria,
			int firstIndex, int lastIndex) throws DAOException {
		LOGGER.debug("Building criteria to retriew list of news");
		try {
			Criteria criteria = buildNewsCriteria(searchCriteria);
			criteria = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
					.createAlias("commentCountView", "comCount");		
			criteria = criteria.addOrder(Order.desc("comCount.commentCount"))
					.addOrder(Order.desc("modificationDate"))
					.addOrder(Order.desc("newsId"));
			criteria = criteria.setFirstResult(firstIndex)
					.setMaxResults(lastIndex - firstIndex);
			return criteria.list();
		} catch(HibernateException e) {
			throw new DAOException("Exception while buildingsearch criteria proces. SearchCriteria - [" + searchCriteria + "]",e);
		}
	}
	/**
	 * Method for building criteria with filters for news entities.
	 * 
	 * @param searchCriteria  - {@link SearchCriteria} includes all parameters to filter news by. 
	 * @return {@link Criteria} object includes all parameters to filter by.
	 */
	private Criteria buildNewsCriteria(SearchCriteria searchCriteria) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(News.class)
				.setFetchMode("tags",FetchMode.SELECT)
				.setFetchMode("author",FetchMode.JOIN);
		if(searchCriteria.getAuthorId() != null) {
			criteria = criteria.add(
					Restrictions.like("author.authorId",searchCriteria.getAuthorId())
					);
		}
		if(searchCriteria.getTagIdList() != null && !searchCriteria.getTagIdList().isEmpty()) {
			Conjunction conjunction = new Conjunction();
			for (Long tagId : searchCriteria.getTagIdList()) {
				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(News.class)
						.setProjection(Projections.id());
				detachedCriteria.createAlias("tags", "t" + tagId);
				detachedCriteria.add(Restrictions.eq("t" + tagId + ".id", tagId));
				conjunction.add(Property.forName("id").in(detachedCriteria));
			}
			criteria.add(conjunction);
		}
		return criteria;	
	}
}


