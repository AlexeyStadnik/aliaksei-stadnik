package by.stadnik.epam.dao.hibernateImpl;

import java.sql.Timestamp;
import java.util.List;

import org.dbunit.DBTestCase;
import org.dbunit.IDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.stadnik.epam.dao.CommentsDAO;
import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.entity.Comment;
import by.stadnik.epam.entity.News;

@ContextConfiguration(locations = {"classpath:/applicationContextTest.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("HIBERNATE")
public class HibernateCommentsDAOImplTest extends DBTestCase{

	private static final Logger LOGGER = LoggerFactory.getLogger(HibernateCommentsDAOImplTest.class);

	@Autowired
	private  IDatabaseTester databaseTester;
	@Autowired
	private CommentsDAO commentsDAO;

	@Override
	protected IDataSet getDataSet() throws Exception {
		IDataSet dataSet = new FlatXmlDataSetBuilder().build(this.getClass().getResourceAsStream(
				"/script/commentsDAO-test.xml"
				));
		return dataSet;
	}

	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE_ALL;
	}

	@Before
	public void init() throws Exception  {
		databaseTester.setTearDownOperation(DatabaseOperation.DELETE_ALL);
		databaseTester.setDataSet(getDataSet());
		databaseTester.onSetup();
	}

	@After
	public void onTearDown() throws Exception {
		databaseTester.onTearDown();
	}
	
	@Test
	public void getCommentByCommentIdTest() throws DAOException {
		Long commentId = 10L;
		Comment comment = commentsDAO.getCommentByCommentId(commentId);
		Assert.assertNotNull(comment.getCommentId());
	}
	@SuppressWarnings("serial")
	@Test
	public void addCommentTest() throws DAOException {
		Comment expectedComment = new Comment();
		expectedComment.setNews(new News() {{
			setNewsId(10L);
		}});
		expectedComment.setCommentText("Test comment");
		expectedComment.setCreationDate(new Timestamp(1L));
		Long actualCommentId = commentsDAO.addComment(expectedComment);
		Comment actualComment = commentsDAO.getCommentByCommentId(actualCommentId);
		assertCommentsEquals(expectedComment,actualComment);
	}
	@Test
	public void deleteCommentByCommentIdTest() throws DAOException {
		Long commentId = 10L;
		commentsDAO.deleteCommentByCommentId(commentId);
		Comment comment = commentsDAO.getCommentByCommentId(commentId);
		Assert.assertNull(comment);
	}
	@Test
	public void deleteCommentByNewsIdTest() throws DAOException {
		Long newsId = 10L;
		commentsDAO.deleteCommentsByNewsId(newsId);
		List<Comment> commentList = commentsDAO.getComments(newsId);
		Assert.assertEquals(commentList.size(), 0);

	}
	@SuppressWarnings("serial")
	@Test
	public void updateCommentTest() throws DAOException {
		Comment expectedComment = new Comment (
				10L,
				new News() {{
					setNewsId(10L);
				}},
				"New updated text",
				new Timestamp(2L)
				);
		commentsDAO.updateComment(expectedComment);
		Comment actualComment = commentsDAO.getCommentByCommentId(10L);
		Assert.assertEquals(expectedComment.getCommentText(), actualComment.getCommentText());

	}
	@Test
	public void getCommentsTest() throws DAOException {
		Long newsId = 10L;
		List<Comment> commentList = commentsDAO.getComments(newsId);
		Assert.assertNotEquals(commentList.size(), 0);

	}
	private static void assertCommentsEquals(Comment expectedComment,Comment actualComment) {
		Assert.assertEquals(expectedComment.getCommentText(), actualComment.getCommentText());
		Assert.assertEquals(expectedComment.getCreationDate(), actualComment.getCreationDate());
	}
}
