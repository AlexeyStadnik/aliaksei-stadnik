<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<fmt:setLocale value="${Locale}" scope="session"/>
<fmt:bundle basename="resources.pagecontent">
    <html>
    <head>
        <title><fmt:message key="title"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <script type="text/javascript" src="../jquery/js/jquery.js"></script>
        <script type="text/javascript" src="../jquery/js/jquery-ui.js"></script>
        <script type="text/javascript" src="../jquery/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../js/script.js"></script>
        <link type="text/css" rel="stylesheet" href="../jquery/style/jquery-ui.css"/>
        <link rel="stylesheet" type="text/css" href="../css/styles.css"/>
        <link rel="stylesheet" type="text/css" href="../css/table_style.css"/>

    </head>
    <style>
    </style>
    <body>

    <div id="header">
        <h1></h1>
    </div>
    <div id="wrapper">

        <div id="leftcolumn" class="ui-corner-left" style="width: 98%">
            <div id="largeForm" class="loginclass generalForm ui-corner-all ui-helper-reset accordion"
                 style="display:block;width: 450px;margin: 0 auto">
                <h2 class="first current ui-corner-top ui-helper-reset">
                    <span class=""></span>
                    <fmt:message key="form.updateUserInfo"/>
                </h2>

                <div class="pane generalPane ui-helper-reset">
                    <form name="registrationForm" class="updateForm loginForm pane" method="POST" action="controller">
                        <input type="hidden" name="command" value="updateFlightConfirmation"/>
                        <fmt:message key="updateForm.ticketPrice"/>:<br/>
                        <input type="number" step="any" name="ticketPrice" value="${selectedFlight.flightPrice}" required/>
                        <br/><fmt:message key="updateForm.luggagePrice"/>:<br/>
                        <input type="number" step="any" name="luggagePrice" value="${selectedFlight.luggagePrice}" required/>
                        <br/><fmt:message key="updateForm.premiumPrice"/>:<br/>
                        <input type="number" step="any" name="premiumPrice" value="${selectedFlight.premiumPrice}" required/>
                        <br/>

                        <p class="errorMessage">
                                ${updateErrorMessage}
                                ${successfulUpdate}
                        </p>
                        <br/>
                        <input type="submit" class="ui-button ui-button-text-icons  ui-button-text ui-corner-all"
                               value="<fmt:message key="button.update"/>"/>
                    </form>

                    <form name="registrationForm" class="loginForm pane" method="POST" action="controller">
                        <input type="hidden" name="command" value="deleteFlight"/>

                        <p class="errorMessage">
                                ${deleteErrorMessage}
                                ${deleteSuccessfulMessage}
                        </p>
                        <br/>
                        <input type="submit" id="deleteButton" class="ui-button ui-button-text-icons  ui-button-text ui-corner-all"
                               value="<fmt:message key="button.delete"/>"/>
                    </form>

                    <form name="registrationForm" class="loginForm pane" method="POST" action="controller">
                        <input type="hidden" name="command" value="start"/>

                        <br/>
                        <input type="submit" class="ui-button ui-button-text-icons  ui-button-text ui-corner-all"
                               value="<fmt:message key="button.back"/>"/>
                    </form>

                </div>
            </div>

        </div>


        <div id="botton">

        </div>
    </div>
    <script>

        atr = "${deleteConfirmation}";
        if (atr == "true") {
            $('.updateForm').fadeOut(10);
            submitButton = document.getElementById('deleteButton');
            submitButton.disabled = true;

        }




    </script>


    </body>
    </html>
</fmt:bundle>