<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<fmt:setLocale value="${Locale}" scope="session"/>
<fmt:bundle basename="resources.pagecontent">
    <html>
    <head>
        <title><fmt:message key="title"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <script type="text/javascript" src="../jquery/js/jquery.js"></script>
        <script type="text/javascript" src="../jquery/js/jquery-ui.js"></script>
        <script type="text/javascript" src="../jquery/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../js/script.js"></script>
        <link type="text/css" rel="stylesheet" href="../jquery/style/jquery-ui.css"/>
        <link rel="stylesheet" type="text/css" href="../css/styles.css"/>
        <link rel="stylesheet" type="text/css" href="../css/table_style.css"/>

    </head>
    <style>
    </style>
    <body>

    <div id="header">
        <h1></h1>
    </div>
    <div id="wrapper">

        <div id="leftcolumn" class="ui-corner-left" style="width: 98%">
            <form id="flightPurchaseForm" name="buyingForm" class="loginForm pane">


                <table class="simple-little-table" cellspacing='0' style="margin: 0 auto">
                    <tr>
                        <th><fmt:message key="purchase.name"/></th>
                        <th><fmt:message key="purchase.addLuggage"/></th>
                        <th><fmt:message key="purchase.premiumStatus"/></th>
                    </tr>

                    <c:forEach begin="1" end="${clientTicketAmount}" varStatus="loop">
                        <tr>
                            <td><input pattern=".{3,20}" type="text" name="clientName${loop.index}" value="" required/></td>
                            <td><input type="checkbox" name="additionalLuggage${loop.index}"
                                       value="${selectedFlight.luggagePrice}"> ${selectedFlight.luggagePrice} $
                            </td>
                            <td><input type="checkbox" name="premiumStatus${loop.index}"
                                       value="${selectedFlight.premiumPrice}"> ${selectedFlight.premiumPrice} $
                            </td>
                        </tr>
                        <p class="errorMessage">${errorPurchasingPassMessage}</p>
                    </c:forEach>


                </table>

                <table class="simple-little-table" cellspacing='0' style="margin: 0 auto">
                    <tr>
                        <td>
                            <form class="purchaseForm loginForm pane" method="POST" action="controller">
                                <input type="hidden" name="command" value="purchaseConfirmation"/>
                                <button class="purchaseButton" type="submit" name="purchaseFlight" class="ui-button"
                                        value="pushed"><fmt:message key="button.purchase"/>
                                </button>
                            </form>
                        </td>
                    </tr>
                </table>


            </form>


        </div>


        <div id="botton">

        </div>
    </div>
    <script>

        atr = "${purchaseConfirm}";
        if (atr == "true") {
            $('#flightPurchaseForm').fadeOut(10);
            $('#confirmationForm').fadeIn(10);
        }
        if (atr == "false") {
            $('#flightPurchaseForm').fadeOut(10);
            $('#confirmationForm').fadeIn(10);
        }

    </script>

    </body>
    </html>
</fmt:bundle>