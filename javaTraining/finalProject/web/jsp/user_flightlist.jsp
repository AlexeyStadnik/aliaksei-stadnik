<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<fmt:setLocale value="${Locale}" scope="session"/>
<fmt:bundle basename="resources.pagecontent">
    <html>
    <head>
        <title><fmt:message key="title"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <script type="text/javascript" src="../jquery/js/jquery.js"></script>
        <script type="text/javascript" src="../jquery/js/jquery-ui.js"></script>
        <script type="text/javascript" src="../jquery/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../js/script.js"></script>
        <link type="text/css" rel="stylesheet" href="../jquery/style/jquery-ui.css"/>
        <link rel="stylesheet" type="text/css" href="../css/styles.css"/>
        <link rel="stylesheet" type="text/css" href="../css/table_style.css"/>

    </head>
    <style>
    </style>
    <body>

    <div id="header">

    </div>

    <div id="wrapper">






            <form name="buyingForm" class="loginForm pane" >


                <table class="simple-little-table" cellspacing='0' style="margin: 0 auto">
                    <tr>
                        <th><fmt:message key="table.clientName"/></th>
                        <th><fmt:message key="table.departurePlace"/></th>
                        <th><fmt:message key="table.arrivalPlace"/></th>
                        <th><fmt:message key="table.departureTime"/></th>
                        <th><fmt:message key="table.arrivalTime"/></th>
                        <th><fmt:message key="table.addLuggage"/></th>
                        <th><fmt:message key="table.premiumStatus"/></th>
                        <th>
                            <form class="purchaseForm loginForm pane" method="POST" action="controller">
                                <input type="hidden" name="command" value="adminProfileForward"/>
                                <button class="purchaseButton" type="submit" name="adminButton"
                                        class="ui-button" value="back"><fmt:message
                                        key="button.back"/></button>
                            </form>
                        </th>
                    </tr>


                    <c:forEach items="${userFlightList}" var="temp">
                        <tr>
                            <td>${temp.name}</td>
                            <td>${temp.departurePlace}</td>
                            <td>${temp.arrivalPlace}</td>
                            <td style="padding: 1px">${temp.departureTime}</td>
                            <td style="padding: 1px">${temp.arrivalTime}</td>
                            <td>${temp.luggagePrice}</td>
                            <td>${temp.premiumPrice}</td>
                            <td>


                            </td>

                        </tr>
                    </c:forEach>

                </table>

            </form>


    </div>

    </body>
    </html>
</fmt:bundle>