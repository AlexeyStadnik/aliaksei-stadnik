import by.stadnik.epam.util.Validator;
import org.junit.Test;
import static junit.framework.Assert.*;
public class ValidationTest {

    public static String FIRST_TEST_PRICE="100";
    public static String SECOND_TEST_PRICE="10.5";
    public static String THIRD_TEST_PRICE="b";

    public static String FIRST_TEST_DATE="1999-12-12";
    public static String SECOND_TEST_DATE="1999-33-33";
    public static String THIRD_TEST_DATE="1999 12 12";



    @Test
    public void testPriceValidator() throws Exception {

        boolean test = Validator.priceValidator(FIRST_TEST_PRICE);

        assertTrue(test);

        test =  Validator.priceValidator(SECOND_TEST_PRICE);

        assertTrue(test);

        test =  Validator.priceValidator(THIRD_TEST_PRICE);

       assertFalse(test);
    }

    @Test
    public void testDateValidator() throws Exception {

        boolean test = Validator.dateValidator(FIRST_TEST_DATE);

        assertTrue(test);

        test =  Validator.dateValidator(SECOND_TEST_DATE);

        assertFalse(test);

        test =  Validator.dateValidator(THIRD_TEST_DATE);

        assertFalse(test);
    }
}
