package by.stadnik.epam.controller;

import by.stadnik.epam.command.ActionCommand;
import by.stadnik.epam.command.CommandException;
import by.stadnik.epam.command.factory.ActionFactory;
import by.stadnik.epam.connection_pool.ConnectionPool;
import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.resource.ConfigurationManager;
import by.stadnik.epam.resource.MessageManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet controller
 * @see javax.servlet.http.HttpServlet
 *
 */
@WebServlet(name = "controller", urlPatterns = {"/controller"})
public class Controller extends HttpServlet {

    public final static String LOG_FILE_NAME = "logFileName";
    public final static String NULL_PAGE = "nullPage";


    private static final Logger LOG = Logger.getLogger(Controller.class);

    /**
     * init method of the servlet.
     * Initialize logger and connection pool here
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        super.init();


        String prefix = getServletContext().getRealPath("/");
        String filename = getInitParameter(LOG_FILE_NAME);
        if (filename != null) {
            PropertyConfigurator.configure(prefix + filename);
        }
        LOG.debug("Log property file has been found");

        try {
            ConnectionPool.getInstance();
        } catch (DAOException e) {
            LOG.debug("Connection pool error!!!", e);

        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Calls execute method of the ActionCommand class
     * Gets page param and forward request and responce on that page
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String page = null;

        ActionFactory client = new ActionFactory();
        ActionCommand command = client.defineCommand(request);

        try {
            page = command.execute(request);
        } catch (CommandException e) {
            LOG.error("Exception during getting page address", e);
        }


        if (page != null) {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
            dispatcher.forward(request, response);
        } else {
            page = ConfigurationManager.getProperty("path.page.index");
            request.getSession().setAttribute(NULL_PAGE, MessageManager.getProperty("message.nullpage"));
            response.sendRedirect(request.getContextPath() + page);
        }

    }

    /**
     * Destroy method of the servlet.
     * Here connection pool has been closed
     */
    @Override
    public void destroy() {
        super.destroy();
        try {
            ConnectionPool.getInstance().close();
        } catch (DAOException e) {
            LOG.error("ConnectionPool closing error", e);
        }
        LOG.debug("Connection pool gas been closed");
    }
}