package by.stadnik.epam.util;

import by.stadnik.epam.logic.model.Client;
import by.stadnik.epam.logic.model.Person;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validation class
 *
 * @author Alex Stadnik
 */
public class Validator {

    private static final String PARAM_FLIGHT_DEPARTURE_PLACE = "departurePlace";
    private static final String PARAM_FLIGHT_ARRIVAL_PLACE = "arrivalPlace";
    private static final String PARAM_FLIGHT_DATE = "flightDate";
    private static final String PARAM_FLIGHT_TICKET_AMOUNT = "ticketAmount";

    private static final String DATE_FORMAT = "yyyy-mm-dd";

    private static final String PARAM_USER_LOGIN = "login";
    private static final String PARAM_USER_NEW_PASSWORD = "newPassword";
    private static final String PARAM_USER__NEW_PASSWORD_REPEAT = "newPasswordRepeat";

    /**
     * Regexes for validation
     */
    private static final String USERNAME_PATTERN = "^[a-zA-Z0-9_-]{5,15}$";
    private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})";
    private static final String NAME_PATTERN = "^[a-zA-Z\\s]+";
    private static final String PRICE_PATTERN = "[0-9]{1,13}(\\.[0-9]*)?";
    private static final String CITY_PATTERN = "^[a-zA-Z\\s]{3,15}$";
    private static final String TIME_PATTERN = "([01]?[0-9]|2[0-3]):[0-5][0-9]";
    private static final String INT_PATTERN = "\\d+";


    public static final String PARAM_NAME_DEPARTURE = "departurePlace";
    public static final String PARAM_NAME_ARRIVAL = "arrivalPlace";
    public static final String PARAM_NAME_DEPARTURE_DATE = "departureDate";
    public static final String PARAM_NAME_DEPARTURE_TIME = "departureTime";
    public static final String PARAM_NAME_DURATION_TIME = "durationTime";
    public static final String PARAM_FLIGHT_PRICE = "ticketPrice";
    public static final String PARAM_LUGGAGE_PRICE = "luggagePrice";
    public static final String PARAM_PREMIUM_PRICE = "premiumPrice";
    public static final String PARAM_NAME_CAPACITY = "capacity";

    /**
     * method for validating flight selection
     *
     * @param validatedInformation
     * @return
     * @see by.stadnik.epam.logic.FlightLogic
     */
    public static boolean validateFlightSelection(HashMap<String, String> validatedInformation) {


        if (validatedInformation.isEmpty()) {
            return false;
        }
        if (validatedInformation.get(PARAM_FLIGHT_DEPARTURE_PLACE) == null || validatedInformation.get(PARAM_FLIGHT_ARRIVAL_PLACE) == null ||
                validatedInformation.get(PARAM_FLIGHT_DATE) == null || validatedInformation.get(PARAM_FLIGHT_TICKET_AMOUNT) == null) {
            return false;
        }
        if (!Validator.dateValidator(validatedInformation.get(PARAM_FLIGHT_DATE))) {
            return false;
        }
        return true;
    }

    public static boolean validateUpdateInfo(HashMap<String, String> validatedInformation) {


        if (validatedInformation.isEmpty()) {
            return false;
        }
        if (validatedInformation.get(PARAM_USER_LOGIN).isEmpty() && validatedInformation.get(PARAM_USER_NEW_PASSWORD).isEmpty() && validatedInformation.get(PARAM_USER__NEW_PASSWORD_REPEAT).isEmpty()) {
            return false;
        }
        if (!validatedInformation.get(PARAM_USER_LOGIN).isEmpty() && validatedInformation.get(PARAM_USER_LOGIN).length() < 6) {
            return false;
        }
        if (!validatedInformation.get(PARAM_USER_NEW_PASSWORD).isEmpty() && validatedInformation.get(PARAM_USER__NEW_PASSWORD_REPEAT).isEmpty()
                || !validatedInformation.get(PARAM_USER__NEW_PASSWORD_REPEAT).isEmpty() && validatedInformation.get(PARAM_USER_NEW_PASSWORD).isEmpty()) {
            return false;
        }
        if (!validatedInformation.get(PARAM_USER_NEW_PASSWORD).isEmpty() && !validatedInformation.get(PARAM_USER__NEW_PASSWORD_REPEAT).isEmpty()) {
            if (!validatedInformation.get(PARAM_USER_NEW_PASSWORD).equals(validatedInformation.get(PARAM_USER__NEW_PASSWORD_REPEAT))) {
                return false;
            }
        }


        return true;

    }

    public static boolean validateSignUp(Person person) {

        Pattern pattern = Pattern.compile(USERNAME_PATTERN);
        Matcher matcher = pattern.matcher(person.getLogin());
        if (!matcher.matches()) {
            return false;
        }

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(person.getPassword());
        if (!matcher.matches()) {
            return false;
        }

        if (person.getFirstName().isEmpty() || person.getSecondName().isEmpty()) {
            return false;
        }

        return true;

    }

    public static boolean dateValidator(String strDate) {
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        formatter.setLenient(false);
        try {
            Date date = formatter.parse(strDate);
            if (!formatter.format(date).equals(strDate))
                return false;

        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    public static boolean validateFullName(String name) {

        Pattern pattern = Pattern.compile(NAME_PATTERN);
        Matcher matcher = pattern.matcher(name);
        if (!matcher.matches()) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean validateFlightUpdateInfo(ArrayList<String> updatedInfo) {
        Pattern pattern = Pattern.compile(PRICE_PATTERN);
        Matcher matcher = null;

        for (String checkedValue : updatedInfo) {
            matcher = pattern.matcher(checkedValue);
            if (!matcher.matches()) {
                return false;
            }
        }
        return true;
    }

    public static boolean validateDestination(String destination) {
        Pattern pattern = Pattern.compile(CITY_PATTERN);
        Matcher matcher = pattern.matcher(destination);
        if (!matcher.matches()) {
            return false;
        } else {
            return true;
        }

    }

    public static boolean timeValidator(String time) {
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(TIME_PATTERN);
        matcher = pattern.matcher(time);
        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean priceValidator(String price) {
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(PRICE_PATTERN);
        matcher = pattern.matcher(price);
        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean capacityValidator(String capacity) {
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(INT_PATTERN);
        matcher = pattern.matcher(capacity);
        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean validateFlightAdd(HashMap<String, String> addFlightInfo) {

        String checkInfo = addFlightInfo.get(PARAM_NAME_DEPARTURE_DATE);
        if (!dateValidator(checkInfo)) {
            return false;
        }
        checkInfo = addFlightInfo.get(PARAM_NAME_DEPARTURE_TIME);
        if (!timeValidator(checkInfo)) {
            return false;
        }
        checkInfo = addFlightInfo.get(PARAM_NAME_DURATION_TIME);
        if (!timeValidator(checkInfo)) {
            return false;
        }
        checkInfo = addFlightInfo.get(PARAM_FLIGHT_PRICE);
        if (!priceValidator(checkInfo)) {
            return false;
        }
        checkInfo = addFlightInfo.get(PARAM_LUGGAGE_PRICE);
        if (!priceValidator(checkInfo)) {
            return false;
        }
        checkInfo = addFlightInfo.get(PARAM_PREMIUM_PRICE);
        if (!priceValidator(checkInfo)) {
            return false;
        }
        checkInfo = addFlightInfo.get(PARAM_NAME_CAPACITY);
        if (!capacityValidator(checkInfo)) {
            return false;
        }
        if (addFlightInfo.get(PARAM_NAME_DEPARTURE).equals(addFlightInfo.get(PARAM_NAME_ARRIVAL))) {
            return false;
        }
        return true;
    }
}
