package by.stadnik.epam.util;

import by.stadnik.epam.logic.UserLogic;
import by.stadnik.epam.logic.exception.LogicException;

import java.util.Random;

/**
 * Created by Алексей on 20.04.2015.
 */
public class IdGenerator {
    /**
     * Generates new ID for new user according maximal id in the DB
     *
     * @return new id
     * @throws LogicException
     */
    public static int idGenerator() throws LogicException {

        int maxId = UserLogic.takeMaxId();

        return ++maxId;
    }
}
