package by.stadnik.epam.connection_pool;

import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.resource.ConfigurationManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/**
 * Connection pool class . Initialize all DB connections
 */
public class ConnectionPool {

    private static ConnectionPool instance = null;
    private BlockingDeque<Connection> availableConnections;
    private BlockingDeque<Connection> usedConnections;

    private static Lock lock = new ReentrantLock();
    private static final Logger LOG = Logger.getLogger(ConnectionPool.class);

    private static final String URL = ConfigurationManager.getProperty("db.url");
    private static final String USERNAME = ConfigurationManager.getProperty("db.username");
    private static final String PASSWORD = ConfigurationManager.getProperty("db.password");

    public static final int CONNECTIONS_AMOUNT = Integer.parseInt(ConfigurationManager.getProperty("cp.connections_amount"));
    public static final int WAITING_TIME = Integer.parseInt(ConfigurationManager.getProperty("cp.waiting_time"));
    private static int REPEAT_COUNT = 3;

    /**
     * Private constructor.
     * Gets new connections . If cant tryes 3 times then throws DAOException
     * @throws DAOException
     */
    private ConnectionPool() throws DAOException {

        LOG.debug("Initializing of the pool has been started");
        availableConnections = new LinkedBlockingDeque<Connection>();
        usedConnections = new LinkedBlockingDeque<Connection>();
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        } catch (SQLException e) {
            throw new RuntimeException("Exception during connection pool Initialization process", e);
        }

        for (int i = 0; i < CONNECTIONS_AMOUNT; i++) {

            try {
                availableConnections.add(getConnection());
            } catch (DAOException e) {
                LOG.debug("Cant add connection :" + i);
                if (REPEAT_COUNT > 0) {
                    REPEAT_COUNT--;
                    i--;
                } else {
                    throw new DAOException("Exception while getting connections", e);
                }
            }
            LOG.debug("new connection is added :" + i);
        }

    }

    /**
     * Singleton method for getting connection pool instance
     * @return instance of the connection pool
     * @throws DAOException
     */
    public static ConnectionPool getInstance() throws DAOException {
        try {
            if (lock.tryLock(WAITING_TIME, TimeUnit.MILLISECONDS)) {


                if (instance == null) {
                    instance = new ConnectionPool();
                }

            }
        } catch (InterruptedException e) {
            throw new DAOException("Exception during getting the connection pool instance", e);
        } finally {
            lock.unlock();
        }
        return instance;
    }


    private Connection getConnection() throws DAOException {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);

        } catch (SQLException e) {
            throw new DAOException("SQLException during getting the connection", e);
        }
        return conn;
    }

    /**
     * Method for getting connection.
     * Takes it from availableConnections and puts it in usedConnections
     * @return Connection
     * @throws DAOException
     */
    public Connection retrieve() throws DAOException {
        Connection newConn = null;

        if (availableConnections.size() == 0) {
            newConn = instance.getConnection();
        } else {
            newConn = availableConnections.poll();


        }
        if (availableConnections.size() > CONNECTIONS_AMOUNT) {
            for (int i = 0; i < availableConnections.size() - CONNECTIONS_AMOUNT; i++) {
                try {
                    availableConnections.poll().close();
                } catch (SQLException e) {
                    LOG.error("Exception during connection closing", e);
                }
            }
        }

        usedConnections.add(newConn);
        return newConn;
    }

    /**
     * Method for putting back used connection
     * @param connection - connection
     * @throws DAOException
     */
    public void putback(Connection connection) throws DAOException {
        if (connection != null) {
            if (usedConnections.remove(connection)) {
                availableConnections.add(connection);
            } else {
                throw new DAOException("Connection not in the usedConns array");
            }
        }
    }

    /**
     * Method for closing all connection pool connections
     * @throws DAOException
     */

    public void close() throws DAOException {

        Connection connection = null;
        int connectionsSize = usedConnections.size();
        for (int i = 0; i < connectionsSize; i++) {
            connection = usedConnections.poll();
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DAOException("SQLException during connection closing", e);
            }
        }

        connectionsSize = availableConnections.size();

        for (int i = 0; i < connectionsSize; i++) {
            connection = availableConnections.poll();
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DAOException("SQLException during connection closing", e);
            }
        }

    }
}


