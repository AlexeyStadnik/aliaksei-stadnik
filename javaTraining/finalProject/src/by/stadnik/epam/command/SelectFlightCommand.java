package by.stadnik.epam.command;

import by.stadnik.epam.logic.FlightLogic;
import by.stadnik.epam.logic.exception.LogicException;
import by.stadnik.epam.logic.model.Flight;
import by.stadnik.epam.resource.ConfigurationManager;
import by.stadnik.epam.resource.MessageManager;
import by.stadnik.epam.util.Validator;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Алексей on 26.04.2015.
 */
public class SelectFlightCommand implements ActionCommand {

    public static final String PARAM_NAME_DEPARTURE = "departurePlace";
    public static final String PARAM_NAME_ARRIVAL = "arrivalPlace";
    public static final String PARAM_NAME_FLIGHT_DATE = "flightDate";
    public static final String PARAM_NAME_TICKET_AMOUNT = "ticketAmount";
    public static final String PARAM_NAME_CLIENT_TICKET_AMOUNT = "clientTicketAmount";
    public static final String PARAM_FLIGHT_LIST = "flightList";

    public static final String ERROR_SELECTOR_MESSAGE = "errorFlightSelectorMessage";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        String departurePlace = request.getParameter(PARAM_NAME_DEPARTURE);
        String arrivalPLace = request.getParameter(PARAM_NAME_ARRIVAL);
        String flightDate = request.getParameter(PARAM_NAME_FLIGHT_DATE);
        String ticketAmount = request.getParameter(PARAM_NAME_TICKET_AMOUNT);

        HashMap<String, String> selectFlightInfo = new HashMap<>();

        selectFlightInfo.put(PARAM_NAME_DEPARTURE, departurePlace);
        selectFlightInfo.put(PARAM_NAME_ARRIVAL, arrivalPLace);
        selectFlightInfo.put(PARAM_NAME_FLIGHT_DATE, flightDate);
        selectFlightInfo.put(PARAM_NAME_TICKET_AMOUNT, ticketAmount);

        request.getSession().setAttribute(PARAM_NAME_CLIENT_TICKET_AMOUNT, ticketAmount);


        if (Validator.validateFlightSelection(selectFlightInfo)) {
            try {
                ArrayList<Flight> selectedFlight = FlightLogic.selectFlightList(selectFlightInfo);
                request.getSession().setAttribute(PARAM_FLIGHT_LIST, selectedFlight);
            } catch (LogicException e) {
                throw new CommandException("Exception while getting flight list", e);
            }
        } else {
            request.setAttribute(ERROR_SELECTOR_MESSAGE, MessageManager.getProperty("message.flightError"));
            return ConfigurationManager.getProperty("path.page.starter");
        }

        return ConfigurationManager.getProperty("path.page.chooseFlight");
    }
}