package by.stadnik.epam.command;

import by.stadnik.epam.logic.AdministratorLogic;
import by.stadnik.epam.logic.exception.LogicException;
import by.stadnik.epam.logic.model.Flight;
import by.stadnik.epam.resource.ConfigurationManager;
import by.stadnik.epam.resource.MessageManager;
import by.stadnik.epam.util.Validator;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * Command for deleting flight
 * Administrator can only deletes flights for which there were no tickets sold
 *
 * @see by.stadnik.epam.command.ActionCommand
 */
public class DeleteFlightCommand implements ActionCommand {
    public final static String PARAM_SELECTED_FLIGHT = "selectedFlight";


    public final static String ERROR_UPDATE_MESSAGE = "deleteErrorMessage";
    public final static String PARAM_SUCCESSFUL_UPDATE = "deleteSuccessfulMessage";
    public final static String PARAM_IS_DELETED = "deleteConfirmation";


    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        String page = null;

        Flight updatedFlight = (Flight) request.getSession().getAttribute(PARAM_SELECTED_FLIGHT);


        boolean flag = false;

        try {
            flag = AdministratorLogic.deleteFlight(updatedFlight);
        } catch (LogicException e) {
            throw new CommandException("Exception during flight deleting process, check input information or DB connection", e);
        }


        if (flag) {
            request.setAttribute(PARAM_IS_DELETED, "true");
            request.setAttribute(PARAM_SUCCESSFUL_UPDATE, MessageManager.getProperty("message.deleteConfirmation"));

        } else {
            request.setAttribute(ERROR_UPDATE_MESSAGE, MessageManager.getProperty("message.deleteConfirmationError"));
        }


        return ConfigurationManager.getProperty("path.page.updateFlight");
    }
}
