package by.stadnik.epam.command;

import by.stadnik.epam.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Command to forward to the contacts page
 */
public class ContactCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        String page = ConfigurationManager.getProperty("path.page.contacts");
        return page;

    }
}

