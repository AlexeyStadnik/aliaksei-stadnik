package by.stadnik.epam.command;

import by.stadnik.epam.logic.AdministratorLogic;
import by.stadnik.epam.logic.StartLogic;
import by.stadnik.epam.logic.exception.LogicException;
import by.stadnik.epam.resource.ConfigurationManager;
import by.stadnik.epam.resource.MessageManager;
import by.stadnik.epam.util.Validator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * Command for adding new destination in the dataBase
 *
 * @see by.stadnik.epam.logic.AdministratorLogic
 */
public class AddDestinationCommand implements ActionCommand {

    private static final Logger LOG = Logger.getLogger(AddDestinationCommand.class);

    public static final String PARAM_DESTINATION = "destination";
    public static final String ERROR_MESSAGE = "destinationErrorMessage";
    public static final String CONFIRM_MESSAGE = "addDestinationConfirm";


    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        boolean flag = false;
        String page = null;
        String destination = request.getParameter(PARAM_DESTINATION);

        if (!Validator.validateDestination(destination)) {
            request.setAttribute(ERROR_MESSAGE, MessageManager.getProperty("message.flightError"));
            return ConfigurationManager.getProperty("path.page.admin_profile");
        }

        try {
            flag = AdministratorLogic.addDestination(destination);
        } catch (LogicException e) {
            throw new CommandException("Exception during adding new destination", e);
        }


        if (flag) {
            request.setAttribute(CONFIRM_MESSAGE, MessageManager.getProperty("message.addDestination"));
            page = ConfigurationManager.getProperty("path.page.index");
        } else {
            request.setAttribute(ERROR_MESSAGE, MessageManager.getProperty("message.addDestinationError"));
            page = ConfigurationManager.getProperty("path.page.admin_profile");
        }

        return page;
    }
}