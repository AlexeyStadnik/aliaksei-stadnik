package by.stadnik.epam.command;

import by.stadnik.epam.logic.AdministratorLogic;
import by.stadnik.epam.logic.FlightLogic;
import by.stadnik.epam.logic.LoginLogic;
import by.stadnik.epam.logic.RegisterLogic;
import by.stadnik.epam.logic.exception.LogicException;
import by.stadnik.epam.logic.model.Client;
import by.stadnik.epam.logic.model.Flight;
import by.stadnik.epam.logic.model.Person;
import by.stadnik.epam.resource.ConfigurationManager;
import by.stadnik.epam.resource.MessageManager;
import by.stadnik.epam.util.IdGenerator;
import by.stadnik.epam.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Command for adding new flight
 * All flight info is added to the hashMap
 * Available only to the administrator
 *
 * @see by.stadnik.epam.logic.AdministratorLogic
 */
public class AddFlightCommand implements ActionCommand {

    public static final String PARAM_NAME_DEPARTURE = "departurePlace";
    public static final String PARAM_NAME_ARRIVAL = "arrivalPlace";
    public static final String PARAM_NAME_DEPARTURE_DATE = "departureDate";
    public static final String PARAM_NAME_DEPARTURE_TIME = "departureTime";
    public static final String PARAM_NAME_DURATION_TIME = "durationTime";
    public static final String PARAM_FLIGHT_PRICE = "ticketPrice";
    public static final String PARAM_LUGGAGE_PRICE = "luggagePrice";
    public static final String PARAM_PREMIUM_PRICE = "premiumPrice";
    public static final String PARAM_NAME_CAPACITY = "capacity";

    public static final String ERROR_VALIDATION_MESSAGE = "errorFlightAddMessage";
    public static final String CONFIRMATION_MESSAGE = "addFlightConfirm";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        String departurePlace = request.getParameter(PARAM_NAME_DEPARTURE);
        String arrivalPlace = request.getParameter(PARAM_NAME_ARRIVAL);
        String departureDate = request.getParameter(PARAM_NAME_DEPARTURE_DATE);
        String departureTime = request.getParameter(PARAM_NAME_DEPARTURE_TIME);
        String durationTime = request.getParameter(PARAM_NAME_DURATION_TIME);
        String flightPrice = request.getParameter(PARAM_FLIGHT_PRICE);
        String luggagePrice = request.getParameter(PARAM_LUGGAGE_PRICE);
        String premiumPrice = request.getParameter(PARAM_PREMIUM_PRICE);
        String capacity = request.getParameter(PARAM_NAME_CAPACITY);

        HashMap<String, String> addFlightInfo = new HashMap<>();

        addFlightInfo.put(PARAM_NAME_DEPARTURE, departurePlace);
        addFlightInfo.put(PARAM_NAME_ARRIVAL, arrivalPlace);
        addFlightInfo.put(PARAM_NAME_DEPARTURE_DATE, departureDate);
        addFlightInfo.put(PARAM_NAME_DEPARTURE_TIME, departureTime);
        addFlightInfo.put(PARAM_NAME_DURATION_TIME, durationTime);
        addFlightInfo.put(PARAM_FLIGHT_PRICE, flightPrice);
        addFlightInfo.put(PARAM_LUGGAGE_PRICE, luggagePrice);
        addFlightInfo.put(PARAM_PREMIUM_PRICE, premiumPrice);
        addFlightInfo.put(PARAM_NAME_CAPACITY, capacity);

        boolean flag = false;
        String page = null;


        if (Validator.validateFlightAdd(addFlightInfo)) {
            try {
                flag = AdministratorLogic.addFlight(addFlightInfo);
            } catch (LogicException e) {
                throw new CommandException("Exception during flight add process", e);
            }
        } else {
            request.setAttribute(ERROR_VALIDATION_MESSAGE, MessageManager.getProperty("message.addFlightValError"));
            return ConfigurationManager.getProperty("path.page.admin_profile");
        }

        if (flag) {
            request.setAttribute(CONFIRMATION_MESSAGE, MessageManager.getProperty("message.addFlightConfirm"));
            page = ConfigurationManager.getProperty("path.page.starter");
        } else {
            request.setAttribute(ERROR_VALIDATION_MESSAGE, MessageManager.getProperty("message.addFlightError"));
            page = ConfigurationManager.getProperty("path.page.admin_profile");
        }

        return page;
    }
}
