package by.stadnik.epam.command;

import by.stadnik.epam.logic.model.Flight;
import by.stadnik.epam.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * Command for choosing flight for updating information
 * Available only for administrators
 */
public class UpdateFlightCommand implements ActionCommand {

    public final static String PARAM_FLIGHT_LIST = "flightList";
    public final static String PARAM_UPDATED_FLIGHT = "updateFlight";
    public final static String PARAM_SELECTED_FLIGHT = "selectedFlight";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        ArrayList<Flight> flightList = (ArrayList<Flight>) request.getSession().getAttribute(PARAM_FLIGHT_LIST);
        for (Flight flight : flightList) {
            if (flight.getId() == Integer.parseInt(request.getParameter(PARAM_UPDATED_FLIGHT))) {
                request.getSession().setAttribute(PARAM_SELECTED_FLIGHT, flight);
            }
        }


        return ConfigurationManager.getProperty("path.page.updateFlight");
    }
}