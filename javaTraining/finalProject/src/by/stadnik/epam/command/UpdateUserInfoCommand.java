package by.stadnik.epam.command;

import by.stadnik.epam.logic.UserLogic;
import by.stadnik.epam.logic.exception.LogicException;
import by.stadnik.epam.resource.ConfigurationManager;
import by.stadnik.epam.resource.MessageManager;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * Command for updating userInfo
 * Available only for clients
 * Info stores in the hashMap
 */
public class UpdateUserInfoCommand implements ActionCommand {

    public static final String PARAM_USER_ID = "userId";
    public static final String PARAM_USER_LOGIN = "login";
    public static final String PARAM_USER_NEW_PASSWORD = "newPassword";
    public static final String PARAM_USER__NEW_PASSWORD_REPEAT = "newPasswordRepeat";


    public static final String PARAM_USERLOGIN = "userLogin";
    public static final String PARAM_SUCCESSFUL_UPDATE = "successfulUpdate";
    public static final String ERROR_UPDATE_MESSAGE = "updateErrorMessage";


    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        String userId = request.getSession().getAttribute(PARAM_USER_ID).toString();
        String newLogin = request.getParameter(PARAM_USER_LOGIN);
        String newPassword = request.getParameter(PARAM_USER_NEW_PASSWORD);
        String newPasswordRepeat = request.getParameter(PARAM_USER__NEW_PASSWORD_REPEAT);

        HashMap<String, String> userUpdateInfo = new HashMap<>();

        userUpdateInfo.put(PARAM_USER_ID, userId);
        userUpdateInfo.put(PARAM_USER_LOGIN, newLogin);
        userUpdateInfo.put(PARAM_USER_NEW_PASSWORD, newPassword);
        userUpdateInfo.put(PARAM_USER__NEW_PASSWORD_REPEAT, newPasswordRepeat);

        boolean flag = false;

        try {
            flag = UserLogic.updateUserInfo(userUpdateInfo);
        } catch (LogicException e) {
            throw new CommandException("Exception while updating user info", e);
        }


        if (flag) {
            if (newLogin != null) {
                request.getSession().setAttribute(PARAM_USERLOGIN, newLogin);
                request.setAttribute(PARAM_SUCCESSFUL_UPDATE, MessageManager.getProperty("message.updateConfirmation"));
            }
        } else {
            request.setAttribute(ERROR_UPDATE_MESSAGE, MessageManager.getProperty("message.updateConfirmationError"));
        }


        return ConfigurationManager.getProperty("path.page.user_profile");
    }
}
