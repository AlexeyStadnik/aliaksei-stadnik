package by.stadnik.epam.command;

import by.stadnik.epam.logic.FlightLogic;
import by.stadnik.epam.logic.exception.LogicException;
import by.stadnik.epam.logic.model.Flight;
import by.stadnik.epam.resource.ConfigurationManager;
import by.stadnik.epam.resource.MessageManager;
import by.stadnik.epam.util.Validator;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Command confirms purchasing of the ticket
 * All ticket info stores int the hashMap
 *
 * @see by.stadnik.epam.logic.FlightLogic
 */
public class PurchaseConfirmationCommand implements ActionCommand {

    public static final String PARAM_USER_ID = "userId";
    public static final String PARAM_SELECTED_FLIGHT = "selectedFlight";
    public static final String PARAM_CLIENT_TICKET_AMOUNT = "clientTicketAmount";
    public static final String PARAM_FLIGHT_ID = "flightId";
    public static final String PARAM_FLIGHT_PRICE = "flightPrice";
    public static final String PARAM_LUGGAGE_PRICE = "luggagePrice";
    public static final String PARAM_PREMIUM_PRICE = "premiumPrice";

    public static final String PARAM_CLIENT_NAME = "clientName";
    public static final String PARAM_ADDITIONAL_LUGGAGE = "additionalLuggage";
    public static final String PARAM_PREMIUM_STATUS = "premiumStatus";

    public static final String PARAM_PURCHASE_CONFIRM = "purchaseConfirm";


    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        String page = null;
        HashMap<String, String> purchasingFlightInfo = new HashMap<>();

        String userId = request.getSession().getAttribute(PARAM_USER_ID).toString();
        Flight selectedFlight = (Flight) request.getSession().getAttribute(PARAM_SELECTED_FLIGHT);
        int amountOfTicket = Integer.parseInt(request.getSession().getAttribute(PARAM_CLIENT_TICKET_AMOUNT).toString());


        purchasingFlightInfo.put(PARAM_USER_ID, userId);
        purchasingFlightInfo.put(PARAM_FLIGHT_ID, String.valueOf(selectedFlight.getId()));

        purchasingFlightInfo.put(PARAM_CLIENT_TICKET_AMOUNT, String.valueOf(amountOfTicket));
        purchasingFlightInfo.put(PARAM_FLIGHT_PRICE, String.valueOf(selectedFlight.getFlightPrice()));
        purchasingFlightInfo.put(PARAM_LUGGAGE_PRICE, String.valueOf(selectedFlight.getLuggagePrice()));
        purchasingFlightInfo.put(PARAM_PREMIUM_PRICE, String.valueOf(selectedFlight.getPremiumPrice()));


        for (int i = 1; i <= amountOfTicket; i++) {
            String clientName = request.getParameter(PARAM_CLIENT_NAME + i);

            if (!Validator.validateFullName(clientName)) {
                request.setAttribute(PARAM_PURCHASE_CONFIRM, MessageManager.getProperty("message.purchaseError"));
                return ConfigurationManager.getProperty("path.page.starter");
            }


            purchasingFlightInfo.put(PARAM_CLIENT_NAME + i, clientName);

            if (request.getParameter(PARAM_ADDITIONAL_LUGGAGE + i) == null) {
                purchasingFlightInfo.put(PARAM_ADDITIONAL_LUGGAGE + i, "false");
            } else {
                purchasingFlightInfo.put(PARAM_ADDITIONAL_LUGGAGE + i, "true");
            }

            if (request.getParameter(PARAM_PREMIUM_STATUS + i) == null) {
                purchasingFlightInfo.put(PARAM_PREMIUM_STATUS + i, "false");
            } else {
                purchasingFlightInfo.put(PARAM_PREMIUM_STATUS + i, "true");
            }


        }

        try {
            if (FlightLogic.purchaseConfirmation(purchasingFlightInfo)) {
                request.setAttribute(PARAM_PURCHASE_CONFIRM, MessageManager.getProperty("message.purchaseConfirmation"));
                page = ConfigurationManager.getProperty("path.page.starter");
            } else {
                request.setAttribute(PARAM_PURCHASE_CONFIRM, MessageManager.getProperty("message.purchaseError"));
                page = ConfigurationManager.getProperty("path.page.starter");
            }
        } catch (LogicException e) {
            throw new CommandException("Exception during purchasing process", e);
        }

        return page;

    }
}