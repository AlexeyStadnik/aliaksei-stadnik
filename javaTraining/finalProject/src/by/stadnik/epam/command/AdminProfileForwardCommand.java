package by.stadnik.epam.command;

import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.logic.AdministratorLogic;
import by.stadnik.epam.logic.FlightLogic;
import by.stadnik.epam.logic.exception.LogicException;
import by.stadnik.epam.logic.model.Client;
import by.stadnik.epam.logic.model.ClientOnFlight;
import by.stadnik.epam.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * Command for forwarding administrator to the system
 * Gets user list from the dataBase
 */
public class AdminProfileForwardCommand implements ActionCommand {

    private static final String PARAM_USER_LIST = "userList";


    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        ArrayList<Client> userList = null;
        try {
            userList = AdministratorLogic.takeUserList();
        } catch (LogicException e) {
            throw new CommandException("Exception during getting user list", e);
        }

        request.setAttribute(PARAM_USER_LIST, userList);
        return ConfigurationManager.getProperty("path.page.admin_profile");
    }
}
