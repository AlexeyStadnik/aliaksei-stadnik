package by.stadnik.epam.command;

import by.stadnik.epam.logic.AdministratorLogic;
import by.stadnik.epam.logic.UserLogic;
import by.stadnik.epam.logic.exception.LogicException;
import by.stadnik.epam.logic.model.Flight;
import by.stadnik.epam.resource.ConfigurationManager;
import by.stadnik.epam.resource.MessageManager;
import by.stadnik.epam.util.Validator;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Command for updating flight
 * All info stores in the arrayList
 * Even thought info is not updated it stores in the arrayList
 */
public class UpdateFlightConfirmCommand implements ActionCommand {

    public final static String PARAM_SELECTED_FLIGHT = "selectedFlight";
    public final static String PARAM_FLIGHT_PRICE = "ticketPrice";
    public final static String PARAM_LUGGAGE_PRICE = "luggagePrice";
    public final static String PARAM_PREMIUM_PRICE = "premiumPrice";

    public final static String ERROR_UPDATE_MESSAGE = "updateErrorMessage";
    public final static String PARAM_SUCCESSFUL_UPDATE = "successfulUpdate";


    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        String page = null;

        Flight updatedFlight = (Flight) request.getSession().getAttribute(PARAM_SELECTED_FLIGHT);

        ArrayList<String> updatedInfo = new ArrayList<String>() {
            {
                this.add(request.getParameter(PARAM_FLIGHT_PRICE));
                this.add(request.getParameter(PARAM_LUGGAGE_PRICE));
                this.add(request.getParameter(PARAM_PREMIUM_PRICE));

            }
        };

        if (!Validator.validateFlightUpdateInfo(updatedInfo)) {
            request.setAttribute(ERROR_UPDATE_MESSAGE, MessageManager.getProperty("message.updateConfirmationError"));
            return ConfigurationManager.getProperty("path.page.updateFlight");
        }

        updatedFlight.setFlightPrice(Double.parseDouble(request.getParameter(PARAM_FLIGHT_PRICE)));
        updatedFlight.setLuggagePrice(Double.parseDouble(request.getParameter(PARAM_LUGGAGE_PRICE)));
        updatedFlight.setPremiumPrice(Double.parseDouble(request.getParameter(PARAM_PREMIUM_PRICE)));

        boolean flag = false;

        try {
            flag = AdministratorLogic.updateFlightInfo(updatedFlight);
        } catch (LogicException e) {
            throw new CommandException("Exception during flight updating process, check input information or DB connection", e);
        }


        if (flag) {
            request.getSession().setAttribute(PARAM_SELECTED_FLIGHT, updatedFlight);
            request.setAttribute(PARAM_SUCCESSFUL_UPDATE, MessageManager.getProperty("message.updateConfirmation"));

        } else {
            request.setAttribute(ERROR_UPDATE_MESSAGE, MessageManager.getProperty("message.updateConfirmationError"));
        }


        return ConfigurationManager.getProperty("path.page.updateFlight");
    }
}