package by.stadnik.epam.command;

import by.stadnik.epam.logic.StartLogic;
import by.stadnik.epam.logic.exception.LogicException;
import by.stadnik.epam.logic.model.Flight;
import by.stadnik.epam.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Starting command of the application
 * Gets destenaton list and forwards to the starter page
 */
public class StartCommand implements ActionCommand {

    public static final String PARAM_FLIGHT_ARRAY = "flightArray";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        ArrayList<String> flightList = null;
        try {
            flightList = StartLogic.takeStartingFlightList();
        } catch (LogicException e) {
            return ConfigurationManager.getProperty("path.page.error");
        }

        request.getSession().setAttribute(PARAM_FLIGHT_ARRAY, flightList);
        return ConfigurationManager.getProperty("path.page.starter");
    }
}
