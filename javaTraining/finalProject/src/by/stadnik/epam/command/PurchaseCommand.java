package by.stadnik.epam.command;

import by.stadnik.epam.logic.model.Flight;
import by.stadnik.epam.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * Created by Алексей on 02.05.2015.
 */
public class PurchaseCommand implements ActionCommand {

    public final static String PARAM_FLIGHT_LIST = "flightList";
    public final static String PARAM_PURCHASE_FLIGHT = "purchaseFlight";
    public final static String PARAM_SELECTED_FLIGHT = "selectedFlight";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        ArrayList<Flight> flightList = (ArrayList<Flight>) request.getSession().getAttribute(PARAM_FLIGHT_LIST);
        for (Flight flight : flightList) {
            if (flight.getId() == Integer.parseInt(request.getParameter(PARAM_PURCHASE_FLIGHT))) {
                request.getSession().setAttribute(PARAM_SELECTED_FLIGHT, flight);
            }
        }


        return ConfigurationManager.getProperty("path.page.purchaseFlight");
    }
}