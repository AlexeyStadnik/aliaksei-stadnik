package by.stadnik.epam.command;

import by.stadnik.epam.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * If there are command which there are not in the command enumeration , empty command will be created.
 */
public class EmptyCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {

        String page = ConfigurationManager.getProperty("path.page.error");
        return page;
    }
}