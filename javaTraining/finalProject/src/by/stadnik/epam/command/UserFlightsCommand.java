package by.stadnik.epam.command;

import by.stadnik.epam.logic.FlightLogic;
import by.stadnik.epam.logic.exception.LogicException;
import by.stadnik.epam.logic.model.ClientOnFlight;
import by.stadnik.epam.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * Gets all user flights from the DB
 */
public class UserFlightsCommand implements ActionCommand {

    private static final String PARAM_BUTTON = "adminButton";
    private static final String PARAM_USER_FLIGHTLIST = "userFlightList";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {


        int userId = Integer.parseInt(request.getParameter(PARAM_BUTTON));


        ArrayList<ClientOnFlight> flightList = null;
        try {
            flightList = FlightLogic.selectFlightListByUserID(userId);
        } catch (LogicException e) {
            throw new CommandException("Exception while getting flgiht list", e);
        }
        request.setAttribute(PARAM_USER_FLIGHTLIST, flightList);
        return ConfigurationManager.getProperty("path.page.user_flightlist");
    }
}
