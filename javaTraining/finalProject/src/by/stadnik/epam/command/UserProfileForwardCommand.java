package by.stadnik.epam.command;

import by.stadnik.epam.logic.FlightLogic;
import by.stadnik.epam.logic.exception.LogicException;
import by.stadnik.epam.logic.model.ClientOnFlight;
import by.stadnik.epam.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * Command for forwarding client to the his own page
 */
public class UserProfileForwardCommand implements ActionCommand {

    private static final String PARAM_USER_ID = "userId";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        int userId = Integer.parseInt(request.getSession().getAttribute(PARAM_USER_ID).toString());

        ArrayList<ClientOnFlight> flightList = null;
        try {
            flightList = FlightLogic.selectFlightListByUserID(userId);
        } catch (LogicException e) {
            throw new CommandException("Exception while getting flight list", e);
        }
        request.setAttribute("userFlightList", flightList);
        return ConfigurationManager.getProperty("path.page.user_profile");
    }
}