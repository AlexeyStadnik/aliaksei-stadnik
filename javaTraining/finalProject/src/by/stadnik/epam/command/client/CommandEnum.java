package by.stadnik.epam.command.client;

import by.stadnik.epam.command.*;

public enum CommandEnum {
    START {
        {
            this.command = new StartCommand();
        }
    },
    LOGIN {
        {
            this.command = new LoginCommand();
        }
    },
    REGISTER {
        {
            this.command = new RegisterCommand();
        }
    },
    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    },
    LANGUAGE {
        {
            this.command = new LanguageCommand();
        }
    },
    SELECTFLIGHT {
        {
            this.command = new SelectFlightCommand();
        }
    },
    PURCHASE {
        {
            this.command = new PurchaseCommand();
        }
    },
    PURCHASECONFIRMATION {
        {
            this.command = new PurchaseConfirmationCommand();
        }
    },
    UPDATEFLIGHT {
        {
            this.command = new UpdateFlightCommand();
        }
    },
    UPDATEFLIGHTCONFIRMATION{
        {
            this.command = new UpdateFlightConfirmCommand();
        }
    },
    DELETEFLIGHT{
        {
            this.command = new DeleteFlightCommand();
        }
    },
    ADMINPROFILEFORWARD{
        {
            this.command = new AdminProfileForwardCommand();
        }
    },
    ADDDESTINATION{
        {
            this.command = new AddDestinationCommand();
        }
    },
    ADDFLIGHT{
        {
            this.command = new AddFlightCommand();
        }
    },
    USERFLIGHTS{
        {
            this.command = new UserFlightsCommand();
        }
    },
    USERPROFILEFORWARD {
        {
            this.command = new UserProfileForwardCommand();
        }
    },
    UPDATEUSERINFO {
        {
            this.command = new UpdateUserInfoCommand();
        }
    },
    CONTACTS {
        {
            this.command = new ContactCommand();
        }
    };

    ActionCommand command;

    public ActionCommand getCurrentCommand() {
        return command;
    }
}
