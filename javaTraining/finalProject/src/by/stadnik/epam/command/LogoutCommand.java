package by.stadnik.epam.command;

import by.stadnik.epam.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Command for loging out form the system
 */
public class LogoutCommand implements ActionCommand {


    @Override
    public String execute(HttpServletRequest request) {

        String page = ConfigurationManager.getProperty("path.page.index");
        request.getSession().invalidate();
        return page;

    }


}
