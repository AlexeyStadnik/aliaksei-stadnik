package by.stadnik.epam.command;

import by.stadnik.epam.logic.LoginLogic;
import by.stadnik.epam.logic.RegisterLogic;
import by.stadnik.epam.logic.exception.LogicException;
import by.stadnik.epam.logic.model.Client;
import by.stadnik.epam.logic.model.Person;
import by.stadnik.epam.resource.ConfigurationManager;
import by.stadnik.epam.resource.MessageManager;
import by.stadnik.epam.util.IdGenerator;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

/**
 * Command for registration into the system
 */

public class RegisterCommand implements ActionCommand {

    public static final String PARAM_SIGN_UP = "signup";
    public static final String PARAM_LOGIN_OR_SIGNUP = "loginOrSignUp";
    public static final String PARAM_REGISTER_MESSAGE = "registerMessage";
    public static final String ERROR_LOGIN_MESSAGE = "errorLoginPassMessage";

    public static final String PARAM_USER = "user";
    public static final String ERROR_REGISTER_MESSAGE = "errorRegisterPassMessage";
    public static final String REGISTER_MESSAGE = "registerMessage";


    public static final String PARAM_NAME_LOGIN = "login";
    public static final String PARAM_NAME_PASSWORD = "password";
    public static final String PARAM_NAME_FIRST_NAME = "firstName";
    public static final String PARAM_NAME_SECOND_NAME = "secondName";
    public static final String PARAM_NAME_ADRESS = "adress";

    public static final String PARAM_NAME_ROLE = "role";
    public static final String PARAM_NAME_USER = "user";
    public static final String PARAM_NAME_USERID = "userId";
    public static final String PARAM_NAME_USER_LOGIN = "userLogin";


    @Override
    public String execute(HttpServletRequest request) throws CommandException {


        if (PARAM_SIGN_UP.equals(request.getParameter(PARAM_LOGIN_OR_SIGNUP))) {
            request.setAttribute(PARAM_REGISTER_MESSAGE, "true");
            return ConfigurationManager.getProperty("path.page.starter");
        }

        String page = null;
        Person person = null;

        String login = request.getParameter(PARAM_NAME_LOGIN);
        String pass = request.getParameter(PARAM_NAME_PASSWORD);
        String firstName = request.getParameter(PARAM_NAME_FIRST_NAME);
        String secondName = request.getParameter(PARAM_NAME_SECOND_NAME);
        String adress = request.getParameter(PARAM_NAME_ADRESS);

        try {
            person = LoginLogic.checkLogin(login, pass);
        } catch (LogicException e) {
            throw new CommandException("LogicException", e);
        }
        if (person != null) {
            request.setAttribute(ERROR_LOGIN_MESSAGE, MessageManager.getProperty("message.loginerror"));
            return ConfigurationManager.getProperty("path.page.starter");
        }
        person = new Client();
        try {
            person.setId(IdGenerator.idGenerator());
        } catch (LogicException e) {
            throw new CommandException(e.getMessage(), e);
        }
        person.setLogin(login);
        person.setPassword(pass);
        person.setFirstName(firstName);
        person.setSecondName(secondName);
        ((Client) person).setAdress(adress);

        try {
            if (RegisterLogic.checkRegister(person)) {
                HttpSession session = request.getSession();
                session.setAttribute(PARAM_NAME_ROLE, PARAM_NAME_USER);
                session.setAttribute(PARAM_NAME_USERID, person.getId());
                session.setAttribute(PARAM_NAME_USER_LOGIN, person.getLogin());
                session.setAttribute(PARAM_NAME_USER, person.getFirstName() + " " + person.getSecondName());
                page = ConfigurationManager.getProperty("path.page.starter");
            } else {
                request.setAttribute(ERROR_REGISTER_MESSAGE, MessageManager.getProperty("message.registererror"));
                request.setAttribute(REGISTER_MESSAGE, "true");
                page = ConfigurationManager.getProperty("path.page.starter");
            }
        } catch (LogicException e) {
            throw new CommandException("Exception during register process", e);
        }

        return page;

    }
}
