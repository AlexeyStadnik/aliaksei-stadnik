package by.stadnik.epam.command;

import by.stadnik.epam.logic.LoginLogic;
import by.stadnik.epam.logic.exception.LogicException;
import by.stadnik.epam.logic.model.Person;
import by.stadnik.epam.resource.ConfigurationManager;
import by.stadnik.epam.resource.MessageManager;
import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Command for loging into application
 * Finds user by login and password and then checks is user either administrator or client
 */
public class LoginCommand implements ActionCommand {

    public static final String PARAM_NAME_LOGIN = "login";
    public static final String PARAM_NAME_LOGIN_OR_SIGN_UP = "loginOrSignUp";
    public static final String PARAM_NAME_LOGIN_MESSAGE = "loginMessage";
    public static final String PARAM_NAME_ROLE = "role";
    public static final String PARAM_NAME_USER = "user";
    public static final String PARAM_NAME_USERID = "userId";
    public static final String PARAM_NAME_USER_LOGIN = "userLogin";
    public static final String PARAM_NAME_PASSWORD = "password";

    public static final String PARAM_NAME_ADMIN = "admin";

    public static final String ERROR_LOGIN_MESSAGE = "errorLoginPassMessage";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {


        if (PARAM_NAME_LOGIN.equals(request.getParameter(PARAM_NAME_LOGIN_OR_SIGN_UP))) {
            request.setAttribute(PARAM_NAME_LOGIN_MESSAGE, "true");
            return ConfigurationManager.getProperty("path.page.starter");
        }


        String page = null;
        Person person = null;

        String login = request.getParameter(PARAM_NAME_LOGIN);  // извлечение из запроса логина и пароля
        String pass = request.getParameter(PARAM_NAME_PASSWORD);
        try {
            person = LoginLogic.checkLogin(login, pass);
        } catch (LogicException e) {
            throw new CommandException("Exception during login process", e);
        }

        if (person != null && person.getStatus() == null) {      // проверка логина и пароля


            HttpSession session = request.getSession();
            session.setAttribute(PARAM_NAME_ROLE, PARAM_NAME_USER);
            session.setAttribute(PARAM_NAME_USERID, person.getId());
            session.setAttribute(PARAM_NAME_USER_LOGIN, person.getLogin());
            session.setAttribute(PARAM_NAME_USER, person.getFirstName() + " " + person.getSecondName());
            page = ConfigurationManager.getProperty("path.page.starter");   // определение пути к main.jsp

        }
        if (person != null && PARAM_NAME_ADMIN.equals(person.getStatus())) {
            HttpSession session = request.getSession();
            session.setAttribute(PARAM_NAME_ROLE, PARAM_NAME_ADMIN);
            session.setAttribute(PARAM_NAME_USER, person.getFirstName() + " " + person.getSecondName());
            page = ConfigurationManager.getProperty("path.page.starter");
        }
        if (person == null) {
            request.setAttribute(ERROR_LOGIN_MESSAGE, MessageManager.getProperty("message.loginerror"));
            request.setAttribute(PARAM_NAME_LOGIN_MESSAGE, "true");
            page = ConfigurationManager.getProperty("path.page.starter");
        }
        return page;
    }
}
