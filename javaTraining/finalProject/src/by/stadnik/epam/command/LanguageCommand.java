package by.stadnik.epam.command;

import by.stadnik.epam.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;

/**
 * Command for changing language of the application.
 * Changes system locale and session attribute Locale
 */
public class LanguageCommand implements ActionCommand {

    public static final String PARAM_ENGLISH = "english";
    public static final String PARAM_LANGUAGE = "language";
    public static final String ATTR_LOCALE = "Locale";
    public static final String PARAM_EN = "en";
    public static final String PARAM_RU = "ru";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        HttpSession session = request.getSession();

        if (PARAM_ENGLISH.equals(request.getParameter(PARAM_LANGUAGE))) {
            session.setAttribute(ATTR_LOCALE, PARAM_EN);
            Locale.setDefault(new Locale(PARAM_EN));
        } else {
            session.setAttribute(ATTR_LOCALE, PARAM_RU);
            Locale.setDefault(new Locale(PARAM_RU));
        }


        return ConfigurationManager.getProperty("path.page.starter");
    }
}
