package by.stadnik.epam.command.factory;

import by.stadnik.epam.command.ActionCommand;
import by.stadnik.epam.command.EmptyCommand;
import by.stadnik.epam.command.client.CommandEnum;
import by.stadnik.epam.resource.MessageManager;

import javax.servlet.http.HttpServletRequest;

public class ActionFactory {

    public static final String PARAM_COMMAND = "command";
    public static final String PARAM_WRONG_ACTION = "wrongAction";

    public ActionCommand defineCommand(HttpServletRequest request) {

        ActionCommand current = new EmptyCommand();
        String action = request.getParameter(PARAM_COMMAND);   // извлечение имени команды из запроса

        if (action == null || action.isEmpty()) {

            return current;   // если команда не задана в текущем запросе

        }

        try {

            CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase()); // получение объекта, соответствующего команде


            current = currentEnum.getCurrentCommand();

        } catch (IllegalArgumentException e) {

            request.setAttribute(PARAM_WRONG_ACTION, action + MessageManager.getProperty("message.wrongaction"));

        }
        return current;
    }
}
