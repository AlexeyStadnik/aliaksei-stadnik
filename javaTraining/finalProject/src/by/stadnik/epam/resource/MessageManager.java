package by.stadnik.epam.resource;

import java.util.Locale;
import java.util.ResourceBundle;

public class MessageManager {

    public static String getProperty(String key) {
        return ResourceBundle.getBundle("resources.messages", Locale.getDefault()).getString(key);
    }

}
