package by.stadnik.epam.resource;

import java.util.ResourceBundle;

public class ConfigurationManager {

    public static String getProperty(String key) {
        return ResourceBundle.getBundle("resources.config").getString(key);
    }
}
