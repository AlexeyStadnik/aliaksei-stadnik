package by.stadnik.epam.logic;

import by.stadnik.epam.dao.PersonDAO;
import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.logic.exception.LogicException;
import by.stadnik.epam.logic.model.Person;
import by.stadnik.epam.util.Validator;


/**
 * Created by Алексей on 01.04.2015.
 */
public class RegisterLogic {
    /**
     * Class for signing up in the system
     *
     * @param person all information about user
     * @return true if signed up successful, false if not
     * @throws LogicException
     */
    public static boolean checkRegister(Person person) throws LogicException {
        boolean flag = false;

        if (!Validator.validateSignUp(person)) {

        }


        try {
            flag = (new PersonDAO()).checkFreeLogin(person.getLogin());
            if (!flag) {
                return flag;
            }
        } catch (DAOException e) {
            throw new LogicException("Exception while adding user", e);
        }
        flag = false;
        try {
            flag = (new PersonDAO()).addClient(person);
        } catch (DAOException e) {
            throw new LogicException("User is not added, failed", e);
        }

        return flag;
    }
}
