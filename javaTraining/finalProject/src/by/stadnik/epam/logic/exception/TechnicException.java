package by.stadnik.epam.logic.exception;

/**
 * Created by Алексей on 22.03.2015.
 */
public class TechnicException extends Exception {

    public TechnicException() {
    }

    public TechnicException(String message) {
        super(message);
    }

    public TechnicException(String message, Throwable cause) {
        super(message, cause);
    }

    public TechnicException(Throwable cause) {
        super(cause);
    }

    public TechnicException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
