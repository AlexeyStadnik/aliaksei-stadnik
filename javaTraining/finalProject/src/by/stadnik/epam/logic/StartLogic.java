package by.stadnik.epam.logic;

import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.dao.FlightDAO;
import by.stadnik.epam.logic.exception.LogicException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Алексей on 26.04.2015.
 */
public class StartLogic {
    /**
     * Method for getting all destination for start of the application
     *
     * @return list of destinations
     * @throws LogicException
     */
    public static ArrayList<String> takeStartingFlightList() throws LogicException {

        try {
            FlightDAO flightDAO = new FlightDAO();
            ArrayList<String> flightList = flightDAO.takeFlightList();
            return flightList;
        } catch (DAOException e) {
            throw new LogicException("Failed getting flight-list! Check DB status", e);
        }

    }

}
