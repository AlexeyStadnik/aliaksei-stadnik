package by.stadnik.epam.logic;

import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.dao.PersonDAO;
import by.stadnik.epam.logic.exception.LogicException;
import by.stadnik.epam.util.Validator;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Алексей on 05.05.2015.
 */
public class UserLogic {
    /**
     * Method for updating user info
     *
     * @param userUpdateInfo hashMap with all info for updating
     * @return true if update was successful false if not
     * @throws LogicException
     */
    public static boolean updateUserInfo(HashMap<String, String> userUpdateInfo) throws LogicException {

        boolean flag = false;

        if (Validator.validateUpdateInfo(userUpdateInfo)) {
            try {
                flag = (new PersonDAO()).update(userUpdateInfo);
            } catch (DAOException e) {
                throw new LogicException("Exception during updating process", e);
            }
            return flag;

        } else {
            return false;
        }

    }

    /**
     * Method for getting new id for new users
     *
     * @return ID of the new user
     * @throws LogicException
     */
    public static int takeMaxId() throws LogicException {

        ArrayList<Integer> idList = null;
        try {
            idList = PersonDAO.takeMaxId();
        } catch (DAOException e) {
            throw new LogicException("Exception during getting user ID", e);
        }
        int maxId = 0;
        for (int x : idList) {
            if (x > maxId) {
                maxId = x;
            }
        }

        return maxId;
    }
}
