package by.stadnik.epam.logic.model;

import java.time.LocalDateTime;

/**
 * Created by Алексей on 22.03.2015.
 */
public class Flight extends Entity {

    private int id;
    private String arrivalPlace;
    private String departurePlace;
    private double flightPrice;
    private double luggagePrice;
    private double premiumPrice;
    private int capacity;
    private LocalDateTime departureTime;
    private LocalDateTime arrivalTime;


    public Flight() {
    }

    /**
     * @param id
     * @param arrivalPlace
     * @param departurePlace
     * @param flightPrice
     * @param luggagePrice
     * @param premiumPrice
     * @param capacity
     * @param departureTime
     * @param arrivalTime
     */

    public Flight(int id, String arrivalPlace, String departurePlace, double flightPrice, double luggagePrice, double premiumPrice, int capacity, LocalDateTime departureTime, LocalDateTime arrivalTime) {
        this.id = id;
        this.arrivalPlace = arrivalPlace;
        this.departurePlace = departurePlace;
        this.flightPrice = flightPrice;
        this.luggagePrice = luggagePrice;
        this.premiumPrice = premiumPrice;
        this.capacity = capacity;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getArrivalPlace() {
        return arrivalPlace;
    }

    public void setArrivalPlace(String arrivalPlace) {
        this.arrivalPlace = arrivalPlace;
    }

    public String getDeparturePlace() {
        return departurePlace;
    }

    public void setDeparturePlace(String depaturePlace) {
        this.departurePlace = depaturePlace;
    }

    public double getFlightPrice() {
        return flightPrice;
    }

    public void setFlightPrice(double flightPrice) {
        this.flightPrice = flightPrice;
    }

    public double getLuggagePrice() {
        return luggagePrice;
    }

    public void setLuggagePrice(double laguegePrice) {
        this.luggagePrice = laguegePrice;
    }

    public double getPremiumPrice() {
        return premiumPrice;
    }

    public void setPremiumPrice(double premiumPrice) {
        this.premiumPrice = premiumPrice;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public LocalDateTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalDateTime departureTime) {
        this.departureTime = departureTime;
    }

    public LocalDateTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(LocalDateTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getNormalFormatDepartureTime() {
        return departureTime.toString().replaceAll("T", " ");
    }

    public String getNormalFormatArrivalTime() {
        return arrivalTime.toString().replaceAll("T", " ");
    }


    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", arrivalPlace='" + arrivalPlace + '\'' +
                ", departurePlace='" + departurePlace + '\'' +
                ", flightPrice=" + flightPrice +
                ", luggagePrice=" + luggagePrice +
                ", premiumPrice=" + premiumPrice +
                ", capacity=" + capacity +
                ", departureTime=" + departureTime.toString().replaceAll("T", " ") +
                ", arrivalTime=" + arrivalTime.toString().replaceAll("T", " ") +
                '}';
    }
}
