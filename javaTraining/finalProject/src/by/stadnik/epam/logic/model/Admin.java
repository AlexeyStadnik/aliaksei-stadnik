package by.stadnik.epam.logic.model;


/**
 * Class Admin entity
 * Created by Алексей on 22.03.2015.
 *
 * @see by.stadnik.epam.logic.model.Person
 */
public class Admin extends Person {

    private String position;

    public Admin() {
    }

    public Admin(int id, String firstName, String secondName, String login, String password) {
        super(id, firstName, secondName, login, password);
    }

    /**
     * @param id         - id of the user
     * @param firstName
     * @param secondName
     * @param login
     * @param password
     * @param position   - position in the company
     */
    public Admin(int id, String firstName, String secondName, String login, String password, String position) {
        super(id, firstName, secondName, login, password);

        this.position = position;

    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {

        return super.toString() + "Admin{" +
                "position='" + position + '\'' +
                '}';
    }
}
