package by.stadnik.epam.logic.model;

import java.time.LocalDateTime;

/**
 * Class for purchasing ticket. Has all necessary information for ticket purchasing
 * Created by Алексей on 04.05.2015.
 */
public class ClientOnFlight extends Entity {

    private String name;
    private String arrivalPlace;
    private String departurePlace;
    private LocalDateTime departureTime;
    private LocalDateTime arrivalTime;
    private boolean luggagePrice;
    private boolean premiumPrice;

    public ClientOnFlight() {
    }

    /**
     * @param name
     * @param arrivalPlace
     * @param departurePlace
     * @param departureTime
     * @param arrivalTime
     * @param luggagePrice   - price for additional luggage
     * @param premiumPrice   - price for premium status
     */
    public ClientOnFlight(String name, String arrivalPlace, String departurePlace, LocalDateTime departureTime, LocalDateTime arrivalTime, boolean luggagePrice, boolean premiumPrice) {
        this.name = name;
        this.arrivalPlace = arrivalPlace;
        this.departurePlace = departurePlace;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.luggagePrice = luggagePrice;
        this.premiumPrice = premiumPrice;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArrivalPlace() {
        return arrivalPlace;
    }

    public void setArrivalPlace(String arrivalPlace) {
        this.arrivalPlace = arrivalPlace;
    }

    public String getDeparturePlace() {
        return departurePlace;
    }

    public void setDeparturePlace(String departurePlace) {
        this.departurePlace = departurePlace;
    }

    public LocalDateTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalDateTime departureTime) {
        this.departureTime = departureTime;
    }

    public LocalDateTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(LocalDateTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public boolean isLuggagePrice() {
        return luggagePrice;
    }

    public void setLuggagePrice(boolean luggagePrice) {
        this.luggagePrice = luggagePrice;
    }

    public boolean isPremiumPrice() {
        return premiumPrice;
    }

    public void setPremiumPrice(boolean premiumPrice) {
        this.premiumPrice = premiumPrice;
    }

    @Override
    public String toString() {
        return "ClientOnFlight{" +
                "name='" + name + '\'' +
                ", arrivalPlace='" + arrivalPlace + '\'' +
                ", departurePlace='" + departurePlace + '\'' +
                ", departureTime=" + departureTime +
                ", arrivalTime=" + arrivalTime +
                ", luggagePrice=" + luggagePrice +
                ", premiumPrice=" + premiumPrice +
                '}';
    }
}
