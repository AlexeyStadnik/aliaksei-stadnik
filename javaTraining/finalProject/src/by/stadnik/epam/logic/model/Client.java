package by.stadnik.epam.logic.model;


/**
 * Class Client entity
 * Created by Алексей on 22.03.2015.
 *
 * @see by.stadnik.epam.logic.model.Person
 */
public class Client extends Person {

    private String adress;
    private String clientStatus;
    private double money;

    public Client() {
    }

    /**
     * @param id
     * @param firstName
     * @param secondName
     * @param login      - user login
     * @param password
     * @param adress
     * @param status     - status of the user. basic or premium
     * @param money
     */
    public Client(int id, String firstName, String secondName, String login, String password, String adress, String status, double money) {
        super(id, firstName, secondName, login, password);
        this.adress = adress;
        this.clientStatus = status;
        this.money = money;
    }

    public String getClientStatus() {
        return clientStatus;
    }

    public void setClientStatus(String status) {
        this.clientStatus = status;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    @Override
    public String toString() {
        return super.toString() + "Client{" +
                "adress='" + adress + '\'' +
                ", clientStatus='" + clientStatus + '\'' +
                '}';
    }


}

