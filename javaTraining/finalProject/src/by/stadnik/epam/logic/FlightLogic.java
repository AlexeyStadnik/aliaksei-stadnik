package by.stadnik.epam.logic;

import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.dao.FlightDAO;
import by.stadnik.epam.logic.exception.LogicException;
import by.stadnik.epam.logic.model.ClientOnFlight;
import by.stadnik.epam.logic.model.Flight;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Алексей on 02.05.2015.
 */
public class FlightLogic {

    private static final String PARAM_NAME_FLIGHT_DATE = "flightDate";

    /**
     * Selects flights according info form hashMap
     *
     * @param flightInformation - hashMap with all info
     * @return list of flights
     * @throws LogicException
     * @see by.stadnik.epam.logic.model.Flight
     */
    public static ArrayList<Flight> selectFlightList(HashMap<String, String> flightInformation) throws LogicException {
        try {
            ArrayList<Flight> flightList = (new FlightDAO()).takeFlightList(flightInformation);

            return FlightLogic.dateChecker(flightList, flightInformation.get(PARAM_NAME_FLIGHT_DATE));
        } catch (DAOException e) {
            throw new LogicException("exception during getting flight list", e);
        }
    }

    /**
     * Method for purchasing new flight
     *
     * @param flightInformation - hashMap with all info about purchased flight
     * @return true if flight was purchased, false if not
     * @throws LogicException
     */
    public static boolean purchaseConfirmation(HashMap<String, String> flightInformation) throws LogicException {

        boolean purchaseStatus = false;
        try {
            purchaseStatus = (new FlightDAO()).purchaseFinalConfirmation(flightInformation);
        } catch (DAOException e) {
            throw new LogicException(e);
        }

        return purchaseStatus;
    }

    /**
     * Method for getting all user flights
     *
     * @param userID id of the user whose flights we are searching for
     * @return list of flights
     * @throws LogicException
     * @see by.stadnik.epam.logic.model.ClientOnFlight - special class with all info about purchased flight
     */
    public static ArrayList<ClientOnFlight> selectFlightListByUserID(int userID) throws LogicException {

        ArrayList<ClientOnFlight> flightList = null;
        try {
            flightList = (new FlightDAO().takeFlightByUserID(userID));
        } catch (DAOException e) {
            throw new LogicException(e);
        }
        return flightList;
    }

    /**
     * Class for checking if the date of the flight suits user's dates.
     * Flights are geted with diapason +- 1 month from user's requirements
     *
     * @param flightList
     * @param date       - user date
     * @return - new flight list (delete from it flights with inappropriate date )
     */
    public static ArrayList<Flight> dateChecker(ArrayList<Flight> flightList, String date) {
        date = date + " 00:00";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(date, formatter);

        for (int i = 0; i < flightList.size(); i++) {

            LocalDateTime dateTimePlus = dateTime.plusMonths(1);

            LocalDateTime dateTimeMinus = dateTime.minusMonths(1);
            if (dateTimeMinus.compareTo(LocalDateTime.now()) < 0) {
                dateTimeMinus = LocalDateTime.now();
            }


            if ((flightList.get(i).getDepartureTime().compareTo(dateTimePlus) >= 1) || (flightList.get(i).getDepartureTime().compareTo(dateTimeMinus) < 0)) {
                flightList.remove(i);
            }
        }

        return flightList;
    }
}
