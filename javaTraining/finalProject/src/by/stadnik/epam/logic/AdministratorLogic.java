package by.stadnik.epam.logic;

import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.dao.FlightDAO;
import by.stadnik.epam.dao.PersonDAO;
import by.stadnik.epam.logic.exception.LogicException;
import by.stadnik.epam.logic.model.Client;
import by.stadnik.epam.logic.model.Flight;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAmount;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class with all administrators methods
 * Created by Алексей on 09.05.2015.
 */
public class AdministratorLogic {

    public static final String PARAM_NAME_DEPARTURE = "departurePlace";
    public static final String PARAM_NAME_ARRIVAL = "arrivalPlace";
    public static final String PARAM_NAME_DEPARTURE_DATE = "departureDate";
    public static final String PARAM_NAME_DEPARTURE_TIME = "departureTime";
    public static final String PARAM_NAME_DURATION_TIME = "durationTime";
    public static final String PARAM_FLIGHT_PRICE = "ticketPrice";
    public static final String PARAM_LUGGAGE_PRICE = "luggagePrice";
    public static final String PARAM_PREMIUM_PRICE = "premiumPrice";
    public static final String PARAM_NAME_CAPACITY = "capacity";

    /**
     * Method for updating flight info
     *
     * @param flight object with new information
     * @return boolean flag. true if update , false if not
     * @throws LogicException
     */

    public static boolean updateFlightInfo(Flight flight) throws LogicException {

        boolean flag = false;

        try {
            flag = (new FlightDAO()).update(flight);
        } catch (DAOException e) {
            throw new LogicException("Exception during update process, please check input info or DB connection", e);
        }

        return flag;
    }

    /**
     * Method for deleting flight
     *
     * @param updatedFlight flight that would be deleted
     * @return true if delete , false if not
     * @throws LogicException
     */

    public static boolean deleteFlight(Flight updatedFlight) throws LogicException {
        boolean flag = false;

        try {
            flag = (new FlightDAO()).delete(updatedFlight);
        } catch (DAOException e) {
            throw new LogicException("Exception during flight deleting process (remember that you can only delete empty flights)", e);
        }

        return flag;
    }

    /**
     * Gets all user which have already in application DB
     *
     * @return List of Clients
     * @throws LogicException
     */

    public static ArrayList<Client> takeUserList() throws LogicException {

        ArrayList<Client> userList = null;

        try {
            userList = (new PersonDAO()).selectUserList();
        } catch (DAOException e) {
            throw new LogicException("Exception during getting user list", e);
        }

        return userList;
    }

    /**
     * Method for adding new destinations
     *
     * @param destination
     * @return true if added, false if not
     * @throws LogicException
     */

    public static boolean addDestination(String destination) throws LogicException {

        boolean flag = false;
        try {
            flag = (new FlightDAO()).addDestination(destination);
        } catch (DAOException e) {
            throw new LogicException("Exception during adding new destination", e);
        }
        return flag;
    }

    /**
     * Method for adding new flight
     * HashMap includes all necessary information for adding
     *
     * @param addFlightInfo - hashMap with all information about new flight
     * @return boolean flag
     * @throws LogicException
     */
    public static boolean addFlight(HashMap<String, String> addFlightInfo) throws LogicException {

        boolean flag = false;


        String departurePlace = addFlightInfo.get(PARAM_NAME_DEPARTURE);
        String arrivalPlace = addFlightInfo.get(PARAM_NAME_ARRIVAL);
        String departureDate = addFlightInfo.get(PARAM_NAME_DEPARTURE_DATE);
        String departureTime = addFlightInfo.get(PARAM_NAME_DEPARTURE_TIME);
        String durationTime = addFlightInfo.get(PARAM_NAME_DURATION_TIME);
        String flightPrice = addFlightInfo.get(PARAM_FLIGHT_PRICE);
        String luggagePrice = addFlightInfo.get(PARAM_LUGGAGE_PRICE);
        String premiumPrice = addFlightInfo.get(PARAM_PREMIUM_PRICE);
        String capacity = addFlightInfo.get(PARAM_NAME_CAPACITY);


        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime departureDateTime = LocalDateTime.parse(departureDate + " " + departureTime, formatter);


        formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime durationDateTime = LocalDateTime.parse(departureDate + " " + durationTime, formatter);

        LocalDateTime arrivalTime = departureDateTime.plusHours(durationDateTime.getHour());
        arrivalTime = arrivalTime.plusMinutes(durationDateTime.getMinute());


        Flight flight = new Flight(
                0,
                arrivalPlace,
                departurePlace,
                Double.parseDouble(flightPrice),
                Double.parseDouble(luggagePrice),
                Double.parseDouble(premiumPrice),
                Integer.parseInt(capacity),
                departureDateTime,
                arrivalTime


        );

        try {
            flag = (new FlightDAO()).addFlight(flight);
        } catch (DAOException e) {
            throw new LogicException("Exception during flight add process", e);
        }
        return flag;
    }
}
