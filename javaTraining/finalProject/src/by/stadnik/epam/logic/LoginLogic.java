package by.stadnik.epam.logic;

import by.stadnik.epam.dao.PersonDAO;
import by.stadnik.epam.dao.DAOException;
import by.stadnik.epam.logic.exception.LogicException;
import by.stadnik.epam.logic.model.Person;

public class LoginLogic {
    /**
     * Method for log in in system
     * Checks login and password
     *
     * @param enterLogin
     * @param enterPass
     * @return Person  with all info about logined user
     * @throws LogicException
     * @see by.stadnik.epam.logic.model.Person
     */
    public static Person checkLogin(String enterLogin, String enterPass) throws LogicException {
        Person person = null;
        try {
            person = (new PersonDAO()).findPersonByLogin(enterLogin, enterPass);
        } catch (DAOException e) {
            throw new LogicException("Exception while trying login", e);
        }
        return person;

    }
}
