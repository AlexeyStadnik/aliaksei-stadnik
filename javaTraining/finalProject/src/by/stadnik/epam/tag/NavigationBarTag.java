package by.stadnik.epam.tag;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * User tag
 * Class helps to decrease amount of pages in the application
 * All kinds of users on the start page see different navigation bar
 * Provide multi functional page
 */
public class NavigationBarTag extends TagSupport {
    @Override
    public int doStartTag() throws JspException {


        String role = (String) pageContext.getSession().getAttribute("role");
        String ref = null;
        Locale locale = new Locale("RU");

        if ("en".equals(pageContext.getSession().getAttribute("Locale"))) {
            locale = new Locale("EN");
        }

        ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.pagecontent", locale);

        String home = resourceBundle.getString("button.home");
        String login = resourceBundle.getString("button.login");
        String signup = resourceBundle.getString("button.signup");
        String contacts = resourceBundle.getString("button.contacts");

        String buyTickets = resourceBundle.getString("button.buytickets");
        String profile = resourceBundle.getString("button.myprofile");
        String logout = resourceBundle.getString("button.logout");

        String adminZone = resourceBundle.getString("button.adminZone");
        String buyFlights = resourceBundle.getString("button.findFlight");


        if ("user".equals(role)) {
            ref = "<li><a id=\"homehref\"href=\"javascript:void(0);\">" + buyTickets + "</a></li> " +
                    "<li><a id=\"profilehref\"href=\"controller?command=userProfileForward\">" + profile + "</a></li> " +
                    " <li><a id=\"logouthref\"href=\"controller?command=logout\">" + logout + "</a></li> " +
                    "<li><a href=\"controller?command=CONTACTS\">" + contacts + "</a></li>";
        } else if ("admin".equals(role)) {
            ref = "<li><a id=\"homehref\"href=\"javascript:void(0);\">" + buyFlights + "</a></li> " +
                    "<li><a id=\"profilehref\"href=\"controller?command=adminProfileForward\">" + adminZone + "</a></li> " +
                    " <li><a id=\"logouthref\"href=\"controller?command=logout\">" + logout + "</a></li> " +
                    "<li><a href=\"controller?command=CONTACTS\">" + contacts + "</a></li>";

        } else {

            ref = "<li><a id=\"homehref\"href=\"javascript:void(0);\">" + home + "</a></li> " +
                    "<li><a id=\"loginhref\"href=\"javascript:void(0);\">" + login + "</a></li> " +
                    " <li><a id=\"registerhref\"href=\"javascript:void(0);\">" + signup + "</a></li> " +
                    "<li><a href=\"controller?command=CONTACTS\">" + contacts + "</a></li>";

        }
        try {
            JspWriter out = pageContext.getOut();
            out.write(ref);
        } catch (IOException e) {
            throw new JspException("Exception in the custom tug", e);
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}