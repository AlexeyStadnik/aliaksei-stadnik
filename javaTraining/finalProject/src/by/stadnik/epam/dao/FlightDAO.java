package by.stadnik.epam.dao;

import by.stadnik.epam.connection_pool.ConnectionPool;
import by.stadnik.epam.logic.model.ClientOnFlight;
import by.stadnik.epam.logic.model.Flight;
import by.stadnik.epam.logic.model.Person;
import org.apache.log4j.Logger;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Class with all DAO methods connecting with flights processes.
 *
 * @author Alex Stadnik
 * @see by.stadnik.epam.dao.AbstractDAO
 */


public class FlightDAO extends AbstractDAO<Integer, Flight> {

    private static final Logger LOG = Logger.getLogger(FlightDAO.class);


    public static final String PARAM_PLACE_NAME = "place_name";
    public static final String PARAM_DEPARTURE_PLACE = "departurePlace";
    public static final String PARAM_ARRIVAL_PLACE = "arrivalPlace";
    public static final String PARAM_FLIGHT_ID = "flight_id";
    public static final String PARAM_TICKET_PRICE = "ticket_price";
    public static final String PARAM_LUGGAGE_PRICE = "luggage_price";
    public static final String PARAM_PREMIUM_PRICE = "premium_price";
    public static final String PARAM_CAPACITY = "capacity";
    public static final String PARAM_ARRIVAL_TIME = "arrival_time";
    public static final String PARAM_DEPARTURE_TIME = "departure_time";
    public static final String PARAM_FLIGHTID = "flightId";
    public static final String PARAM_CLIENT_TICKET_AMOUNT = "clientTicketAmount";
    public static final String PARAM_USERID = "userId";
    public static final String PARAM_CLIENT_NAME = "clientName";
    public static final String PARAM_ADDITIONAL_LUGGAGE = "additionalLuggage";
    public static final String PARAM_PREMIUMSTATUS = "premiumStatus";

    public static final String PARAM_PASSENGER_NAME = "passenger_name";
    public static final String PARAM_DEPARTURE_PLACE_NAME = "departure_place_name";
    public static final String PARAM_ARRIVAL_PLACE_NAME = "arrival_place_name";
    public static final String PARAM_AMOUNT_OF_LUGGAGE = "amount_of_laguege";
    public static final String PARAM_PREMIUM_STATUS = "premium_status";

    public static final String SELECT_PLACE_LIST = "SELECT place_name FROM places";
    public static final String SELECT_FLIGHT = "SELECT * FROM flight_info WHERE flight_id=?";
    public static final String SELECT_FLIGHT_BY_USER_ID = "SELECT flight_ticket_id,passenger_name,amount_of_laguege,premium_status,flight_info.departure_time,flight_info.arrival_time,C1.place_name AS departure_place_name,C2.place_name AS arrival_place_name FROM client_on_flight JOIN flight_info ON flight_ticket_id = flight_id INNER JOIN places AS C1 ON flight_info.departure_place = C1.place_id INNER JOIN places AS C2 ON flight_info.arrival_place = C2.place_id WHERE client_ticket_id=?";
    public static final String SELECT_FLIGHT_LIST = "SELECT * FROM flight_info WHERE flight_info.departure_place IN (SELECT place_id FROM places WHERE places.place_name =?) AND flight_info.arrival_place IN (SELECT place_id FROM places WHERE place_name =?)";
    public static final String INSERT_NEW_INTO_CLIENT_ON_FLIGHT = "INSERT INTO client_on_flight VALUES(?,?,?,?,?)";
    public static final String UPDATE_CAPACITY = "UPDATE flight_info SET capacity=? WHERE  flight_id=? ";
    public static final String UPDATE_FLIGHT_INFO = "UPDATE flight_info SET ticket_price=?, luggage_price=?, premium_price=? WHERE  flight_id=? ";
    public static final String DELETE_FLIGHT = "DELETE FROM flight_info WHERE  flight_id=?";
    public static final String SELECT_CLIENT_ON_FLIGHT = "SELECT * FROM client_on_flight WHERE flight_ticket_id=?";
    public static final String INSERT_NEW_DESTINATION = "INSERT INTO places (place_name) VALUES (?);";
    public static final String INSERT_NEW_FLIGHT = "INSERT INTO flight_info (departure_place, arrival_place, arrival_time, departure_time, capacity, ticket_price, luggage_price, premium_price) VALUES ((SELECT place_id FROM places WHERE place_name=?), (SELECT place_id FROM places WHERE place_name=?), ?, ?, ?, ?,?,?)";


    @Override
    public boolean delete(Integer id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean update(HashMap<String, String> hashMap) throws DAOException {
        throw new UnsupportedOperationException();
    }

    /**
     * Deletes flight which was passed as a parameter
     * First check is there any clients who has already bought tickets for the flight.
     *
     * @param flight flight with all information abut it.
     * @return true if the flight was deleted successful, false if not
     * @see Flight
     */

    @Override
    public boolean delete(Flight flight) throws DAOException {
        boolean flag = false;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            try {
                connection = ConnectionPool.getInstance().retrieve();
                preparedStatement = connection.prepareStatement(SELECT_CLIENT_ON_FLIGHT);
                preparedStatement.setInt(1, flight.getId());
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.isBeforeFirst()) {
                    return false;
                }
            } catch (SQLException e) {
                throw new DAOException("SQLException during flight update process,please check DB connection", e);
            }

            try {
                connection = ConnectionPool.getInstance().retrieve();
                preparedStatement = connection.prepareStatement(DELETE_FLIGHT);
                preparedStatement.setInt(1, flight.getId());
                preparedStatement.execute();
                flag = true;
            } catch (SQLException e) {
                throw new DAOException("SQLException during flight update process,please check DB connection", e);
            }
        } finally {
            FlightDAO.close(preparedStatement);
            ConnectionPool.getInstance().putback(connection);
        }

        return flag;

    }

    /**
     * Updates flight information.
     *
     * @param flight flight with all information abut it.
     * @return true if the flight was updated successful, false if not
     * @see Flight
     */

    @Override
    public boolean update(Flight flight) throws DAOException {
        boolean flag = false;
        Connection connection = null;
        PreparedStatement preparedStatement = null;


        try {
            connection = ConnectionPool.getInstance().retrieve();
            preparedStatement = connection.prepareStatement(UPDATE_FLIGHT_INFO);
            preparedStatement.setDouble(1, flight.getFlightPrice());
            preparedStatement.setDouble(2, flight.getLuggagePrice());
            preparedStatement.setDouble(3, flight.getPremiumPrice());
            preparedStatement.setInt(4, flight.getId());
            preparedStatement.execute();
            flag = true;
        } catch (SQLException e) {
            throw new DAOException("SQLException during flight update process,please check DB connection", e);
        } finally {
            FlightDAO.close(preparedStatement);
            ConnectionPool.getInstance().putback(connection);
        }

        return flag;
    }

    /**
     * Takes destination list.
     *
     * @return ArrayList of destinations
     * @throws DAOException
     */

    public ArrayList<String> takeFlightList() throws DAOException {
        ArrayList<String> flightList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().retrieve();
            preparedStatement = connection.prepareStatement(SELECT_PLACE_LIST);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                flightList.add(resultSet.getString(PARAM_PLACE_NAME));
            }
            return flightList;
        } catch (SQLException e) {
            throw new DAOException("Failed getting flight list", e);
        } finally {
            FlightDAO.close(preparedStatement);
            ConnectionPool.getInstance().putback(connection);
        }


    }

    /**
     * Selects all flights according params in hashMap
     *
     * @param flightInformation - hashMap where all params are
     * @return ArrayList of Flights . If there are no such flights return empty list
     * @see Flight
     */

    public ArrayList<Flight> takeFlightList(HashMap<String, String> flightInformation) throws DAOException {
        ArrayList<Flight> flightList = new ArrayList<Flight>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().retrieve();
            preparedStatement = connection.prepareStatement(SELECT_FLIGHT_LIST);
            preparedStatement.setString(1, flightInformation.get(PARAM_DEPARTURE_PLACE));
            preparedStatement.setString(2, flightInformation.get(PARAM_ARRIVAL_PLACE));
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                flightList.add(
                        new Flight(
                                resultSet.getInt(PARAM_FLIGHT_ID),
                                flightInformation.get(PARAM_DEPARTURE_PLACE),
                                flightInformation.get(PARAM_ARRIVAL_PLACE),
                                resultSet.getDouble(PARAM_TICKET_PRICE),
                                resultSet.getDouble(PARAM_LUGGAGE_PRICE),
                                resultSet.getDouble(PARAM_PREMIUM_PRICE),
                                resultSet.getInt(PARAM_CAPACITY),
                                resultSet.getTimestamp(PARAM_ARRIVAL_TIME).toLocalDateTime(),
                                resultSet.getTimestamp(PARAM_DEPARTURE_TIME).toLocalDateTime()
                        )
                );
            }

            return flightList;
        } catch (SQLException e) {
            throw new DAOException("Exception during finding flight process, check input information or data base connection", e);
        } finally {
            FlightDAO.close(preparedStatement);
            ConnectionPool.getInstance().putback(connection);
        }

    }

    /**
     * Adds to data base information about purchased ticket.
     *
     * @param flightInformation - hashMap where all params are
     * @return true if process passed successful, false if not
     */

    public boolean purchaseFinalConfirmation(HashMap<String, String> flightInformation) throws DAOException {

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = ConnectionPool.getInstance().retrieve();
            preparedStatement = connection.prepareStatement(SELECT_FLIGHT);
            preparedStatement.setString(1, flightInformation.get(PARAM_FLIGHTID));
            ResultSet resultSet = preparedStatement.executeQuery();
            int updatedCapacity = 0;
            while (resultSet.next()) {
                if (Integer.parseInt(flightInformation.get(PARAM_CLIENT_TICKET_AMOUNT)) > resultSet.getInt(PARAM_CAPACITY)) {
                    return false;
                }
                updatedCapacity = resultSet.getInt(PARAM_CAPACITY) - Integer.parseInt(flightInformation.get(PARAM_CLIENT_TICKET_AMOUNT));

            }

            for (int i = 1; i <= Integer.parseInt(flightInformation.get(PARAM_CLIENT_TICKET_AMOUNT)); i++) {

                preparedStatement = connection.prepareStatement(INSERT_NEW_INTO_CLIENT_ON_FLIGHT);
                preparedStatement.setInt(1, Integer.parseInt(flightInformation.get(PARAM_USERID)));
                preparedStatement.setInt(2, Integer.parseInt(flightInformation.get(PARAM_FLIGHTID)));
                preparedStatement.setString(3, flightInformation.get(PARAM_CLIENT_NAME + i));
                if ("true".equals(flightInformation.get(PARAM_ADDITIONAL_LUGGAGE + i))) {
                    preparedStatement.setString(4, "true");
                } else {
                    preparedStatement.setString(4, "false");
                }

                if ("true".equals(flightInformation.get(PARAM_PREMIUMSTATUS + i))) {
                    preparedStatement.setString(5, "true");
                } else {
                    preparedStatement.setString(5, "false");
                }

                preparedStatement.execute(); // here exception


            }

            preparedStatement = connection.prepareStatement(UPDATE_CAPACITY);
            preparedStatement.setInt(1, updatedCapacity);
            preparedStatement.setInt(2, Integer.parseInt(flightInformation.get(PARAM_FLIGHTID)));
            preparedStatement.execute();


            return true;

        } catch (SQLException e) {
            throw new DAOException("Exception during flight confirmation, please check input data or DB connection", e);
        } finally {
            FlightDAO.close(preparedStatement);
            ConnectionPool.getInstance().putback(connection);
        }


    }

    /**
     * Selects all flights by user id.
     *
     * @param userID - user ID by which flights would be found.
     * @return list of flights. If there are no flights return empty list.
     */

    public ArrayList<ClientOnFlight> takeFlightByUserID(int userID) throws DAOException {
        ArrayList<ClientOnFlight> flightList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().retrieve();
            preparedStatement = connection.prepareStatement(SELECT_FLIGHT_BY_USER_ID);
            preparedStatement.setInt(1, userID);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {


                flightList.add(
                        new ClientOnFlight(
                                resultSet.getString(PARAM_PASSENGER_NAME),
                                resultSet.getString(PARAM_DEPARTURE_PLACE_NAME),
                                resultSet.getString(PARAM_ARRIVAL_PLACE_NAME),
                                resultSet.getTimestamp(PARAM_DEPARTURE_TIME).toLocalDateTime(),
                                resultSet.getTimestamp(PARAM_ARRIVAL_TIME).toLocalDateTime(),
                                Boolean.valueOf(resultSet.getString(PARAM_AMOUNT_OF_LUGGAGE)),
                                Boolean.valueOf(resultSet.getString(PARAM_PREMIUM_STATUS))
                        )
                );
            }

            return flightList;
        } catch (SQLException e) {
            throw new DAOException("Exception during finding flight process, check input information or data base connection", e);
        } finally {
            FlightDAO.close(preparedStatement);
            ConnectionPool.getInstance().putback(connection);
        }
    }

    /**
     * Adds new destination.
     * If there are already such destination return false.
     *
     * @param destination - destination which would be added.
     * @return true if destination was added false if not
     */

    public boolean addDestination(String destination) throws DAOException {

        Connection connection = null;
        PreparedStatement preparedStatement = null;


        try {
            connection = ConnectionPool.getInstance().retrieve();
            preparedStatement = connection.prepareStatement(INSERT_NEW_DESTINATION);
            preparedStatement.setString(1, destination);
            preparedStatement.execute();
        } catch (SQLException e) {
            LOG.error("Exception while adding new destination, such destination is already exist", e);
            return false;
        } finally {
            FlightDAO.close(preparedStatement);
            ConnectionPool.getInstance().putback(connection);
        }

        return true;

    }

    /**
     * Adds new flight to the DB.
     *
     * @param flight - Flight with all params which would be added.
     * @return true if flight was added.
     * @throws by.stadnik.epam.dao.DAOException
     * @see Flight
     */

    public boolean addFlight(Flight flight) throws DAOException {

        Connection connection = null;
        PreparedStatement preparedStatement = null;


        try {
            connection = ConnectionPool.getInstance().retrieve();
            preparedStatement = connection.prepareStatement(INSERT_NEW_FLIGHT);
            preparedStatement.setString(1, flight.getDeparturePlace());
            preparedStatement.setString(2, flight.getArrivalPlace());
            preparedStatement.setTimestamp(3, Timestamp.valueOf(flight.getDepartureTime()));
            preparedStatement.setTimestamp(4, Timestamp.valueOf(flight.getArrivalTime()));
            preparedStatement.setInt(5, flight.getCapacity());
            preparedStatement.setDouble(6, flight.getFlightPrice());
            preparedStatement.setDouble(7, flight.getLuggagePrice());
            preparedStatement.setDouble(8, flight.getPremiumPrice());
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DAOException("Exception during new flight add process", e);
        } finally {
            FlightDAO.close(preparedStatement);
            ConnectionPool.getInstance().putback(connection);
        }

        return true;

    }
}
