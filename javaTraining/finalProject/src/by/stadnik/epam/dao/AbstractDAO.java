package by.stadnik.epam.dao;


import by.stadnik.epam.logic.model.Entity;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;

/**
 * Relative class for all DAO classes.
 *
 * @author Alex Stadnik
 * @see by.stadnik.epam.dao.PersonDAO
 * @see by.stadnik.epam.dao.FlightDAO
 */

public abstract class AbstractDAO <K, T extends Entity> {

    private static final Logger LOG = Logger.getLogger(AbstractDAO.class);

    public abstract boolean delete(K id);
    public abstract boolean delete(T entity) throws DAOException;
    public abstract boolean update(T entity) throws DAOException;
    public abstract boolean update(HashMap<String,String> hashMap) throws DAOException;

    /**
     * Method for statement closing.
     *
     * @param statement - Statement which would be closed
     *
     */

    public static void close(Statement statement) {
        if (statement != null) {
            try {
                statement.close();

            } catch (SQLException e) {
               LOG.error("exception during statement closing",e);
            }
        }
    }


}
