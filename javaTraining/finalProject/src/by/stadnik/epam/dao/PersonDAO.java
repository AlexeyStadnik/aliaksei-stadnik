package by.stadnik.epam.dao;

import by.stadnik.epam.connection_pool.ConnectionPool;
import by.stadnik.epam.logic.model.Admin;
import by.stadnik.epam.logic.model.Client;
import by.stadnik.epam.logic.model.Person;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Class with all DAO methods connecting with person processes.
 *
 * @author Alex Stadnik
 * @see by.stadnik.epam.dao.AbstractDAO
 * @see by.stadnik.epam.logic.model.Person
 */
public class PersonDAO extends AbstractDAO<Integer, Person> {

    private static final Logger LOG = Logger.getLogger(PersonDAO.class);

    public static final String PARAM_ADMIN = "admin";
    public static final String PARAM_PERSON_STATUS = "status";
    public static final String PARAM_PERSON_ID = "person_id";
    public static final String PARAM_PERSON_LOGIN = "login";
    public static final String PARAM_PERSON_PASSWORD = "password";
    public static final String PARAM_PERSON_FIRST_NAME = "first_name";
    public static final String PARAM_PERSON_LAST_NAME = "last_name";
    public static final String PARAM_PERSON_POSITION = "position";
    public static final String PARAM_PERSON_ADDRESS = "adress";
    public static final String PARAM_CLIENT = "client";
    public static final String PARAM_BASIC = "basic";
    public static final String PARAM_CONFIDENT = "confident";
    public static final double PARAM_MONEY = 0;


    public static final String PARAM_USER_ID = "userId";
    public static final String PARAM_USER_LOGIN = "login";
    public static final String PARAM_USER_NEW_PASSWORD = "newPassword";


    public static final String TAKE_ID = "SELECT person_id FROM login_info";
    public static final String CHECK_FREE_LOGIN = "SELECT person_id FROM login_info WHERE Login=?";
    public static final String CHECK_LOGIN_PERSON = "SELECT * FROM login_info WHERE Login=? AND Password=md5(?)";
    public static final String FIND_ADMIN = "SELECT * FROM administrator WHERE administrator_id = ? ";
    public static final String FIND_CLIENT = "SELECT * FROM client WHERE client_id = ? ";
    public static final String INSERT_NEW_INTO_LOGIN = "INSERT INTO login_info VALUES(?,?,md5(?),?)";
    public static final String INSERT_NEW_INTO_CLIENT = "INSERT INTO client VALUES(?,?,?,?,?,?)";
    public static final String UPDATE_USER_LOGIN = "UPDATE login_info SET login=? WHERE  person_id=? ";
    public static final String UPDATE_USER_PASSWORD = "UPDATE login_info SET password=md5(?) WHERE  person_id=? ";
    public static final String SELECT_USER_LIST = "SELECT * FROM client JOIN login_info ON client.client_id=login_info.person_id";


    @Override
    public boolean delete(Integer id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(Person entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean update(Person entity) {
        throw new UnsupportedOperationException();
    }

    /**
     * Checks if login is free.
     * Checks if resultSet is empty.
     *
     * @param login
     * @return true if login is free, false if not
     * @throws DAOException
     */

    public boolean checkFreeLogin(String login) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().retrieve();
            preparedStatement = connection.prepareStatement(CHECK_FREE_LOGIN);
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.isBeforeFirst()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while checking if the login is free", e);
        } finally {
            PersonDAO.close(preparedStatement);
            ConnectionPool.getInstance().putback(connection);
        }

    }

    /**
     * Finds all person information in DB by login and password
     *
     * @param login    person login
     * @param password person password
     * @return Person object
     * @throws DAOException
     * @see Person
     */

    public Person findPersonByLogin(String login, String password) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Person person = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = connectionPool.retrieve();
            preparedStatement = connection.prepareStatement(CHECK_LOGIN_PERSON);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                return person;
            }

            resultSet.next();

            if (PARAM_ADMIN.equals(resultSet.getString(PARAM_PERSON_STATUS))) {
                person = new Admin();
                person.setId(resultSet.getInt(PARAM_PERSON_ID));
                person.setLogin(resultSet.getString(PARAM_PERSON_LOGIN));
                person.setPassword(resultSet.getString(PARAM_PERSON_PASSWORD));
                person.setStatus(resultSet.getString(PARAM_PERSON_STATUS));
                preparedStatement = connection.prepareStatement(FIND_ADMIN);
                preparedStatement.setInt(1, person.getId());
                resultSet = preparedStatement.executeQuery();
                resultSet.next();
                person.setFirstName(resultSet.getString(PARAM_PERSON_FIRST_NAME));
                person.setSecondName(resultSet.getString(PARAM_PERSON_LAST_NAME));
                ((Admin) person).setPosition(resultSet.getString(PARAM_PERSON_POSITION));
                return person;
            } else {
                person = new Client();
                person.setId(resultSet.getInt(PARAM_PERSON_ID));
                person.setLogin(resultSet.getString(PARAM_PERSON_LOGIN));
                person.setPassword(resultSet.getString(PARAM_PERSON_PASSWORD));
                preparedStatement = connection.prepareStatement(FIND_CLIENT);
                preparedStatement.setInt(1, person.getId());
                resultSet = preparedStatement.executeQuery();
                resultSet.next();
                person.setFirstName(resultSet.getString(PARAM_PERSON_FIRST_NAME));
                person.setSecondName(resultSet.getString(PARAM_PERSON_LAST_NAME));
                ((Client) person).setAdress(resultSet.getString(PARAM_PERSON_ADDRESS));
                ((Client) person).setClientStatus(resultSet.getString(PARAM_PERSON_STATUS));
                return person;
            }


        } catch (SQLException e) {
            throw new DAOException("SQLException in DAO layer during finding the user", e);
        } finally {
            PersonDAO.close(preparedStatement);
            connectionPool.putback(connection);
        }

    }

    /**
     * Adds new client in dataBase while registrating process
     *
     * @param person - object of Person class with all user information
     * @return true if add
     * @throws DAOException
     * @see Person
     */

    public boolean addClient(Person person) throws DAOException {

        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        PreparedStatement clientStatement = null;
        boolean flag = false;

        try {
            connection = connectionPool.retrieve();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(INSERT_NEW_INTO_LOGIN);
            preparedStatement.setInt(1, person.getId());
            preparedStatement.setString(2, person.getLogin());
            preparedStatement.setString(3, person.getPassword());
            preparedStatement.setString(4, PARAM_CLIENT);
            preparedStatement.execute();

            clientStatement = connection.prepareStatement(INSERT_NEW_INTO_CLIENT);
            clientStatement.setInt(1, person.getId());
            clientStatement.setString(2, person.getFirstName());
            clientStatement.setString(3, person.getSecondName());
            clientStatement.setString(4, ((Client) person).getAdress());
            clientStatement.setString(5, PARAM_BASIC);
            clientStatement.setDouble(6, 0);
            clientStatement.execute();
            connection.commit();
            flag = true;


        } catch (SQLException e) {

            try {
                connection.rollback();
            } catch (SQLException e1) {
                LOG.error("Exception during rollback transaction",e);
            }

            throw new DAOException("Exception while registration process, check input information or DB connection", e);

        } finally {

            PersonDAO.close(preparedStatement);
            PersonDAO.close(clientStatement);
            connectionPool.putback(connection);

        }

        return flag;


    }

    /**
     * Updates user info such as login or password
     *
     * @param userInfo - hashMap with all updating info
     * @return true if update false if not
     * @throws DAOException
     */

    public boolean update(HashMap<String, String> userInfo) throws DAOException {
        Connection connection = ConnectionPool.getInstance().retrieve();
        PreparedStatement preparedStatement = null;

        String userNewLogin = userInfo.get(PARAM_USER_LOGIN);
        String userNewPassword = userInfo.get(PARAM_USER_NEW_PASSWORD);

        System.out.println(userNewLogin);
        System.out.println(userNewPassword);

        try {


            if (!userNewLogin.isEmpty()) {
                try {

                    preparedStatement = connection.prepareStatement(UPDATE_USER_LOGIN);
                    preparedStatement.setString(1, userNewLogin);
                    preparedStatement.setInt(2, Integer.parseInt(userInfo.get(PARAM_USER_ID)));
                    preparedStatement.execute();

                } catch (SQLException e) {
                    throw new DAOException("Exception during updating user info, please check input data or DB connection", e);
                }
            }
            if (!userNewPassword.isEmpty()) {
                try {
                    preparedStatement = connection.prepareStatement(UPDATE_USER_PASSWORD);
                    preparedStatement.setString(1, userNewPassword);
                    preparedStatement.setInt(2, Integer.parseInt(userInfo.get(PARAM_USER_ID)));
                    preparedStatement.execute();

                } catch (SQLException e) {
                    throw new DAOException("Exception during updating user info, please check input data or DB connection", e);
                }
            }
        } finally {
            PersonDAO.close(preparedStatement);
            ConnectionPool.getInstance().putback(connection);
        }

        return true;


    }

    /**
     * Takes id list of users from DB
     *
     * @return id list
     * @throws DAOException
     */

    public static ArrayList<Integer> takeMaxId() throws DAOException {

        ArrayList<Integer> idList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().retrieve();
            preparedStatement = connection.prepareStatement(TAKE_ID);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                idList.add(resultSet.getInt("person_id"));
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while checking if the login is free", e);
        } finally {
            PersonDAO.close(preparedStatement);
            ConnectionPool.getInstance().putback(connection);
        }

        return idList;
    }

    /**
     * Selects all users who are in the dataBase
     *
     * @return userList
     * @throws DAOException
     * @see Client
     */

    public ArrayList<Client> selectUserList() throws DAOException {

        ArrayList<Client> userList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().retrieve();
            preparedStatement = connection.prepareStatement(SELECT_USER_LIST);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                userList.add(
                        new Client(
                                resultSet.getInt(PARAM_PERSON_ID),
                                resultSet.getString(PARAM_PERSON_FIRST_NAME),
                                resultSet.getString(PARAM_PERSON_LAST_NAME),
                                resultSet.getString(PARAM_USER_LOGIN),
                                PARAM_CONFIDENT,
                                resultSet.getString(PARAM_PERSON_ADDRESS),
                                PARAM_BASIC,
                                PARAM_MONEY
                        )
                );
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while selecting user list", e);
        } finally {
            PersonDAO.close(preparedStatement);
            ConnectionPool.getInstance().putback(connection);
        }
        return userList;
    }
}
