$(function () {
    $('#loginhref').on('click', function () {
        $('#smallForm').fadeIn(200);
        $('#mainForm').fadeIn(200);
        $('#largeForm').fadeOut(100);
        $('#listForm').fadeOut(100);

    });

    $('input:submit').button();
    $('button:submit').button();
    $('input:text, input:password,input[type=number],input[type=time], input[type=email],#ui-datepicker-month').button()
        .addClass('ui-textfield')
        .off('mouseenter').off('mousedown').off('keydown');
    $('#registerhref').on('click', function () {
        $('#mainForm').fadeOut(100);
        $('#smallForm').fadeOut(100);
        $('#largeForm').fadeIn(200);
        $('#listForm').fadeOut(100);
    });


    $('#homehref').on('click', function () {
        $('#mainForm').fadeIn(200);
        $('#smallForm').fadeOut(100);
        $('#largeForm').fadeOut(100);
        $('#listForm').fadeOut(100);
    });


    $('#buyticketshref').on('click', function () {
        $('#mainForm').fadeIn(200);
        $('#listForm').fadeOut(100);
    });

    $(function () {
        $("#datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1,
            dateFormat: "yy-mm-dd",
            beforeShow: function () {
                $(".ui-datepicker").css('font-size', 12)
                $(".ui-datepicker-month").css('color', '#41A2D7')
            },
            minDate: 0,
            maxDate: "+1Y"
        });


    });


});
