<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<fmt:setLocale value="${Locale}" scope="session"/>
<fmt:bundle basename="resources.pagecontent">
    <html>
    <head>
        <title><fmt:message key="title"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <script type="text/javascript" src="../jquery/js/jquery.js"></script>
        <script type="text/javascript" src="../jquery/js/jquery-ui.js"></script>
        <script type="text/javascript" src="../jquery/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../js/script.js"></script>
        <link type="text/css" rel="stylesheet" href="../jquery/style/jquery-ui.css"/>
        <link rel="stylesheet" type="text/css" href="../css/styles.css"/>

    </head>

    <body>

    <div id="header">
        <h1></h1>
    </div>
    <div id="wrapper">
        <div id="navigation" class="ui-corner-all">

            <ul><ctg:info-time/></ul>

        </div>
        <div id="leftcolumn" class="ui-corner-left">

            <div id="mainForm" class="generalForm ui-corner-all ui-helper-reset accordion">
                <h2 class="first current ui-corner-top ui-helper-reset">
                    <span class=""></span>
                    <fmt:message key="accordion.ticket"/>
                </h2>

                <div class="pane generalPane ui-helper-reset">


                    <form name="buyingForm" class="loginForm pane" method="POST" action="controller">
                        <input type="hidden" name="command" value="selectFlight"/>
                        <fmt:message key="buyingForm.depature"/><br/>
                        <select class="ui-corner-all" name="departurePlace" required>
                            <option name="depaturePlace" value=""></option>
                            <c:forEach items="${flightArray}" var="temp">
                                <option name="depaturePlace" value="${temp}" >${temp}</option>
                            </c:forEach>

                        </select>
                        <br/><fmt:message key="buyingForm.arrival"/><br/>
                        <select class="ui-corner-all" name="arrivalPlace">

                            <c:forEach items="${flightArray}" var="temp">
                                <option name="arrivalPlace" value="${temp}">${temp}</option>
                            </c:forEach>

                        </select>

                        <p><fmt:message key="buyingForm.date"/><br/>
                            <input type="text" id="datepicker" name="flightDate" value="" class="date ui-corner-left"
                                   required autocomplete="off" onclick="this.blur();"></p>
                        <br/><fmt:message key="buyingForm.amount"/><br/>
                        <select class="ui-corner-all" name="ticketAmount">

                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                        <br/>

                        <p class="errorMessage"> ${errorFlightSelectorMessage} </p>
                        <br/>
                        <input type="submit" id="jsbutton"
                               class="ui-button ui-button-text-icons  ui-button-text ui-corner-all"
                               value="<fmt:message key ="button.find"/>"/>
                    </form>

                </div>
            </div>


            <div id="smallForm" class="generalForm ui-corner-all ui-helper-reset accordion">
                <h2 class="first current ui-corner-top ui-helper-reset">
                    <span class=""></span>
                    <fmt:message key="accordion.login"/>
                </h2>

                <div class="pane generalPane ui-helper-reset">
                    <form name="loginForm" class="loginForm pane" method="POST" action="controller">
                        <input type="hidden" name="command" value="login"/>
                        <fmt:message key="loginForm.login"/>:<br/>
                        <input pattern=".{5,20}" type="text" name="login" value="" required/>
                        <br/><fmt:message key="loginForm.password"/>:<br/>
                        <input pattern=".{5,20}" type="password" name="password" value="" required/>

                        <p class="errorMessage">
                            <br/>
                                ${errorLoginPassMessage}
                            <br/>
                                ${wrongAction}
                            <br/>
                                ${nullPage}
                            <br/>
                        </p>
                        <input type="submit" class="ui-button" value="<fmt:message key ="button.login"/>"/>
                    </form>
                </div>
            </div>


            <div id="largeForm" class="generalForm loginclass ui-corner-all ui-helper-reset accordion">
                <h2 class="first current ui-corner-top ui-helper-reset">
                    <span class=""></span>
                    <fmt:message key="accordion.register"/>
                </h2>

                <div class="pane generalPane ui-helper-reset">
                    <form name="registrationForm" class="loginForm pane" method="POST" action="controller">
                        <input type="hidden" name="command" value="register"/>
                        <fmt:message key="loginForm.login"/>:<br/>
                        <input pattern=".{5,20}" type="text" name="login" value="" required/>
                        <br/><fmt:message key="loginForm.password"/>:<br/>
                        <input pattern=".{5,20}" type="password" name="password" value="" required/>
                        <br/><fmt:message key="registerForm.firstName"/>:<br/>
                        <input pattern=".{3,20}" type="text" name="firstName" value="" required/>
                        <br/><fmt:message key="registerForm.secondName"/>:<br/>
                        <input pattern=".{3,20}" type="text" name="secondName" value="" required/>
                        <br/><fmt:message key="registerForm.address"/>:<br/>
                        <input pattern=".{5,30}" type="text" name="adress" value="" required/>
                        <br/>

                        <p class="errorMessage"> ${errorRegisterPassMessage} </p>
                        <br/>
                        <input type="submit" class="ui-button ui-button-text-icons  ui-button-text ui-corner-all"
                               value="<fmt:message key="button.signup"/>"/>
                    </form>
                </div>
            </div>

        </div>

        <div id="rightcolumn" class="ui-corner-right">

            <div id="infoForm" class=" ui-corner-all ui-helper-reset accordion">
                <h2 class="first current ui-corner-top ui-helper-reset">
                    <span class=""></span>
                    <fmt:message key="accordion.bestoffers"/>
                </h2>

                <div class="bestoffers pane generalPane ui-helper-reset">
                        ${user} , <fmt:message key="accordion.watchbestoffers"/>
                </div>
                <div class="bestoffers pane generalPane ui-helper-reset">
                        ${purchaseConfirm}

                </div>

            </div>
            <div id="adminInfoForm" class=" ui-corner-all ui-helper-reset accordion" style="display: none">
                <h2 class="first current ui-corner-top ui-helper-reset">
                    <span class=""></span>
                    <fmt:message key="accordion.adminInfo"/>
                </h2>

                <div class="bestoffers pane generalPane ui-helper-reset">
                        ${addDestinationConfirm}
                        ${addFlightConfirm}

                </div>
            </div>


        </div>
        <!-- End Right Column -->
        <div id="botton">

            <form name="languageForm" class="languageForm" method="POST" action="controller">
                <input type="hidden" name="command" value="language"/>
                <button type="submit" name="language" class="ui-button" value="english"><fmt:message
                        key="english"/></button>
                <button type="submit" name="language" class="ui-button" value="russian"><fmt:message
                        key="russian"/></button>
            </form>
        </div>
    </div>
    <script>
        var atr = "${loginMessage}";
        if (atr == "true") {
            $('#smallForm').fadeIn(10);
        }
        atr = "${registerMessage}";
        if (atr == "true") {
            $('#largeForm').fadeIn(200);
            $('#mainForm').fadeOut(10);
        }

        var role = "${sessionScope.role}";
        if (role == "user") {
            $('#infoForm').fadeIn(200);
        }
        if (role == "admin") {
            $('#adminInfoForm').fadeIn(200);
        }


    </script>
    </body>
    </html>
</fmt:bundle>