<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<fmt:setLocale value="${Locale}" scope="session"/>
<fmt:bundle basename="resources.pagecontent">
    <html>
    <head>
        <title><fmt:message key="title"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <script type="text/javascript" src="../jquery/js/jquery.js"></script>
        <script type="text/javascript" src="../jquery/js/jquery-ui.js"></script>
        <script type="text/javascript" src="../jquery/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../js/script.js"></script>
        <link type="text/css" rel="stylesheet" href="../jquery/style/jquery-ui.css"/>
        <link rel="stylesheet" type="text/css" href="../css/styles.css"/>
        <link rel="stylesheet" type="text/css" href="../css/table_style.css"/>

    </head>
    <style>
    </style>
    <body>
    <script>
        $(function () {
            $('#updateHref').on('click', function () {
                $('#mainForm').fadeOut(100);
                $('#flightTable').fadeOut(100);
                $('#largeForm').fadeIn(200);
            });

            $('#flightHref').on('click', function () {
                $('#mainForm').fadeOut(100);
                $('#flightTable').fadeIn(200);
                $('#largeForm').fadeOut(100);
            });
        });
    </script>

    <div id="header">

    </div>

    <div id="wrapper">
        <div id="navigation" class="ui-corner-all">
            <ul>
                <li><a id="backHref" href="index.jsp"><fmt:message key="href.back"/></a></li>
                <li><a id="updateHref" href="javascript:void(0);"><fmt:message key="href.updateInfo"/></a></li>
                <li><a id="flightHref" href="javascript:void(0);"><fmt:message key="href.myFlights"/></a></li>
                <li><a id="logoutHref\" href="controller?command=logout"><fmt:message key="href.logout"/></a></li>
            </ul>
        </div>
        <div id="leftcolumn" class="ui-corner-left" style="width: 98%">

            <div id="mainForm" class="loginclass generalForm ui-corner-all ui-helper-reset accordion"
                 style="display: block; width: 350px;margin: 0 auto">
                <h2 class="first current ui-corner-top ui-helper-reset">
                    <span class=""></span>
                    <fmt:message key="form.info"/>
                </h2>

                <div class="bestoffers pane generalPane ui-helper-reset">

                    <fmt:message key="form.username"/><br/>
                        ${userLogin}
                    <br/><fmt:message key="form.name"/>:<br/>
                        ${user}



                </div>
            </div>


            <div id="largeForm" class="loginclass generalForm ui-corner-all ui-helper-reset accordion"
                 style="display: none; width: 450px;margin: 0 auto">
                <h2 class="first current ui-corner-top ui-helper-reset">
                    <span class=""></span>
                    <fmt:message key="form.updateUserInfo"/>
                </h2>

                <div class="pane generalPane ui-helper-reset">
                    <form name="registrationForm" class="loginForm pane" method="POST" action="controller">
                        <input type="hidden" name="command" value="updateUserInfo"/>
                        <fmt:message key="updateForm.newLogin"/>:<br/>
                        <input type="text" name="login" value="${userLogin}"/>
                        <br/><fmt:message key="updateForm.newPassword"/>:<br/>
                        <input type="password" name="newPassword" value=""/>
                        <br/><fmt:message key="updateForm.repeatNewPassword"/>:<br/>
                        <input type="password" name="newPasswordRepeat" value=""/>
                        <br/>

                        <p class="errorMessage">
                                ${updateErrorMessage}
                                ${successfulUpdate}
                        </p>
                        <br/>
                        <input type="submit" class="ui-button ui-button-text-icons  ui-button-text ui-corner-all"
                               value=" <fmt:message key="button.update"/>"/>
                    </form>

                </div>
            </div>



                    <table id="flightTable" class="simple-little-table" cellspacing='0' style="margin: 0 auto;display: none">
                        <tr>
                            <th><fmt:message key="table.clientName"/></th>
                            <th><fmt:message key="table.departurePlace"/></th>
                            <th><fmt:message key="table.arrivalPlace"/></th>
                            <th><fmt:message key="table.departureTime"/></th>
                            <th><fmt:message key="table.arrivalTime"/></th>
                            <th><fmt:message key="table.addLuggage"/></th>
                            <th><fmt:message key="table.premiumStatus"/></th>
                        </tr>


                        <c:forEach items="${userFlightList}" var="temp">
                            <tr>
                                <td>${temp.name}</td>
                                <td>${temp.departurePlace}</td>
                                <td>${temp.arrivalPlace}</td>
                                <td style="padding: 1px">${temp.departureTime}</td>
                                <td style="padding: 1px">${temp.arrivalTime}</td>
                                <td>${temp.luggagePrice}</td>
                                <td>${temp.premiumPrice}</td>

                            </tr>
                        </c:forEach>

                    </table>


        </div>


    </div>

    </body>
    </html>
</fmt:bundle>