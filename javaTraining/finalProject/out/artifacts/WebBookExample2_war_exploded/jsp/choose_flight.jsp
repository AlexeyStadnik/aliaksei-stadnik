<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<fmt:setLocale value="${Locale}" scope="session"/>
<fmt:bundle basename="resources.pagecontent">
    <html>
    <head>
        <title><fmt:message key="title"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <script type="text/javascript" src="../jquery/js/jquery.js"></script>
        <script type="text/javascript" src="../jquery/js/jquery-ui.js"></script>
        <script type="text/javascript" src="../jquery/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../js/script.js"></script>
        <link type="text/css" rel="stylesheet" href="../jquery/style/jquery-ui.css"/>
        <link rel="stylesheet" type="text/css" href="../css/styles.css"/>
        <link rel="stylesheet" type="text/css" href="../css/table_style.css"/>

    </head>
    <style>
    </style>
    <body>

    <div id="header">

    </div>

    <div id="wrapper">
        <div id="navigation" class="ui-corner-all">

        </div>
        <div id="leftcolumn" class="ui-corner-left" style="width: 98%">




                    <form name="buyingForm" class="loginForm pane" >


                        <table class="simple-little-table" cellspacing='0' style="margin: 0 auto">
                            <tr>
                                <th><fmt:message key="table.departurePlace"/></th>
                                <th><fmt:message key="table.arrivalPlace"/></th>
                                <th><fmt:message key="table.departureTime"/></th>
                                <th><fmt:message key="table.arrivalTime"/></th>
                                <th><fmt:message key="table.flightPrice"/></th>
                                <th><fmt:message key="table.luggagePrice"/></th>
                                <th><fmt:message key="table.premiumPrice"/></th>
                                <th>
                                    <form  class="purchaseForm loginForm pane" method="POST" action="controller">
                                        <input type="hidden" name="command" value="start"/>
                                        <button class="backButton" type="submit" name="purchaseFlight" class="ui-button"  ><fmt:message key="button.back"/></button>
                                    </form>
                                </th>
                            </tr>
                            <!-- Table Header -->

                            <c:forEach items="${flightList}" var="temp">
                                <tr>
                                    <td>${temp.departurePlace}</td>
                                    <td>${temp.arrivalPlace}</td>
                                    <td style="padding: 1px">${temp.departureTime}</td>
                                    <td style="padding: 1px">${temp.arrivalTime}</td>
                                    <td>${temp.flightPrice}$</td>
                                    <td>${temp.luggagePrice}$</td>
                                    <td>${temp.premiumPrice}$</td>
                                    <td>
                                        <form  class="purchaseForm loginForm pane" method="POST" action="controller">
                                            <input type="hidden" name="command" value="purchase"/>
                                            <button class="purchaseButton" type="submit" name="purchaseFlight" class="ui-button" value="${temp.id}"style="display: none"><fmt:message key="button.purchase"/></button>
                                        </form>

                                        <form  class="purchaseForm loginForm pane" method="POST" action="controller">
                                            <input type="hidden" name="command" value="updateFlight"/>
                                            <button class="adminButton" type="submit" name="updateFlight" class="ui-button" value="${temp.id}"style="display: none"><fmt:message key="button.update"/></button>
                                        </form>

                                        <form name="languageForm" class="guestLink loginForm pane" method="POST" action="controller">
                                            <input type="hidden" name="command" value="login"/>
                                            <button type="submit" name="loginOrSignUp" class="ui-button" value="login"><fmt:message key="button.login"/></button>
                                        </form>
                                        <form name="languageForm" class="guestLink loginForm pane" method="POST" action="controller">
                                            <input type="hidden" name="command" value="register"/>
                                            <button type="submit" name="loginOrSignUp" class="ui-button" value="signup"><fmt:message key="button.signup"/></button>
                                        </form>



                                    </td>
                                </tr>
                            </c:forEach>

                        </table>

                    </form>

        </div>
    </div>
    <script>
        var role = "${sessionScope.role}";
        if (role == "user") {
            $('.purchaseButton').fadeIn(200);
            $('.guestLink').fadeOut(100);
        }
        if (role == "admin") {
            $('.purchaseButton').fadeOut(100);
            $('.guestLink').fadeOut(100);
            $('.adminButton').fadeIn(100);
        }
    </script>
    </body>
    </html>
</fmt:bundle>

