<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<fmt:setLocale value="${Locale}" scope="session"/>
<fmt:bundle basename="resources.pagecontent">
    <html>
    <head>
        <title><fmt:message key="title"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <script type="text/javascript" src="../jquery/js/jquery.js"></script>
        <script type="text/javascript" src="../jquery/js/jquery-ui.js"></script>
        <script type="text/javascript" src="../jquery/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../js/script.js"></script>
        <link type="text/css" rel="stylesheet" href="../jquery/style/jquery-ui.css"/>
        <link rel="stylesheet" type="text/css" href="../css/styles.css"/>
        <link rel="stylesheet" type="text/css" href="../css/table_style.css"/>

    </head>

    <body>
    <script>
        $(function () {
            $('#addHref').on('click', function () {
                $('#mainForm').fadeIn(100);
                $('#smallForm').fadeOut(100);
                $('#largeForm').fadeIn(200);
            });

            $('#userListHref').on('click', function () {
                $('#mainForm').fadeOut(100);
                $('#smallForm').fadeIn(200);
                $('#largeForm').fadeOut(100);
            });
        });
    </script>

    <div id="header">

    </div>

    <div id="wrapper">
        <div id="navigation" class="ui-corner-all">
            <ul>
                <li><a id="backHref" href="index.jsp"><fmt:message key="href.back"/></a></li>
                <li><a id="addHref" href="javascript:void(0);"><fmt:message key="href.addFlight"/></a></li>
                <li><a id="userListHref" href="javascript:void(0);"><fmt:message key="href.userList"/></a></li>
                <li><a id="logoutHref\" href="controller?command=logout"><fmt:message key="href.logout"/></a></li>
            </ul>
        </div>
        <div id="leftcolumn" class="ui-corner-left" style="width: 98%">

            <div id="mainForm" class="generalForm ui-corner-all ui-helper-reset accordion" style="display: block">
                <h2 class="first current ui-corner-top ui-helper-reset">
                    <span class=""></span>
                    <fmt:message key="addForm.destination"/>
                </h2>

                <div class="pane generalPane ui-helper-reset">


                    <form name="buyingForm" class="loginForm pane" method="POST" action="controller">
                        <input type="hidden" name="command" value="addDestination"/>
                        <fmt:message key="addForm.destination"/><br/>
                        <input pattern=".{2,20}" type="text" name="destination" value="" required/>
                        </br>
                            ${destinationErrorMessage}
                        </br>
                        <input type="submit" id="jsbutton"
                               class="ui-button ui-button-text-icons  ui-button-text ui-corner-all"
                               value="<fmt:message key ="button.addDestination"/>"/>
                    </form>

                </div>
            </div>


            <div id="largeForm" class="loginclass generalForm ui-corner-all ui-helper-reset accordion"
                 style="display: block; width: 300px; height: 620px">
                <h2 class="first current ui-corner-top ui-helper-reset">
                    <span class=""></span>
                    <fmt:message key="form.addFlight"/>
                </h2>

                <div class="pane generalPane ui-helper-reset">
                    <form name="buyingForm" class="loginForm pane" method="POST" action="controller">
                        <input type="hidden" name="command" value="addFlight"/>
                        <fmt:message key="addForm.departure"/><br/>
                        <select class="ui-corner-all" name="departurePlace">

                            <c:forEach items="${flightArray}" var="temp">
                                <option name="departurePlace" value="${temp}">${temp}</option>
                            </c:forEach>

                        </select>
                        <br/><fmt:message key="addForm.arrival"/><br/>
                        <select class="ui-corner-all" name="arrivalPlace">

                            <c:forEach items="${flightArray}" var="temp">
                                <option name="arrivalPlace" value="${temp}">${temp}</option>
                            </c:forEach>

                        </select>

                        <br/>

                        <p><fmt:message key="addForm.departureDate"/><br/>
                            <input type="text" id="datepicker" name="departureDate" value=""
                                   class=" date ui-corner-left"
                                   required autocomplete="off" onclick="this.blur();"></p>

                        <br/><fmt:message key="addForm.departureTime"/>:<br/>
                        <input type="time" name="departureTime" value="" required/>


                        <br/><fmt:message key="addForm.duration"/>:<br/>
                        <input type="time" name="durationTime" value="" required/>

                        <br/><fmt:message key="updateForm.ticketPrice"/>:<br/>
                        <input type="number" step="any" name="ticketPrice" value="" required/>

                        <br/><fmt:message key="updateForm.luggagePrice"/>:<br/>
                        <input type="number" step="any" name="luggagePrice" value="" required/>

                        <br/><fmt:message key="updateForm.premiumPrice"/>:<br/>
                        <input type="number" step="any" name="premiumPrice" value="" required/>

                        <br/><fmt:message key="addForm.capacity"/>:<br/>
                        <input type="number" step="1" name="capacity" value="" required/>
                        <br/>


                        <br/>

                        <p class="errorMessage">${errorFlightAddMessage}</p>
                        <input type="submit" id="jsbutton"
                               class="ui-button ui-button-text-icons  ui-button-text ui-corner-all"
                               value="<fmt:message key ="button.addFlight"/>"/>
                    </form>


                </div>
            </div>

            <div id="smallForm" class=" loginclass generalForm ui-corner-all ui-helper-reset accordion"
                 style="width: 99% ;margin: 0 auto ;height: 600px">
                <h2 class="first current ui-corner-top ui-helper-reset">
                    <span class=""></span>
                    <fmt:message key="form.userList"/>
                </h2>

                <div class="pane generalPane ui-helper-reset">

                    <table class="simple-little-table" cellspacing='0' style="margin: 0 auto">
                        <tr>
                            <th><fmt:message key="table.userId"/></th>
                            <th><fmt:message key="table.userLogin"/></th>
                            <th><fmt:message key="table.userFirstName"/></th>
                            <th><fmt:message key="table.userLastName"/></th>
                            <th><fmt:message key="table.userAddress"/></th>
                            <th><fmt:message key="table.watchFlight"/></th>
                        </tr>


                        <c:forEach items="${userList}" var="temp">
                            <tr>
                                <td>${temp.id}</td>
                                <td>${temp.login}</td>
                                <td>${temp.firstName}</td>
                                <td>${temp.secondName}</td>
                                <td>${temp.adress}</td>
                                <td>
                                    <form class="purchaseForm loginForm pane" method="POST" action="controller">
                                        <input type="hidden" name="command" value="userFlights"/>
                                        <button class="purchaseButton" type="submit" name="adminButton"
                                                class="ui-button" value="${temp.id}"><fmt:message
                                                key="button.userFlights"/></button>
                                    </form>
                                </td>

                            </tr>
                        </c:forEach>

                    </table>

                </div>
            </div>

        </div>


    </div>

    </body>
    </html>
</fmt:bundle>
