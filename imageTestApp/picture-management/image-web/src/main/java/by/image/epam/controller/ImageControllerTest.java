package by.image.epam.controller;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import by.image.epam.service.ImageService;
import by.image.epam.controller.ImageController;
import by.image.epam.entity.Image;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:mvc-daspatcher-servlet.xml"})
public class ImageControllerTest {

	@Mock
	private ImageService imageService;

	@InjectMocks
	private ImageController imageController;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(imageController).build();
	}


	@Test
	public void getAllImagesTest() throws Exception {
		String image1 = "im1";
		String image2 = "im2";
		when(imageService.getAllImageTitles()).thenReturn(Arrays.asList(image1, image2));

		mockMvc.perform(get("/images"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$", hasSize(2)));

		verify(imageService, times(1)).getAllImageTitles();
		verifyNoMoreInteractions(imageService);
	}

	@Test
	public void getSinglImageTest() throws Exception {
		String imageTitle = "title";
		byte [] imageData = new byte[10];
		Image image = new Image(imageTitle,new byte[10]);
		when(imageService.getImage(imageTitle)).thenReturn(image);

		mockMvc.perform(get("/images/title"))
		.andExpect(status().is(201))
		.andExpect(content().bytes(imageData));

		verify(imageService, times(1)).getImage(imageTitle);
		verifyNoMoreInteractions(imageService);
	}

	@Test
	public void deleteImageTest() throws Exception {
		mockMvc.perform(delete("/images/title"))
		.andExpect(status().is(200));

		verify(imageService, times(1)).deleteImage("title");
		verifyNoMoreInteractions(imageService);
	}

	@Test
	public void postImageTest() throws Exception {
		String imageTitle = "title";
		final byte [] imageData = new byte[10];
		Image image = new Image(imageTitle,new byte[10]);
		when(imageService.createImage(image)).thenReturn(imageTitle);

		MockMultipartFile file = new MockMultipartFile("image", imageData);
		mockMvc.perform(fileUpload("/images", Long.valueOf(1))
				.file(file)
				.contentType(MediaType.IMAGE_JPEG))
				.andExpect(status().is(201));

		verify(imageService, times(1)).createImage(Mockito.any(Image.class));
		verifyNoMoreInteractions(imageService);
	}
}