package by.image.epam.controller;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import by.image.epam.entity.Image;
import by.image.epam.service.ImageService;
import by.image.epam.service.ServiceException;

@Controller
@RequestMapping(value = "images" )  
public class ImageController {

	@Autowired
	private ImageService imageService;

	@RequestMapping(method = RequestMethod.GET)
	public  @ResponseBody List<String> getAllImages(HttpServletResponse response) {
		List<String> titleList = null;
		try {
			titleList = imageService.getAllImageTitles();
		} catch (ServiceException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return null;
		}
		return titleList;
	}

	@RequestMapping(value="/{imageTitle}" , method = RequestMethod.DELETE)
	public @ResponseBody String deleteImage(@PathVariable("imageTitle") String imageTitle,
			HttpServletResponse response) {
		try {
			imageService.deleteImage(imageTitle);
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (ServiceException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		return "delete";
	}

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody String addImageFromForm(MultipartHttpServletRequest request,HttpServletResponse response)  {
		Iterator<String> iterator = request.getFileNames();
		MultipartFile multiFile = null;

		if(iterator.hasNext()) {
			multiFile = request.getFile(iterator.next());
		} else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return "error";
		}

		try {
			Image newImage = new Image();
			newImage.setImageTitle("title" + System.currentTimeMillis());
			newImage.setImageData(multiFile.getBytes());
			imageService.createImage(newImage);
		} catch (IOException | ServiceException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return "error";
		}
		response.setStatus(HttpServletResponse.SC_CREATED);
		return "created";
	}

	@RequestMapping(value="/{imageTitle}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<byte[]> getImage(@PathVariable("imageTitle") String imageTitle) {
		Image image = null;
		byte [] imageData = null;
		HttpHeaders headers = new HttpHeaders();
		try {
			image = imageService.getImage(imageTitle);
			imageData = image.getImageData();
			headers.setContentType(MediaType.IMAGE_JPEG);
		} catch (ServiceException e) {
			return new ResponseEntity<byte[]>(new byte[0], HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<byte[]>(imageData, headers, HttpStatus.CREATED);
	}

}
