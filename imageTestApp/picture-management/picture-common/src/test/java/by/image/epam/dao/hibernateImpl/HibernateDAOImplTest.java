package by.image.epam.dao.hibernateImpl;

import java.util.List;

import org.dbunit.DBTestCase;
import org.dbunit.IDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.image.epam.dao.DAOException;
import by.image.epam.dao.ImageDAO;
import by.image.epam.entity.Image;

@ContextConfiguration(locations = {"classpath:/applicationContextTest.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class HibernateDAOImplTest extends DBTestCase{
	
	@Autowired
	private  IDatabaseTester databaseTester;
	@Autowired
	private ImageDAO imageDAO;
	
	

	public IDatabaseTester getDatabaseTester() {
		return databaseTester;
	}

	public void setDatabaseTester(IDatabaseTester databaseTester) {
		this.databaseTester = databaseTester;
	}

	public ImageDAO getImageDAO() {
		return imageDAO;
	}

	public void setImageDAO(ImageDAO imageDAO) {
		this.imageDAO = imageDAO;
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		IDataSet dataSet = new FlatXmlDataSetBuilder().build(this.getClass().getResourceAsStream(
				"/script/imageDAO-test.xml"
				));
		return dataSet;
	}
	
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.DELETE_ALL;
	}
	
	@Before
	public void init() throws Exception  {
		databaseTester.setTearDownOperation(DatabaseOperation.DELETE_ALL);
		databaseTester.setDataSet(getDataSet());
		databaseTester.onSetup();
	}

	@After
	public void onTearDown() throws Exception {
		databaseTester.onTearDown();
	}
	
	@Test
	public void getImageTest() throws DAOException {
		String title = "test";
		Image image = imageDAO.getImage(title);
		assertNotNull(image);
		assertEquals(title, image.getImageTitle());
	}
	
	@Test
	public void deleteImageTest() throws DAOException {
		String title = "delete";
		imageDAO.deleteImage(title);
		Image image = imageDAO.getImage(title);
		assertNull(image);
	}
	
	@Test
	public void postImageTest() throws DAOException {
		String title = "post";
		Image image = new Image(title,"12345".getBytes());
		String actualsTitle = imageDAO.createImage(image);
		assertEquals(actualsTitle,title);
		assertNotNull(imageDAO.getImage(title));
	}
	
	@Test
	public void getAllImagesTest() throws DAOException {
		List<String> testList = imageDAO.getAllImageTitles();
		assertEquals(3,testList.size());
		assertTrue(testList.contains("count"));
	}

}
