package by.image.epam.service;

import java.util.List;

import by.image.epam.entity.Image;

public interface ImageService {
	
	String createImage(Image image) throws ServiceException;
	Image getImage(String imageTitle) throws ServiceException;
	void deleteImage(String imageTitle) throws ServiceException;
	String updateImage(Image image) throws ServiceException;
	List<String> getAllImageTitles() throws ServiceException;
}
