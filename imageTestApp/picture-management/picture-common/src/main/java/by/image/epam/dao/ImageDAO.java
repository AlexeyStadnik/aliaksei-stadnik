package by.image.epam.dao;

import java.util.List;

import by.image.epam.entity.Image;

public interface ImageDAO {
	
	String createImage(Image image) throws DAOException;
	Image getImage(String imageTitle) throws DAOException;
	void deleteImage(String imageTitle) throws DAOException;
	String updateImage(Image image) throws DAOException;
	List<String> getAllImageTitles() throws DAOException;
	
}
