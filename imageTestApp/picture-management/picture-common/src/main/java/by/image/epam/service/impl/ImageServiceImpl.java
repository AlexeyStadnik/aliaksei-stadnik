package by.image.epam.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import by.image.epam.dao.DAOException;
import by.image.epam.dao.ImageDAO;
import by.image.epam.entity.Image;
import by.image.epam.service.ImageService;
import by.image.epam.service.ServiceException;


public class ImageServiceImpl implements ImageService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ImageServiceImpl.class);
	

	private ImageDAO imageDAO;

	public ImageServiceImpl() {

	}

	public ImageServiceImpl(ImageDAO imageDAO) {
		this.imageDAO = imageDAO;
	}

	public void setImageDAO(ImageDAO imageDAO) {
		this.imageDAO = imageDAO;
	}

	public String createImage(Image image) throws ServiceException {
		String imageTitle = null;
		try {
			imageTitle =  imageDAO.createImage(image);
		} catch (DAOException e) {
			LOGGER.error("Exception while image with title -" 
					+ image.getImageTitle() + " creating process",e);
			throw new ServiceException("Exception while image with title -" 
					+ image.getImageTitle() + " creating process",e);
		}
		return imageTitle;
	}

	public Image getImage(String imageTitle) throws ServiceException {
		Image image = null;
		try {
			image = imageDAO.getImage(imageTitle);
		} catch (DAOException e) {
			LOGGER.error("Exception while image with title -" 
					+ imageTitle + " creating process",e);
			throw new ServiceException("Exception while image with title -" 
					+ imageTitle + " creating process",e);
		}
		return image;
	}

	public void deleteImage(String imageTitle) throws ServiceException {
		try {
			imageDAO.deleteImage(imageTitle);
		} catch (DAOException e) {
			LOGGER.error("Exception while image with title -" 
					+ imageTitle + " deleting process",e);
			throw new ServiceException("Exception while image with title -" 
					+ imageTitle + " deleting process",e);
		}
	}

	public String updateImage(Image image) throws ServiceException {
		String imageTitle = null;
		try {
			imageTitle =  imageDAO.updateImage(image);
		} catch (DAOException e) {
			LOGGER.error("Exception while image "+ image + " updating process",e);
			throw new ServiceException("Exception while image "+ image + " updating process",e);
		}
		return imageTitle;
	}

	public List<String> getAllImageTitles() throws ServiceException {
		List<String> imageTitleList = null;
		try {
			imageTitleList = imageDAO.getAllImageTitles();
		} catch (DAOException e) {
			LOGGER.error("Exception while all images retriewing" ,e);
			throw new ServiceException("Exception while all images retriewing" ,e);
		}
		return imageTitleList;
	}

}
